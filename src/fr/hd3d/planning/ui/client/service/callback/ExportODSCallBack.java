package fr.hd3d.planning.ui.client.service.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.planning.ui.client.Planning;


public class ExportODSCallBack extends BaseCallback
{

    @Override
    protected void onSuccess(Request request, Response response)
    {
        Info.display(Planning.MESSAGES.exportInfosTitle(), Planning.MESSAGES.exportInfos());
    }

}
