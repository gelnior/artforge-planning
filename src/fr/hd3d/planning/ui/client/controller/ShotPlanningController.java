package fr.hd3d.planning.ui.client.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.IdGenerator;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.ShotPlanningModel;
import fr.hd3d.planning.ui.client.model.util.NameSorter;
import fr.hd3d.planning.ui.client.modeldata.TimedSequenceModelData;
import fr.hd3d.planning.ui.client.view.ShotPlanningView;


public class ShotPlanningController extends WorkObjectPlanningController
{
    protected ShotPlanningView view;
    protected ShotPlanningModel model;

    public ShotPlanningController(ShotPlanningView view, ShotPlanningModel model)
    {
        super(view, model, EPlanningDisplayMode.SHOT);

        this.view = view;
        this.model = model;
        this.registerEvents();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_SEQUENCES_DATA_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.SEQUENCE_SEQUENCES_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.SEQUENCE_SHOTS_LOADED);

        this.registerEventTypes(PlanningAppEvents.SHOT_MODE_CLICKED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        if (WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADED.equals(event.getType()))
        {
            this.onRootSequencesLoaded(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_SEQUENCES_DATA_LOADED.equals(event.getType()))
        {
            this.onRootSequencesDataLoaded(event);
        }
        else if (WorkObjectPlanningEvents.SEQUENCE_SEQUENCES_LOADED.equals(event.getType()))
        {
            this.onSequencesSequenceLoaded(event);
        }
        else if (WorkObjectPlanningEvents.SEQUENCE_SHOTS_LOADED.equals(event.getType()))
        {
            this.onShotsSequenceLoaded(event);
        }
        else if (PlanningAppEvents.SHOT_MODE_CLICKED.equals(event.getType()))
        {
            this.onShotModeClicked(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onShotModeClicked(AppEvent event)
    {
        this.model.loadSequencesForShotMode();
    }

    // Root Sequences
    private void onRootSequencesLoaded(AppEvent event)
    {
        ServiceStore<SequenceModelData> sequences = event.getData();

        if (sequences.getCount() > 0)
        {
            this.model.loadSequencesData(sequences);
        }
        else
        {
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED);
        }
    }

    private void onRootSequencesDataLoaded(AppEvent event)
    {
        FastMap<TimedSequenceModelData> timedSequences = event.getData();
        List<TimedSequenceModelData> sequenceList = new ArrayList<TimedSequenceModelData>();
        for (TimedSequenceModelData sequence : timedSequences.values())
        {
            sequenceList.add(sequence);
        }
        Collections.sort(sequenceList, new NameSorter<TimedSequenceModelData>());

        for (TimedSequenceModelData sequence : sequenceList)
        {
            TaskGroupModelData taskGroup = new TaskGroupModelData();
            taskGroup.setColor("#AAAAFF");
            taskGroup.setName(sequence.getName());

            taskGroup.setStartDate(sequence.getStartDate());
            taskGroup.setEndDate(sequence.getEndDate());

            if (sequence.getStartDate() != null && sequence.getEndDate() != null)
            {
                taskGroup.setId(IdGenerator.getNewId());
                taskGroup.setType(EPlanningDisplayMode.SHOT);
                taskGroup.setObjectId(sequence.getId());

                this.view.addTaskGroup(taskGroup);
            }
        }
        EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED);
    }

    // Sequence group
    @Override
    protected void loadGroup(TaskGroupModelData taskGroup)
    {
        this.model.loadSequencesSequence(taskGroup);
    }

    private void onSequencesSequenceLoaded(AppEvent event)
    {
        List<TimedSequenceModelData> sequences = event.getData(WorkObjectPlanningEvents.SEQUENCES_VAR);
        final TaskGroupModelData taskGroup = event.getData(WorkObjectPlanningEvents.TASK_GROUP_VAR);

        Collections.sort(sequences, new NameSorter<TimedSequenceModelData>());

        for (TimedSequenceModelData sequence : sequences)
        {
            TaskGroupModelData taskGroupChild = new TaskGroupModelData();
            taskGroupChild.setColor(TaskGroupModelData.SHOT_COLOR);
            taskGroupChild.setName(sequence.getName());
            taskGroupChild.setStartDate(sequence.getStartDate());
            taskGroupChild.setEndDate(sequence.getEndDate());
            taskGroupChild.setId(sequence.getId());
            taskGroupChild.setType(EPlanningDisplayMode.SHOT);
            taskGroupChild.setObjectId(sequence.getId());
            taskGroupChild.setTaskGroupID(taskGroup.getId());

            this.view.addTaskGroup(taskGroupChild);
        }
        this.model.loadShotsSequence(taskGroup);
    }

    private void onShotsSequenceLoaded(AppEvent event)
    {
        final TaskGroupModelData taskGroup = event.getData();

        List<Long> shotIds = new ArrayList<Long>();
        for (ShotModelData shot : this.model.getShotStore().getModels())
        {
            shotIds.add(shot.getId());
        }

        if (shotIds.size() > 0)
        {
            this.model.loadTask(ShotModelData.SIMPLE_CLASS_NAME, taskGroup, shotIds);
        }
        else
        {
            this.view.layoutTaskGroup(taskGroup);
            this.view.setTaskGroupOpen(taskGroup);
            this.view.expandTaskGroup(taskGroup);
        }
    }

    /**
     * When a task is dropped, its tool tip is refreshed. If task does not exist, it is created and added.
     * 
     * @param event
     *            Task dropped event.
     */
    @Override
    protected void onTaskFromGridDropped(AppEvent event)
    {
        TaskModelData task = event.getData();
        if (this.view.isRegisteredTask(task))
        {
            this.view.refreshTask(task);
        }
        else if (FieldUtils.isShot(task.getBoundEntityName()))
        {
            this.view.addTaskPanel(task);
        }
    }

}
