package fr.hd3d.planning.ui.client.controller.context;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.view.PlanningMainView;


public class ColorModeAction implements ContextCommand
{
    private final PlanningMainView view;

    public ColorModeAction(PlanningMainView view)
    {
        this.view = view;
    }

    public void execute(String colorMode)
    {
        try
        {
            this.view.toggleColorButton(colorMode);
            PlanningMainModel.displayMode = ETaskDisplayMode.valueOf(colorMode);
        }
        catch (Exception e)
        {
            PlanningMainModel.displayMode = ETaskDisplayMode.STATUS;
            Logger.warn("no color mode set in preferences");
        }
    }

}
