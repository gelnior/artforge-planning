package fr.hd3d.planning.ui.client.controller.context;

import java.util.Map.Entry;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.setting.UserSettings;


public class PlanningContext
{
    static FastMap<ContextCommand> commands = new FastMap<ContextCommand>();

    public static void put(String key, ContextCommand command)
    {
        commands.put(key, command);
    }

    public static void restoreContext()
    {
        for (Entry<String, ContextCommand> entry : commands.entrySet())
        {
            entry.getValue().execute(UserSettings.getSetting(entry.getKey()));
        }
    }
}
