package fr.hd3d.planning.ui.client.controller.context;

import java.util.Date;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.view.PlanningMainView;


public class PlanningEndDateAction implements ContextCommand
{
    public static void setSetting(Date endDate)
    {
        UserSettings.setSetting(PlanningConfig.END_DATE_SETTING_KEY + PlanningMainModel.getCurrentProject().getId(),
                DatetimeUtil.formatDate(fr.hd3d.common.client.DateFormat.DATE_TIME_STRING, endDate));
    }

    private final PlanningMainView view;

    public PlanningEndDateAction(PlanningMainView view)
    {
        this.view = view;
    }

    public void execute(String value)
    {
        try
        {
            Date date = DateFormat.DATE_TIME.parse(value);
            PlanningMainModel.getCurrentPlanning().setEndDate(date);
            this.view.setDateRangePickerEndDate(date);
        }
        catch (Exception e)
        {
            Logger.warn("No planning start date set in user preferences.");
        }
    }

}
