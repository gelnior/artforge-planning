package fr.hd3d.planning.ui.client.controller.context;

public interface ContextCommand
{
    public void execute(String value);
}
