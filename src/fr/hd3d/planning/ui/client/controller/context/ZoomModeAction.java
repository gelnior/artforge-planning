package fr.hd3d.planning.ui.client.controller.context;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.planning.ui.client.view.PlanningMainView;


public class ZoomModeAction implements ContextCommand
{
    private final PlanningMainView view;

    public ZoomModeAction(PlanningMainView view)
    {
        this.view = view;
    }

    public void execute(String zoomMode)
    {
        try
        {
            this.view.toggleZoomButton(zoomMode);
            BasePlanningWidget.setZoomMode(EZoomMode.valueOf(zoomMode));
        }
        catch (Exception e)
        {
            Logger.warn("no zoom mode set in preferences");
        }
    }
}
