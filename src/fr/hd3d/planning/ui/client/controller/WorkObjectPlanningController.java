package fr.hd3d.planning.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.TaskActivityReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.model.WorkObjectPlanningModel;
import fr.hd3d.planning.ui.client.view.WorkObjectPlanningView;


/**
 * Base controller class for planning planning task widget : it handles task group events and task events.
 * 
 * @author HD3D
 */
public abstract class WorkObjectPlanningController extends MaskableController
{
    private final WorkObjectPlanningView view;
    private final WorkObjectPlanningModel model;

    protected final EPlanningDisplayMode mode;

    public WorkObjectPlanningController(WorkObjectPlanningView view, WorkObjectPlanningModel model,
            EPlanningDisplayMode mode)
    {
        this.registerEvents();
        this.view = view;
        this.model = model;
        this.mode = mode;
    }

    protected void registerEvents()
    {
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_OPENED);
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_CLOSED);
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_SELECTED);
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_REFRESH_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_LOAD_WORKED_DAYS_CLICKED);
        this.registerEventTypes(PlanningAppEvents.TASK_ACTIVITIES_LOADED);

        this.registerEventTypes(WorkObjectPlanningEvents.WORK_OBJECT_TASKS_LOADED);

        this.registerEventTypes(PlanningCommonEvents.TASK_MOVED);
        this.registerEventTypes(PlanningAppEvents.TASK_RESIZED);
        this.registerEventTypes(PlanningAppEvents.TASK_FROM_GRID_DROPPED);
        this.registerEventTypes(PlanningCommonEvents.TASK_SELECTED);
        this.registerEventTypes(PlanningCommonEvents.TASK_DESELECTED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        // Task Group events
        if (PlanningCommonEvents.TASK_GROUP_OPENED.equals(event.getType()))
        {
            this.onTaskGroupOpened(event);
        }
        else if (PlanningCommonEvents.TASK_GROUP_CLOSED.equals(event.getType()))
        {
            this.onTaskGroupClosed(event);
        }
        else if (PlanningCommonEvents.TASK_GROUP_SELECTED.equals(event.getType()))
        {
            this.onTaskGroupSelected(event);
        }
        else if (PlanningCommonEvents.TASK_GROUP_REFRESH_CLICKED.equals(event.getType()))
        {
            this.onRefreshTaskGroupClicked(event);
        }
        else if (PlanningCommonEvents.TASK_GROUP_LOAD_WORKED_DAYS_CLICKED.equals(event.getType()))
        {
            this.onLoadWorkedDaysClicked(event);
        }
        else if (WorkObjectPlanningEvents.WORK_OBJECT_TASKS_LOADED.equals(event.getType()))
        {
            this.onTasksWorkObjectLoaded(event);
        }

        // Task events
        else if (PlanningCommonEvents.TASK_MOVED.equals(event.getType()))
        {
            this.onTaskMoved(event);
        }
        else if (PlanningAppEvents.TASK_RESIZED.equals(event.getType()))
        {
            this.onTaskResized(event);
        }
        else if (PlanningAppEvents.TASK_FROM_GRID_DROPPED.equals(event.getType()))
        {
            this.onTaskFromGridDropped(event);
        }
        else if (PlanningAppEvents.TASK_ACTIVITIES_LOADED.equals(event.getType()))
        {
            this.onTaskActivitiesLoaded(event);
        }
        else if (PlanningCommonEvents.TASK_SELECTED.equals(event.getType()))
        {
            this.onTaskSelected(event);
        }
        else if (PlanningCommonEvents.TASK_DESELECTED.equals(event.getType()))
        {
            this.onTaskDeselected(event);
        }

    }

    private void onTaskSelected(AppEvent event)
    {
        this.view.markTaskAsSelected((TaskModelData) event.getData());
    }

    private void onTaskDeselected(AppEvent event)
    {
        this.view.markTaskAsDeselected((TaskModelData) event.getData());
    }

    /**
     * When task group is opened, if it the first time that it is opened, its data (children task groups and tasks) are
     * loaded then task group node is expanded. Else the task group node is directly expanded.
     * 
     * @param event
     *            Task group opened event.
     */
    protected void onTaskGroupOpened(AppEvent event)
    {
        final TaskGroupModelData taskGroup = event.getData();
        Boolean isFirstExpand = event.getData(PlanningCommonEvents.FIRST_EXPAND_EVENT_VAR_NAME);

        if (isFirstExpand)
        {
            this.loadGroup(taskGroup);
        }
        else
        {
            this.view.expandTaskGroup(taskGroup);
        }
    }

    /**
     * Load task group children.
     * 
     * @param taskGroup
     *            The task group to load.
     */
    protected abstract void loadGroup(TaskGroupModelData taskGroup);

    /**
     * When task group is closed, the corresponding task group item is collapsed.
     * 
     * @param event
     *            Task group closed event. Contains closed task group.
     */
    protected void onTaskGroupClosed(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();
        this.view.collapseTaskGroup(taskGroup);
    }

    /**
     * When task group is selected it is stored in model as the selected task group.
     * 
     * @param event
     *            Task group selected event.
     */
    protected void onTaskGroupSelected(AppEvent event)
    {
        this.model.setSelectedTaskGroup((TaskGroupModelData) event.getData());
    }

    /**
     * When refresh is click upon a task group, it collapses then expand the task as if it was the first time that it is
     * opened : it clears everything then reload children task groups and task corresponding to group task type.
     */
    protected void onRefreshTaskGroupClicked(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();
        if (taskGroup == null)
        {
            taskGroup = this.model.getSelectedTaskGroup();
            EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_GROUP_REFRESH_CLICKED, taskGroup);
        }
        else
        {
            this.view.refreshTaskGroup(taskGroup);
        }
    }

    protected void onLoadWorkedDaysClicked(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();

        if (taskGroup == null)
        {
            taskGroup = this.model.getSelectedTaskGroup();
            EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_GROUP_LOAD_WORKED_DAYS_CLICKED, taskGroup);
        }
        else
        {
            List<TaskModelData> tasks = this.view.getTasksFromTaskGroup(taskGroup);
            List<Long> taskIds = new ArrayList<Long>();
            for (TaskModelData task : tasks)
                taskIds.add(task.getId());

            final ServiceStore<TaskActivityModelData> activities = new ServiceStore<TaskActivityModelData>(
                    new TaskActivityReader());
            activities.setPath(ServicesPath.TASK_ACTIVITIES);
            activities.addInConstraint("task.id", taskIds);

            AppEvent newEvent = new AppEvent(PlanningAppEvents.TASK_ACTIVITIES_LOADED, activities);
            newEvent.setData(PlanningConfig.TASK_GROUP_EVENT_VAR_NAME, taskGroup);
            activities.addLoadListener(new AllLoadListener<TaskActivityModelData>(activities, newEvent));
            activities.reload();
        }
    }

    private void onTaskActivitiesLoaded(AppEvent event)
    {
        ServiceStore<TaskActivityModelData> activities = event.getData();
        TaskGroupModelData taskGroup = event.getData(PlanningConfig.TASK_GROUP_EVENT_VAR_NAME);
        view.clearActivities(taskGroup);
        for (TaskActivityModelData activity : activities.getModels())
        {
            view.displayActivity(activity);
        }
    }

    /**
     * When work object tasks are loaded, they are added inside the extra line of the corresponding work object.
     * 
     * @param event
     *            Task work object is loaded.
     */
    protected void onTasksWorkObjectLoaded(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();

        // this.model.getTaskCategoryStore().sort(TaskModelData.START_DATE_FIELD, SortDir.ASC);

        for (TaskModelData task : this.model.getTaskCategoryStore().getModels())
        {
            this.view.addTaskPanel(taskGroup, task);

            TaskModelData modifiedTask = (TaskModelData) PlanningMainModel.getPlanningModifiedTask(task);
            if (modifiedTask != null)
                this.view.refreshTask(modifiedTask);
        }
        this.view.layoutTaskGroup(taskGroup);
        this.view.setTaskGroupOpen(taskGroup);
        this.view.expandTaskGroup(taskGroup);
    }

    /**
     * When a task is moved, its tool tip is refreshed.
     * 
     * @param event
     *            Task moved event.
     */
    protected void onTaskMoved(AppEvent event)
    {
        TaskModelData task = event.getData();
        EPlanningDisplayMode taskMode = event.getData(PlanningConfig.PLANNING_TYPE_EVENT_VAR_NAME);

        this.view.refreshActivitiesForTask(task);
        if (taskMode != null && taskMode != this.mode)
            this.view.refreshTask(task);
        else
            this.view.refreshToolTip(task);
    }

    /**
     * When a task is resized, its tool tip is refreshed.
     * 
     * @param event
     *            Task resized event.
     */
    protected void onTaskResized(AppEvent event)
    {
        TaskModelData task = event.getData();
        this.view.refreshTask(task);
        this.view.refreshActivitiesForTask(task);
    }

    /**
     * When a task is dropped, its tool tip is refreshed.
     * 
     * @param event
     *            Task dropped event.
     */
    protected void onTaskFromGridDropped(AppEvent event)
    {
        TaskModelData task = event.getData();
        this.view.refreshTask(task);
    }

}
