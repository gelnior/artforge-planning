package fr.hd3d.planning.ui.client.controller;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.util.PlanningSettings;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.model.PlanningPanelModel;
import fr.hd3d.planning.ui.client.view.PlanningPanelView;
import fr.hd3d.planning.ui.client.widget.dialog.MilestoneEditorDialog;


/**
 * Controller that handles planning panel events (panel that contains all plannings : task type, constituent, sequence
 * plannings).
 * 
 * @author HD3D
 */
public class PlanningPanelController extends MaskableController
{
    /** Model handling planning panel data. */
    private final PlanningPanelModel model;
    /** View handling planning panel widgets. */
    private final PlanningPanelView view;

    public PlanningPanelController(PlanningPanelView planningView, PlanningPanelModel planningModel)
    {
        this.model = planningModel;
        this.view = planningView;

        this.registerEvents();
    }

    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_CHANGED);
        this.registerEventTypes(PlanningAppEvents.REFRESH_PLANNING_CLICKED);
        this.registerEventTypes(PlanningAppEvents.TASKS_SAVED);
        this.registerEventTypes(PlanningAppEvents.TASK_GROUPS_SAVED);

        this.registerEventTypes(WorkObjectPlanningEvents.MILESTONES_LOADED);
        this.registerEventTypes(PlanningCommonEvents.ADD_MILESTONE_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.DELETE_MILESTONE_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.UPDATE_MILESTONE_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.MILESTONE_SAVED);
        this.registerEventTypes(PlanningCommonEvents.DELETE_MILESTONE_CONFIRMED);

        this.registerEventTypes(PlanningAppEvents.COLOR_TASK_START_CLICKED);
        this.registerEventTypes(PlanningAppEvents.COLOR_TASK_STATUS_CLICKED);
        this.registerEventTypes(PlanningAppEvents.COLOR_TASK_TASK_TYPE_CLICKED);

        this.registerEventTypes(PlanningAppEvents.CONSTITUENT_MODE_CLICKED);
        this.registerEventTypes(PlanningAppEvents.SHOT_MODE_CLICKED);
        this.registerEventTypes(PlanningAppEvents.TASK_TYPE_MODE_CLICKED);
        this.registerEventTypes(PlanningAppEvents.TASK_TYPE_SHOT_MODE_CLICKED);

        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_TASK_GROUPS_LOADED);

        this.registerEventTypes(PlanningCommonEvents.TASK_MOVED);
        this.registerEventTypes(PlanningAppEvents.TASK_RESIZED);
        this.registerEventTypes(PlanningAppEvents.TASK_FROM_GRID_DROPPED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {

        // Planning events
        if (CommonEvents.PROJECT_CHANGED.equals(event.getType()))
        {
            this.onProjectChanged(event);
        }
        else if (PlanningAppEvents.PLANNING_CHANGED.equals(event.getType()))
        {
            this.onPlanningChanged(event);
        }
        else if (PlanningAppEvents.REFRESH_PLANNING_CLICKED.equals(event.getType()))
        {
            this.onPlanningRefreshClicked();
        }
        else if (PlanningAppEvents.TASKS_SAVED.equals(event.getType()))
        {
            this.onTasksSaved(event);
        }
        else if (PlanningAppEvents.TASK_GROUPS_SAVED.equals(event.getType()))
        {
            this.onTaskGroupsSaved(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED.equals(event.getType()))
        {
            this.onRootSequencesLoadingFinished(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED.equals(event.getType()))
        {
            this.onRootCategoriesLoadingFinished(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_TASK_GROUPS_LOADED.equals(event.getType()))
        {
            this.onRootTaskGroupsLoadingFinished(event);
        }

        // Milestone events
        else if (WorkObjectPlanningEvents.MILESTONES_LOADED.equals(event.getType()))
        {
            this.onMilestonesLoaded();
        }
        else if (PlanningCommonEvents.ADD_MILESTONE_CLICKED.equals(event.getType()))
        {
            this.onAddMilestoneClicked(event);
        }
        else if (PlanningCommonEvents.UPDATE_MILESTONE_CLICKED.equals(event.getType()))
        {
            this.onUpdateMilestoneClicked(event);
        }
        else if (PlanningCommonEvents.DELETE_MILESTONE_CLICKED.equals(event.getType()))
        {
            this.onDeleteMilestoneClicked(event);
        }
        else if (PlanningCommonEvents.DELETE_MILESTONE_CONFIRMED.equals(event.getType()))
        {
            this.onDeleteMilestoneConfirmed(event);
        }
        else if (PlanningCommonEvents.MILESTONE_SAVED.equals(event.getType()))
        {
            this.onMilestoneSaved(event);
        }

        // Task color mode events
        else if (PlanningAppEvents.COLOR_TASK_TASK_TYPE_CLICKED.equals(event.getType()))
        {
            this.onDisplayModeClicked(ETaskDisplayMode.TASK_TYPE);
        }
        else if (PlanningAppEvents.COLOR_TASK_STATUS_CLICKED.equals(event.getType()))
        {
            this.onDisplayModeClicked(ETaskDisplayMode.STATUS);
        }
        else if (PlanningAppEvents.COLOR_TASK_START_CLICKED.equals(event.getType()))
        {
            this.onDisplayModeClicked(ETaskDisplayMode.ON_TIME);
        }

        // Planning display events
        else if (PlanningAppEvents.TASK_TYPE_MODE_CLICKED.equals(event.getType()))
        {
            this.onTaskTypeModeClicked();
        }
        else if (PlanningAppEvents.TASK_TYPE_SHOT_MODE_CLICKED.equals(event.getType()))
        {
            this.onTaskTypeShotModeClicked();
        }
        else if (PlanningAppEvents.CONSTITUENT_MODE_CLICKED.equals(event.getType()))
        {
            this.onConstituentModeClicked(event);
        }
        else if (PlanningAppEvents.SHOT_MODE_CLICKED.equals(event.getType()))
        {
            this.onShotModeClicked(event);
        }
        else if (PlanningCommonEvents.TASK_MOVED.equals(event.getType()))
        {
            this.forwardToChildForced(event);
        }
        else if (PlanningAppEvents.TASK_RESIZED.equals(event.getType()))
        {
            this.forwardToChildForced(event);
        }
        else if (PlanningAppEvents.TASK_FROM_GRID_DROPPED.equals(event.getType()))
        {
            this.forwardToChildForced(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When project is changed, planning is cleared.
     * 
     * @param event
     *            Project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        this.clearPlanning();
    }

    /**
     * When planning is changed, planning is cleared then current planning is loaded.
     * 
     * @param event
     *            Planning changed event.
     */
    private void onPlanningChanged(AppEvent event)
    {
        Boolean isCreation = event.getData(PlanningConfig.PLANNING_CREATION_EVENT_VAR_NAME);

        this.clearPlanning();
        if (isCreation != null && isCreation)
        {}
        else
        {
            this.openPlanning();
        }
    }

    /** When tasks are saved, all modification indicators are removed from task bars. */
    private void onTasksSaved(AppEvent event)
    {
        List<TaskModelData> tasks = event.getData();
        this.view.removeTaskModificationIndicator(tasks);
    }

    /** When tasks are saved, all modification indicators are removed from task group bars. */
    private void onTaskGroupsSaved(AppEvent event)
    {
        List<TaskGroupModelData> taskGroups = event.getData();
        this.view.removeTaskGroupModificationIndicator(taskGroups);
    }

    /**
     * When root sequences are loaded, the shot planning is mark as loaded.
     * 
     * @param event
     *            Root sequences loaded event.
     */
    private void onRootSequencesLoadingFinished(AppEvent event)
    {
        this.model.setIsPlanningLoaded(EPlanningDisplayMode.SHOT, true);
    }

    /**
     * When root categories are loaded, the constituent planning is mark as loaded.
     * 
     * @param event
     *            Root categories loaded event.
     */
    private void onRootCategoriesLoadingFinished(AppEvent event)
    {
        this.model.setIsPlanningLoaded(EPlanningDisplayMode.CONSTITUENT, true);
    }

    /**
     * When root task types are loaded, the task type planning is mark as loaded.
     * 
     * @param event
     *            Root categories loaded event.
     */
    private void onRootTaskGroupsLoadingFinished(AppEvent event)
    {
        this.forwardToChild(event);
        this.model.reloadMilestones();
    }

    /**
     * Clear planning remove everything from tree and from current planning. Milestone store is cleared too.
     */
    public void clearPlanning()
    {
        this.view.clearTrees();
        this.view.clearPlanning();

        this.model.getMileStones().clear();
        this.model.clearPlanningLoadedMarker();

        this.view.displayPlanning(EPlanningDisplayMode.TASK_TYPE);
        this.view.resetDisplayButtons();
    }

    /** Build time line and load root task groups (task groups with no parent). */
    private void openPlanning()
    {
        this.buildTimeLine();
        this.view.showPlanningPanel();

        this.forwardToChild(new AppEvent(PlanningAppEvents.PLANNING_OPENED));
    }

    /** Build time line depending on date set by current user. */
    private void buildTimeLine()
    {
        PlanningSettings settings = new PlanningSettings(PlanningMainModel.currentPlanning.getStartDate(),
                PlanningMainModel.currentPlanning.getEndDate());

        this.view.buildTimeLine(settings);
    }

    /**
     * When milestones are loaded, it adds every retrieved milestones to time line. Milestone retrieving marks the end
     * of the planning loading so PLANNING_LOADED is dispatched to controllers.
     */
    private void onMilestonesLoaded()
    {
        for (MilestoneModelData milestone : this.model.getMileStones())
        {
            this.view.addMilestone(milestone);
        }
        this.view.scrollToCurrentDay();
        EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_LOADED);
    }

    /**
     * When add milestone is clicked, it displays milestone editor for a new milestone of which date is equal to the
     * date of selected time line element.
     * 
     * @param event
     *            The add milestone clicked event.
     */
    private void onAddMilestoneClicked(AppEvent event)
    {
        DateWrapper date = event.getData();
        MilestoneModelData milestone = new MilestoneModelData();
        milestone.setDate(date.asDate());
        MilestoneEditorDialog.getInstance(milestone).show();
    }

    /**
     * When update milestone is clicked, it opens milestone editor for selected milestone.
     * 
     * @param event
     *            The update milestone clicked event.
     */
    private void onUpdateMilestoneClicked(AppEvent event)
    {
        MilestoneModelData milestone = event.getData();
        MilestoneEditorDialog.getInstance(milestone).show();
    }

    /**
     * When milestone is saved, the milestone is added to the view. If milestone is already present, it is updated.
     * 
     * @param event
     *            The milestone saved event.
     */
    private void onMilestoneSaved(AppEvent event)
    {
        MilestoneModelData milestone = event.getData();
        Boolean isNew = milestone.get(MilestoneModelData.IS_NEW);

        if (isNew != null && !isNew)
        {
            Date date = milestone.getDate();
            milestone.setDate((Date) milestone.get(MilestoneModelData.PREVIOUS_DATE));
            this.view.removeMilestone(milestone);
            milestone.setDate(date);
        }

        this.view.addMilestone(milestone);
    }

    /**
     * When delete milestone is clicked, it opens a confirmation dialog box, with milestone attached to the dialog box
     * confirmation event.
     * 
     * @param event
     *            The delete milestone clicked event.
     */
    private void onDeleteMilestoneClicked(AppEvent event)
    {
        MilestoneModelData milestone = event.getData();
        this.view.displayDeleteMilestoneConfirmation(milestone);
    }

    /**
     * When delete milestone is confirmed, it send a delete requests to services and remove milestone from time line.
     * 
     * @param event
     */
    private void onDeleteMilestoneConfirmed(AppEvent event)
    {
        MilestoneModelData milestone = event.getData();

        this.view.removeMilestone(milestone);
        milestone.setDefaultPath(PlanningMainModel.getCurrentPlanning().getDefaultPath() + "/"
                + ServicesPath.MILESTONES + milestone.getId());
        milestone.delete();
    }

    /**
     * When a display mode button is clicked, all tasks colors are updated depending on display mode selected .
     * 
     * @param displayMode
     */
    private void onDisplayModeClicked(ETaskDisplayMode displayMode)
    {
        // TODO: Make assignment in planning main controller.
        PlanningMainModel.displayMode = displayMode;
        this.view.updateTaskColors();
    }

    /**
     * When refresh button is clicked, planning is cleared. Then current planning is reloaded.
     */
    private void onPlanningRefreshClicked()
    {
        this.clearPlanning();
        this.openPlanning();
    }

    /**
     * When task type mode is clicked, it loads
     */
    private void onTaskTypeModeClicked()
    {
        this.view.displayPlanning(EPlanningDisplayMode.TASK_TYPE);
    }

    /**
     * When task type mode is clicked, it loads
     */
    private void onTaskTypeShotModeClicked()
    {
        this.view.displayPlanning(EPlanningDisplayMode.TASK_TYPE_SHOT);
        if (!this.model.isPlanningLoaded(EPlanningDisplayMode.TASK_TYPE_SHOT))
        {
            this.forwardToChild(new AppEvent(PlanningAppEvents.PLANNING_OPENED));
            this.model.setIsPlanningLoaded(EPlanningDisplayMode.TASK_TYPE_SHOT, Boolean.TRUE);
        }
    }

    /**
     * Display constituent planning. If it is the first time constituent planning is displayed, its data are reloaded
     * and notification event is forwarded to other controllers.
     */
    private void onConstituentModeClicked(AppEvent event)
    {
        this.view.displayPlanning(EPlanningDisplayMode.CONSTITUENT);
        if (!this.model.isPlanningLoaded(EPlanningDisplayMode.CONSTITUENT))
        {
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_STARTED);

            this.forwardToChild(event);
        }
    }

    private void onShotModeClicked(AppEvent event)
    {
        this.view.displayPlanning(EPlanningDisplayMode.SHOT);
        if (!this.model.isPlanningLoaded(EPlanningDisplayMode.SHOT))
        {
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_STARTED);

            this.forwardToChild(event);
        }
    }

}
