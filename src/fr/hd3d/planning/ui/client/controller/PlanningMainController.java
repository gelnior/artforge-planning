package fr.hd3d.planning.ui.client.controller;

import java.util.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.user.client.Timer;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.planning.ui.client.config.EUnassignedFilter;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.controller.context.ColorModeAction;
import fr.hd3d.planning.ui.client.controller.context.PlanningContext;
import fr.hd3d.planning.ui.client.controller.context.PlanningEndDateAction;
import fr.hd3d.planning.ui.client.controller.context.PlanningStartDateAction;
import fr.hd3d.planning.ui.client.controller.context.ZoomModeAction;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.view.PlanningMainView;


/**
 * PlanningController handles events that imply interactions between widgets of planning application.
 * 
 * @author HD3D
 */
public class PlanningMainController extends MainController
{
    /** Model that handles planning main data. */
    private final PlanningMainModel model;
    /** View containing widgets. */
    private final PlanningMainView view;
    /** Flag to know if planning is waiting for save (due to auto-save mode). */
    private boolean isWaitingForSave = false;

    /**
     * Constructor : registers model, view and events.
     * 
     * @param model
     *            Model that handles planning main data.
     * @param view
     *            View containing widgets.
     */
    public PlanningMainController(PlanningMainModel model, PlanningMainView view)
    {
        super(model, view);
        this.model = model;
        this.view = view;
        this.registerEvents();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(PlanningCommonEvents.TASK_MOVED);

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);

        this.registerEventTypes(PlanningAppEvents.TASK_GRID_DATA_LOADED);

        this.registerEventTypes(PlanningAppEvents.PLANNING_LOADED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_CHANGED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_DATE_CHANGED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_SAVE_CLICKED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_AUTO_SAVE_CLICKED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_SAVED);
        this.registerEventTypes(PlanningAppEvents.TASK_GROUPS_SAVED);
        this.registerEventTypes(PlanningAppEvents.EXTRA_LINES_CREATED);
        this.registerEventTypes(PlanningAppEvents.TASKS_SAVED);
        this.registerEventTypes(PlanningAppEvents.TASK_FROM_GRID_DROPPED);
        this.registerEventTypes(PlanningAppEvents.TASK_RESIZED);
        this.registerEventTypes(PlanningAppEvents.TASK_FILTER_CHANGED);
        this.registerEventTypes(PlanningAppEvents.TASK_TASK_TYPE_FILTER_CHANGED);
        this.registerEventTypes(PlanningAppEvents.TASK_WORK_OBJECT_FILTER_CHANGED);

        this.registerEventTypes(PlanningAppEvents.TASK_GROUP_MOVED);
        this.registerEventTypes(PlanningAppEvents.TASK_GROUP_RESIZED);
        this.registerEventTypes(PlanningAppEvents.WEEK_DISPLAY_CLICKED);
        this.registerEventTypes(PlanningAppEvents.MONTH_DISPLAY_CLICKED);
        this.registerEventTypes(PlanningAppEvents.YEAR_DISPLAY_CLICKED);

        this.registerEventTypes(PlanningAppEvents.EXTRA_LINE_ADDED);

        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_STARTED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_STARTED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED);

        this.registerEventTypes(PlanningAppEvents.EXPORT_ODS_PLANNING_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.TASK_SELECTED);
        this.registerEventTypes(PlanningCommonEvents.TASK_DESELECTED);

    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        if (CommonEvents.PROJECT_CHANGED.equals(event.getType()))
        {
            this.onProjectChanged(event);
        }
        else if (PlanningAppEvents.PLANNING_LOADED.equals(event.getType()))
        {
            this.onPlanningLoaded(event);
        }
        else if (PlanningAppEvents.PLANNING_CHANGED.equals(event.getType()))
        {
            this.onPlanningChanged(event);
        }
        else if (PlanningAppEvents.PLANNING_DATE_CHANGED.equals(event.getType()))
        {
            this.onPlanningDateChanged(event);
        }
        else if (PlanningAppEvents.PLANNING_SAVE_CLICKED.equals(event.getType()))
        {
            this.onPlanningSaveClicked(event);
        }
        else if (PlanningAppEvents.PLANNING_AUTO_SAVE_CLICKED.equals(event.getType()))
        {
            this.onPlanningAutoSaveClicked(event);
        }
        else if (PlanningAppEvents.PLANNING_SAVED.equals(event.getType()))
        {
            this.onPlanningSaved(event);
        }
        else if (PlanningAppEvents.TASK_GROUPS_SAVED.equals(event.getType()))
        {
            this.onTaskGroupsSaved(event);
        }
        else if (PlanningAppEvents.EXTRA_LINES_CREATED.equals(event.getType()))
        {
            this.onExtraLinesCreated(event);
        }
        else if (PlanningAppEvents.TASKS_SAVED.equals(event.getType()))
        {
            this.onTasksSaved(event);
        }
        else if (PlanningAppEvents.TASK_FROM_GRID_DROPPED.equals(event.getType()))
        {
            this.onTaskFromGridDropped(event);
        }
        else if (PlanningCommonEvents.TASK_MOVED.equals(event.getType()))
        {
            this.onTaskMoved(event);
        }
        else if (PlanningAppEvents.TASK_RESIZED.equals(event.getType()))
        {
            this.onTaskResized(event);
        }
        else if (PlanningAppEvents.TASK_FILTER_CHANGED.equals(event.getType()))
        {
            this.onTaskFilterChanged(event);
        }
        else if (PlanningAppEvents.TASK_TASK_TYPE_FILTER_CHANGED.equals(event.getType()))
        {
            this.onTaskTaskTypeFilterChanged(event);
        }
        else if (PlanningAppEvents.TASK_GROUP_MOVED.equals(event.getType()))
        {
            this.onTaskGroupMoved(event);
        }
        else if (PlanningAppEvents.TASK_GROUP_RESIZED.equals(event.getType()))
        {
            this.onTaskGroupResized(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_STARTED.equals(event.getType()))
        {
            this.onRootCategoriesStarted();
        }
        else if (WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED.equals(event.getType()))
        {
            this.onRootCategoriesFinished(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_STARTED.equals(event.getType()))
        {
            this.onRootSequencesStarted();
        }
        else if (WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADING_FINISHED.equals(event.getType()))
        {
            this.onRootSequencesFinished(event);
        }
        else if (PlanningAppEvents.EXPORT_ODS_PLANNING_CLICKED.equals(event.getType()))
        {
            this.onExportODSClicked(event);
        }
        else if (PlanningAppEvents.REFRESH_PLANNING_CLICKED.equals(event.getType()))
        {
            this.onPlanningRefreshClicked(event);
        }
        else if (PlanningAppEvents.TASK_WORK_OBJECT_FILTER_CHANGED.equals(event.getType()))
        {
            this.onWorkObjectFilterChanged(event);
        }
        else if (PlanningAppEvents.WEEK_DISPLAY_CLICKED.equals(event.getType()))
        {
            this.onZoomModeChanged(EZoomMode.BIG);
        }
        else if (PlanningAppEvents.MONTH_DISPLAY_CLICKED.equals(event.getType()))
        {
            this.onZoomModeChanged(EZoomMode.NORMAL);
        }
        else if (PlanningAppEvents.YEAR_DISPLAY_CLICKED.equals(event.getType()))
        {
            this.onZoomModeChanged(EZoomMode.SMALL);
        }
        else if (PlanningAppEvents.COLOR_TASK_TASK_TYPE_CLICKED.equals(event.getType()))
        {
            this.onColorModelChanged(event, ETaskDisplayMode.TASK_TYPE);
        }
        else if (PlanningAppEvents.COLOR_TASK_STATUS_CLICKED.equals(event.getType()))
        {
            this.onColorModelChanged(event, ETaskDisplayMode.STATUS);
        }
        else if (PlanningAppEvents.COLOR_TASK_START_CLICKED.equals(event.getType()))
        {
            this.onColorModelChanged(event, ETaskDisplayMode.ON_TIME);
        }
        else if (CommonEvents.ERROR.equals(event.getType()))
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When error occurs, saving indicator, are hidden, buttons are enabled and right error message is displayed
     * (inherited action).
     */
    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);

        this.view.hideSaving();
        this.view.enableToolBarFields();
        this.view.enableUnassignedTasksGrid();
        this.forwardToChild(event);
    }

    /**
     * When settings are initialized, widgets are built, start panel is hidden, user setings are applied to curent
     * context and project combo box store is reloaded.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        this.view.initWidgets();
        this.view.hideStartPanel();

        Boolean isAutoSave = UserSettings.getSettingAsBoolean(PlanningConfig.AUTO_SAVE_SETTING_KEY);
        if (isAutoSave != null && isAutoSave)
        {
            PlanningMainModel.isAutoSave = isAutoSave;
            this.view.toggleAutoSaveButton();
        }

        PlanningContext.put(PlanningConfig.ZOOM_MODE_SETTING_KEY, new ZoomModeAction(view));
        PlanningContext.put(PlanningConfig.COLOR_MODE_SETTING_KEY, new ColorModeAction(view));
        PlanningContext.restoreContext();

        // TODO: register project combo box store to model.
        this.view.reloadProjects();
    }

    /**
     * When project changed, newly selected project is register as a current project.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        this.forwardToChild(event);
        MainModel.currentProject = (ProjectModelData) event.getData();
        this.model.getUnassignedTaskStore().setPath(MainModel.currentProject.getPath() + "/" + ServicesPath.TASKS);
        this.model.getUnassignedTaskTypeStore().setPath(
                MainModel.currentProject.getPath() + "/" + ServicesPath.TASKTYPES);

        MainModel.currentProject.getMasterPlanning(PlanningAppEvents.PLANNING_CHANGED);
    }

    /**
     * When planning is loaded, the toolbar fields and buttons are enabled. The save lists are cleared (task groups,
     * extra lines and tasks).
     * 
     * @param event
     *            The planning loaded event.
     */
    private void onPlanningLoaded(AppEvent event)
    {

        this.model.clearTasksToSave();
        this.model.clearTaskGroupsToSave();
        this.model.clearExtraLineToCreate();
        this.model.clearTaskGroupsToCreate();

        this.view.hideLoading();
        this.view.enableToolBarFields();
    }

    /**
     * When planning changed, if no planning exists, it creates a new one with first day of the month as start date and
     * 3 months later for end date. If a master planning already exists, it is loaded. Planning is also registered as
     * current planning.<br>
     * 
     * @param event
     *            The planning changed event.
     */
    private void onPlanningChanged(AppEvent event)
    {
        PlanningModelData planning = event.getData();

        this.view.disableToolBarFields();
        if (planning == null)
        {
            planning = new PlanningModelData();
            planning.setProject(MainModel.getCurrentProject());
            planning.setName(MainModel.getCurrentProject().getName() + " Planning");
            planning.setMaster(Boolean.TRUE);
            planning.setStartDate(DatetimeUtil.getFirstDayOfCurrentMonth());
            planning.setEndDate(new DateWrapper(planning.getStartDate()).addMonths(3).asDate());
            PlanningMainModel.currentPlanning = planning;

            planning.save(new PostModelDataCallback(planning) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);
                }
            });
        }
        else
        {
            this.view.showPlanningLoading();
            PlanningMainModel.setCurrentPlanning(planning);
            Boolean isChanged = PlanningMainModel.getCurrentPlanning().get(PlanningMainModel.CHANGED_FIELD);
            if (isChanged == null || !isChanged)
            {
                String startDateString = UserSettings.getSetting(PlanningConfig.START_DATE_SETTING_KEY
                        + PlanningMainModel.getCurrentProject().getId());
                String endDateString = UserSettings.getSetting(PlanningConfig.END_DATE_SETTING_KEY
                        + PlanningMainModel.getCurrentProject().getId());
                new PlanningStartDateAction(view).execute(startDateString);
                new PlanningEndDateAction(view).execute(endDateString);
            }
            PlanningMainModel.getCurrentPlanning().set(PlanningMainModel.CHANGED_FIELD, false);

            this.view.bindDateFields(PlanningMainModel.getCurrentPlanning());

            this.model.getUnassignedTaskStore().reload();
            this.forwardToChild(event);
        }
    }

    /**
     * When planning dates are changed, the whole planning is reloaded depending and displays task groups lines between
     * newly set dates.
     * 
     * @param event
     *            Planning date changed event.
     */
    private void onPlanningDateChanged(AppEvent event)
    {
        AppEvent mvcEvent = new AppEvent(PlanningAppEvents.PLANNING_CHANGED);
        mvcEvent.setData(PlanningMainModel.getCurrentPlanning());

        PlanningMainModel.getCurrentPlanning().setStartDate(this.view.getStartDate());
        PlanningMainModel.getCurrentPlanning().setEndDate(this.view.getEndDate());
        PlanningMainModel.getCurrentPlanning().set(PlanningMainModel.CHANGED_FIELD, true);

        PlanningStartDateAction.setSetting(PlanningMainModel.getCurrentPlanning().getStartDate());
        PlanningEndDateAction.setSetting(PlanningMainModel.getCurrentPlanning().getEndDate());

        this.onPlanningChanged(mvcEvent);
        if (PlanningMainModel.isAutoSave)
            EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVE_CLICKED);
    }

    /**
     * When a task is moved, the task grid line corresponding is updated if it is displayed..
     * 
     * @param event
     *            The planning save clicked event.
     */
    private void onPlanningSaveClicked(AppEvent event)
    {
        this.view.disableToolBarFields();
        this.view.showSaving();
        this.model.savePlanning();
    }

    /**
     * When auto save button is clicked, autosave mode is enabled depending if button is up or down. The button state is
     * saved to user preferences.
     * 
     * @param event
     *            The planning auto save clicked event.
     */
    private void onPlanningAutoSaveClicked(AppEvent event)
    {
        PlanningMainModel.isAutoSave = event.getData();

        UserSettings.setSetting(PlanningConfig.AUTO_SAVE_SETTING_KEY, PlanningMainModel.isAutoSave.toString());

        if (PlanningMainModel.isAutoSave)
            EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVE_CLICKED);
    }

    /**
     * When planning is saved, created task groups are saved.<br>
     * Save sequence is : Planning, new task groups, modified task groups, new extra lines, modified tasks.
     * 
     * @param event
     *            Planning saved event.
     */
    private void onPlanningSaved(AppEvent event)
    {
        this.model.saveTaskGroups();
    }

    /**
     * When modified task groups are saved, it save new extra lines.
     * 
     * @param event
     *            Task group saved event.
     */
    private void onTaskGroupsSaved(AppEvent event)
    {
        event.setData(this.model.getModifiedTaskGroups());
        this.forwardToChild(event);
        this.model.clearTaskGroupsToSave();
        this.model.saveCreatedExtraLines();
    }

    /**
     * When extra lines are created, modified tasks are saved.
     * 
     * @param event
     *            Extra line creation event.
     */
    private void onExtraLinesCreated(AppEvent event)
    {
        this.model.saveTasks();
    }

    /**
     * When tasks are saved, saving indicator is hidden, and tool bar fields are enabled. To remove modified indicator
     * on task widgets, the event is forwarded before to the children controllers (to planning widgets).
     * 
     * @param event
     *            Tasks saved event.
     */
    private void onTasksSaved(AppEvent event)
    {
        event.setData(this.model.getModifiedTasks());
        this.forwardToChild(event);
        this.model.getUnassignedTaskStore().commitChanges();
        this.view.hideSaving();
        this.view.enableToolBarFields();
        this.model.clearTasksToSave();
    }

    /**
     * When task is dropped from task grid, its start date, end date and assigned worker are updated.
     * 
     * @param event
     *            Task from task grid dropped event.
     */
    private void onTaskFromGridDropped(AppEvent event)
    {
        int nbDays = event.getData(PlanningConfig.NB_DAYS_EVENT_VAR_NAME);
        int durationDays = event.getData(PlanningConfig.DURATION_DAYS_EVENT_VAR_NAME);
        TaskModelData task = event.getData();

        TaskModelData gridTask = this.model.getUnassignedTaskStore().findModel(TaskModelData.ID_FIELD, task.getId());
        Record gridTaskRecord = this.model.getUnassignedTaskStore().getRecord(gridTask);
        DateWrapper planningStartDate = new DateWrapper(PlanningMainModel.getCurrentPlanning().getStartDate());
        task.setStartDate(planningStartDate.addDays(nbDays).asDate());

        Long duration = task.getDuration();
        if (duration == null)
            duration = 0L;

        DateWrapper taskStartDate = new DateWrapper(task.getStartDate());
        task.setEndDate(taskStartDate.addDays(durationDays - 1).asDate());

        // Workaround to mark task as dirty.
        String workerName = task.getWorkerName();
        Date startDate = task.getStartDate();
        Date endDate = task.getEndDate();
        gridTaskRecord.set(TaskModelData.WORKER_NAME_FIELD, null);
        gridTaskRecord.set(TaskModelData.START_DATE_FIELD, null);
        gridTaskRecord.set(TaskModelData.END_DATE_FIELD, null);
        gridTaskRecord.commit(false);
        gridTaskRecord.set(TaskModelData.WORKER_NAME_FIELD, workerName);
        gridTaskRecord.set(TaskModelData.START_DATE_FIELD, startDate);
        gridTaskRecord.set(TaskModelData.END_DATE_FIELD, endDate);

        this.model.addModifiedTask(task);
        this.forwardToChild(event);

        this.waitForAutoSave();
    }

    /**
     * When a task is moved, the task grid line corresponding is updated (dates and worker) if it is displayed.
     * 
     * @param event
     *            The task moved event.
     */
    private void onTaskMoved(AppEvent event)
    {
        TaskModelData task = event.getData();
        TaskModelData gridTask = this.model.getUnassignedTaskStore().findModel(TaskModelData.ID_FIELD, task.getId());

        if (gridTask != null && event.getData(PlanningConfig.PLANNING_TYPE_EVENT_VAR_NAME) != EPlanningDisplayMode.ALL)
        {
            this.model.updateGridTask(gridTask, task);
        }
        this.model.addModifiedTask(task);
        this.forwardToChild(event);

        this.waitForAutoSave();
    }

    /**
     * When a task is resized, the task grid line corresponding is updated (dates) if it is displayed..
     * 
     * @param event
     *            The task resized event.
     */
    private void onTaskResized(AppEvent event)
    {
        TaskModelData task = event.getData();

        TaskModelData gridTask = this.model.getUnassignedTaskStore().findModel(TaskModelData.ID_FIELD, task.getId());
        if (gridTask != null)
        {
            this.model.updateGridTask(gridTask, task);
        }
        this.model.addModifiedTask(task);

        this.forwardToChild(event);

        this.waitForAutoSave();
    }

    /**
     * When task filter changed, unassigned task store is reloaded for selected filter (all = all tasks, unassigned =
     * tasks with no worker, unplanned = tasks with no start and end date).
     * 
     * @param event
     *            Task filter changed.
     */
    private void onTaskFilterChanged(AppEvent event)
    {
        FieldModelData selectedFilter = event.getData();
        this.model.reloadUnassignedTasks((EUnassignedFilter) selectedFilter.getValue());
    }

    /**
     * When task type filter changed, unassigned task grid is reloaded with only tasks of which task type is equal to
     * selected task type. If task type is null, task type filter is removed and tasks are reloaded.
     * 
     * @param event
     *            Unassigned task grid and task type filter changed.
     */
    private void onTaskTaskTypeFilterChanged(AppEvent event)
    {
        List<TaskTypeModelData> taskTypes = event.getData();
        if (CollectionUtils.isNotEmpty(taskTypes))
            this.model.reloadUnassignedTasks(taskTypes);
        else
        {
            this.view.clearTaskTypeFilter();
            this.model.reloadWithoutTaskType();
        }
    }

    /**
     * When a task group is moved, it is added to the modified task group list.
     * 
     * @param event
     *            Task group moved event.
     */
    private void onTaskGroupMoved(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();

        this.model.addModifiedTaskGroup(taskGroup);
        this.forwardToChild(event);

        if (PlanningMainModel.isAutoSave)
            EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVE_CLICKED);
    }

    /**
     * When a task group is resized, it is added to the modified task group list.
     * 
     * @param event
     *            Task group resized event.
     */
    private void onTaskGroupResized(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();

        this.model.addModifiedTaskGroup(taskGroup);

        if (PlanningMainModel.isAutoSave)
            EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVE_CLICKED);
    }

    /**
     * When root category loading starts, main view displays loading panel for this.
     */
    private void onRootCategoriesStarted()
    {
        this.view.showRootCategoriesLoading();
    }

    /** When category loading finished, the loading dialog is hidden. */
    private void onRootCategoriesFinished(AppEvent event)
    {
        this.view.hideLoading();
        this.forwardToChild(event);
    }

    /**
     * When root sequence loading starts, main view displays loading panel for this.
     */
    private void onRootSequencesStarted()
    {
        this.view.showRootSequencesLoading();
    }

    /** When sequence loading finished, the loading dialog is hidden. */
    private void onRootSequencesFinished(AppEvent event)
    {
        this.view.hideLoading();
        this.forwardToChild(event);
    }

    /**
     * When export button is clicked, export planning service is called via a new navigator window (GET request).
     * 
     * @param event
     *            The export ODS clicked event.
     * @throws Hd3dException
     */
    private void onExportODSClicked(AppEvent event)
    {
        try
        {
            if (PlanningMainModel.currentPlanning.getStartDate() == null
                    || PlanningMainModel.currentPlanning.getEndDate() == null)
            {
                throw new Hd3dException("The start date or the end date is empty or not saved !");
            }
            String path = ServicesPath.PROJECTS + MainModel.currentProject.getId() + "/" + ServicesPath.PLANNINGS
                    + PlanningMainModel.currentPlanning.getId() + "/" + ServicesPath.PLANNING_EXPORT_ODS;

            this.view.openOdsExport(RestRequestHandlerSingleton.getInstance().getServicesUrl() + path);
        }
        catch (Hd3dException e)
        {
            Logger.log("Error occured when trying to export planning.", e);
        }
    }

    /**
     * When planning refresh button is clicked, all data to save are cleared, unassigned task grid is refreshed and
     * event is forwarded to children to reload planning.
     * 
     * @param event
     *            Planning refresh clicked.
     */
    private void onPlanningRefreshClicked(AppEvent event)
    {
        this.model.clearExtraLineToCreate();
        this.model.clearTaskGroupsToCreate();
        this.model.clearTaskGroupsToSave();
        this.model.clearTasksToSave();

        this.model.getUnassignedTaskStore().reload();
        this.forwardToChild(event);
    }

    /**
     * Update planning configuration for cell size then reload planning.
     * 
     * @param zoomMode
     *            to set.
     */
    private void onZoomModeChanged(EZoomMode zoomMode)
    {
        BasePlanningWidget.setZoomMode(zoomMode);
        UserSettings.setSetting(PlanningConfig.ZOOM_MODE_SETTING_KEY, zoomMode.toString());

        this.handleEvent(new AppEvent(PlanningAppEvents.PLANNING_CHANGED, PlanningMainModel.getCurrentPlanning()));
    }

    private void onColorModelChanged(AppEvent event, ETaskDisplayMode colorMode)
    {
        UserSettings.setSetting(PlanningConfig.COLOR_MODE_SETTING_KEY, colorMode.toString());

        this.forwardToChild(event);
    }

    /**
     * When work object filter changed, it updates with value set in filter field the work object name constraint on
     * unassigned task grid store, then it reloads it.
     * 
     * @param event
     *            The event to forward.
     */
    private void onWorkObjectFilterChanged(AppEvent event)
    {
        String value = event.getData();

        this.model.updateWorkObjectConstraint(value);
        this.model.getUnassignedTaskStore().reload();
    }

    /**
     * This is a little trick needed when too much task are moved in a same time. This waiting time avoid to make one
     * request for each task moved. Saving is done only every two seconds, so requests to save task can be bulked.
     */
    private void waitForAutoSave()
    {
        if (PlanningMainModel.isAutoSave && !this.isWaitingForSave)
        {
            this.isWaitingForSave = true;
            Timer timer = new Timer() {
                @Override
                public void run()
                {
                    isWaitingForSave = false;
                    EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVE_CLICKED);
                }
            };
            timer.schedule(2000);
        }
    }
}
