package fr.hd3d.planning.ui.client.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.IdGenerator;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.ConstituentPlanningModel;
import fr.hd3d.planning.ui.client.model.util.NameSorter;
import fr.hd3d.planning.ui.client.modeldata.TimedCategoryModelData;
import fr.hd3d.planning.ui.client.view.ConstituentPlanningView;


/**
 * Controller handling constituent planning events.
 * 
 * @author HD3D
 */
public class ConstituentPlanningController extends WorkObjectPlanningController
{
    /** View displaying constituent planning widgets. */
    private final ConstituentPlanningView view;
    /** Model handling constituent planning data. */
    private final ConstituentPlanningModel model;

    public ConstituentPlanningController(ConstituentPlanningView view, ConstituentPlanningModel model)
    {
        super(view, model, EPlanningDisplayMode.CONSTITUENT);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_CATEGORIES_DATA_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.CATEGORY_CATEGORIES_LOADED);
        this.registerEventTypes(WorkObjectPlanningEvents.CATEGORY_CONSTITUENTS_LOADED);

        this.registerEventTypes(PlanningAppEvents.CONSTITUENT_MODE_CLICKED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        if (WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADED.equals(event.getType()))
        {
            this.onRootCategoriesLoaded(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_CATEGORIES_DATA_LOADED.equals(event.getType()))
        {
            this.onRootCategoriesDataLoaded(event);
        }
        else if (WorkObjectPlanningEvents.CATEGORY_CATEGORIES_LOADED.equals(event.getType()))
        {
            this.onCategoriesCategoryLoaded(event);
        }
        else if (WorkObjectPlanningEvents.CATEGORY_CONSTITUENTS_LOADED.equals(event.getType()))
        {
            this.onConstituentsCategoryLoaded(event);
        }
        else if (PlanningAppEvents.CONSTITUENT_MODE_CLICKED.equals(event.getType()))
        {
            this.onConstituentModeClicked(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When constituent mode is clicked for the first time, it loads root categories.
     * 
     * @param event
     *            Constituent mode clicked event.
     */
    private void onConstituentModeClicked(AppEvent event)
    {
        this.model.loadCategoriesForConstituentMode();
    }

    /**
     * Load sub-category and constituents for this category. Sub-category are added as task group line and children of
     * task group to load. Constituents are added as extra lines.
     * 
     * @param taskGroup
     *            The task group to load.
     */
    @Override
    protected void loadGroup(final TaskGroupModelData taskGroup)
    {
        this.model.loadCategoriesCategory(taskGroup);
    }

    private void onRootCategoriesLoaded(AppEvent event)
    {
        ServiceStore<CategoryModelData> categories = event.getData();

        if (categories.getCount() > 0)
        {
            this.model.loadCategoriesData(categories);
        }
        else
        {
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED);
        }
    }

    private void onRootCategoriesDataLoaded(AppEvent event)
    {
        FastMap<TimedCategoryModelData> categories = event.getData();
        List<TimedCategoryModelData> categoryList = new ArrayList<TimedCategoryModelData>();
        for (TimedCategoryModelData category : categories.values())
        {
            categoryList.add(category);
        }
        Collections.sort(categoryList, new NameSorter<TimedCategoryModelData>());

        for (TimedCategoryModelData category : categoryList)
        {
            TaskGroupModelData taskGroup = new TaskGroupModelData();
            taskGroup.setColor("#AAFFAA");
            taskGroup.setName(category.getName());

            taskGroup.setStartDate(category.getStartDate());
            taskGroup.setEndDate(category.getEndDate());

            if (category.getStartDate() != null && category.getEndDate() != null)
            {
                taskGroup.setId(IdGenerator.getNewId());
                taskGroup.setType(EPlanningDisplayMode.CONSTITUENT);
                taskGroup.setObjectId(category.getId());

                this.view.addTaskGroup(taskGroup);
            }
        }
        EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADING_FINISHED);
    }

    private void onCategoriesCategoryLoaded(AppEvent event)
    {
        List<TimedCategoryModelData> categories = event.getData(WorkObjectPlanningEvents.CATEGORIES_VAR);
        final TaskGroupModelData taskGroup = event.getData(WorkObjectPlanningEvents.TASK_GROUP_VAR);

        Collections.sort(categories, new NameSorter<TimedCategoryModelData>());

        for (TimedCategoryModelData category : categories)
        {
            TaskGroupModelData taskGroupChild = new TaskGroupModelData();
            taskGroupChild.setColor(TaskGroupModelData.CONSTITUENT_COLOR);
            taskGroupChild.setName(category.getName());
            taskGroupChild.setStartDate(category.getStartDate());
            taskGroupChild.setEndDate(category.getEndDate());
            taskGroupChild.setId(IdGenerator.getNewId());
            taskGroupChild.setType(EPlanningDisplayMode.CONSTITUENT);
            taskGroupChild.setObjectId(category.getId());
            taskGroupChild.setTaskGroupID(taskGroup.getId());

            this.view.addTaskGroup(taskGroupChild);
        }
        this.model.loadConstituentsCategory(taskGroup);
    }

    private void onConstituentsCategoryLoaded(AppEvent event)
    {
        final TaskGroupModelData taskGroup = event.getData();

        List<Long> constituentIds = new ArrayList<Long>();
        for (ConstituentModelData constituent : this.model.getConstituentStore().getModels())
        {
            constituentIds.add(constituent.getId());
        }

        if (constituentIds.size() > 0)
        {
            this.model.loadTask(ConstituentModelData.SIMPLE_CLASS_NAME, taskGroup, constituentIds);
        }
        else
        {
            this.view.layoutTaskGroup(taskGroup);
            this.view.setTaskGroupOpen(taskGroup);
            this.view.expandTaskGroup(taskGroup);
        }
    }

}
