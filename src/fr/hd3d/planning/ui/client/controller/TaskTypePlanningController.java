package fr.hd3d.planning.ui.client.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.reader.TaskGroupReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.IdGenerator;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.model.TaskTypePlanningModel;
import fr.hd3d.planning.ui.client.model.util.TaskTypePlanningTaskSorter;
import fr.hd3d.planning.ui.client.view.TaskTypePlanningView;


/**
 * Controller handling task type planning events.
 * 
 * @author HD3D
 */
public class TaskTypePlanningController extends WorkObjectPlanningController
{
    /** View displaying task type planning widgets. */
    protected TaskTypePlanningView view;
    /** Model handling task type planning data. */
    protected TaskTypePlanningModel model;

    /**
     * 
     * @param view
     * @param model
     */
    public TaskTypePlanningController(TaskTypePlanningView view, TaskTypePlanningModel model)
    {
        super(view, model, EPlanningDisplayMode.TASK_TYPE);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_SYNCHRONIZE_BOUNDS_CLICKED);
        this.registerEventTypes(PlanningCommonEvents.TASK_GROUP_ADD_WORKERS_CLICKED);
        this.registerEventTypes(WorkObjectPlanningEvents.TASK_GROUP_ADDING_USERS_REQUESTED);
        this.registerEventTypes(WorkObjectPlanningEvents.TASK_GROUP_BOUNDS_SYNCHRONIZED);
        this.registerEventTypes(PlanningAppEvents.TASK_GROUP_MOVED);

        this.registerEventTypes(WorkObjectPlanningEvents.ROOT_TASK_GROUPS_LOADED);
        this.registerEventTypes(PlanningAppEvents.PLANNING_OPENED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        if (PlanningCommonEvents.TASK_GROUP_ADD_WORKERS_CLICKED.equals(event.getType()))
        {
            this.onTaskGroupAddWorkersClicked(event);
        }
        else if (WorkObjectPlanningEvents.TASK_GROUP_ADDING_USERS_REQUESTED.equals(event.getType()))
        {
            this.onTaskGroupAddingUsersRequested(event);
        }
        else if (PlanningCommonEvents.TASK_GROUP_SYNCHRONIZE_BOUNDS_CLICKED.equals(event.getType()))
        {
            this.onTaskGroupSynchronizeBounds(event);
        }
        else if (WorkObjectPlanningEvents.TASK_GROUP_BOUNDS_SYNCHRONIZED.equals(event.getType()))
        {
            this.onTaskGroupBoundsSynchronized(event);
        }
        else if (PlanningAppEvents.TASK_GROUP_MOVED.equals(event.getType()))
        {
            this.onTaskGroupMoved(event);
        }
        else if (WorkObjectPlanningEvents.ROOT_TASK_GROUPS_LOADED.equals(event.getType()))
        {
            this.onRootTaskGroupsLoaded(event);
        }
        else if (PlanningAppEvents.PLANNING_OPENED.equals(event.getType()))
        {
            this.onPlanningOpened();
        }

        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When planning is opened, root task groups are reloaded.
     */
    protected void onPlanningOpened()
    {
        this.model.reloadTaskGroupStore();
    }

    /**
     * When task group add worker button is clicked, it open a dialog allowing to select workers. Selected workers are
     * then added as extra lines to the task group.
     * 
     * @param event
     *            Task group add worker event.
     */
    protected void onTaskGroupAddWorkersClicked(AppEvent event)
    {
        TaskGroupModelData taskGroup = event.getData();
        this.view.showAddWorkerDialog(taskGroup);
    }

    /**
     * 
     * @param event
     */
    protected void onTaskGroupBoundsSynchronized(AppEvent event)
    {
        this.view.resizeTaskGroup((TaskGroupModelData) event.getData());
    }

    /**
     * When task group bounds are should be synchronized, it
     * 
     * @param event
     * 
     */
    protected void onTaskGroupSynchronizeBounds(AppEvent event)
    {
        TaskGroupModelData taskGroup = this.model.getSelectedTaskGroup();
        this.model.refreshTaskGroupDates(taskGroup);
    }

    /**
     * When adding user to a task group is requested, it adds a user line to the task group.
     * 
     * @param event
     *            Adding user to task group requested event.
     */
    protected void onTaskGroupAddingUsersRequested(AppEvent event)
    {
        List<PersonModelData> personList = event.getData();

        TaskGroupItem groupItem = this.view.getTreeSelectedItem();

        if (groupItem != null)
        {
            for (PersonModelData person : personList)
            {
                if (!groupItem.getWorkerdIds().contains(person.getId()))
                {
                    ExtraLineModelData userLineModelData = new ExtraLineModelData();
                    userLineModelData.setId(IdGenerator.getNewId());
                    userLineModelData.setName(person.getName());
                    userLineModelData.setPersonID(person.getId());
                    userLineModelData.setTaskGroup(groupItem.getTaskGroup());

                    this.addWorkerLine(groupItem, userLineModelData);
                }
            }
            groupItem.setOpen(true);
            groupItem.changeState();
            this.view.layoutTaskGroup(groupItem.getTaskGroup());
            this.view.changeHeight(groupItem);
        }
        else
        {
            fr.hd3d.common.ui.client.logs.Logger
                    .error("No item group is selected, but if user reaches this point a group should be selected.");
        }
    }

    /**
     * When a task group is moved, all tasks linked to this task group are moved of the same distance.
     * 
     * @param event
     *            Task group moved event.
     */
    protected void onTaskGroupMoved(AppEvent event)
    {
        Boolean areTasksToMove = event.getData(PlanningConfig.MOVE_TASK_EVENT_VAR_NAME);
        TaskGroupModelData taskGroup = event.getData();
        this.view.setTaskGroupDirty(taskGroup);

        if (areTasksToMove != null && areTasksToMove)
        {
            Date previousStartDate = event.getData(PlanningConfig.PREVIOUS_START_DATE_EVENT_VAR_NAME);
            int nbDays = CalendarUtil.getDaysBetween(previousStartDate, taskGroup.getStartDate());
            if (this.view.isTaskGroupLoaded(taskGroup))
            {
                this.view.moveAllGroupTasks(taskGroup, nbDays);
            }
            else
            {
                this.loadTaskTypeGroup(taskGroup, nbDays);
            }
        }
    }

    /**
     * Add a worker line to <i>taskGroupItem</i>.
     * 
     * @param taskGoupItem
     *            Task group item in which line will be added.
     * @param workerLineModelData
     *            Line to add.
     */
    protected void addWorkerLine(TaskGroupItem taskGoupItem, ExtraLineModelData workerLineModelData)
    {
        this.view.addExtraLine(taskGoupItem, workerLineModelData);
    }

    /**
     * When root task groups are loaded, they are added to the planning tree and to the planning widget.
     * 
     * @param event
     *            Root task groups loaded event.
     */
    protected void onRootTaskGroupsLoaded(final AppEvent event)
    {
        List<Long> parentIds = new ArrayList<Long>();

        for (TaskGroupModelData taskGroup : this.model.getRootTaskGroups().getModels())
        {
            taskGroup.setType(EPlanningDisplayMode.TASK_TYPE);
            this.view.addTaskGroup(taskGroup);

            parentIds.add(taskGroup.getId());
        }
    }

    /**
     * Load task type data : task type sub groups and tasks linked to this task type.
     */
    @Override
    protected void loadGroup(TaskGroupModelData taskGroup)
    {
        loadTaskTypeGroup(taskGroup, null);
    }

    /**
     * Load task type sub group for a task type group.
     * 
     * @param taskGroup
     * @param nbDays
     */
    protected void loadTaskTypeGroup(final TaskGroupModelData taskGroup, final Integer nbDays)
    {
        final String taskGroupPath = PlanningMainModel.currentPlanning.getDefaultPath() + "/" + ServicesPath.TASKGROUPS;
        final ServiceStore<TaskGroupModelData> children = new ServiceStore<TaskGroupModelData>(new TaskGroupReader());

        children.setPath(taskGroupPath);
        children.addParameter(new EqConstraint("taskGroup.id", taskGroup.getId()));
        children.addParameter(new OrderBy(Arrays.asList(TaskGroupModelData.START_DATE_FIELD,
                TaskGroupModelData.NAME_FIELD)));
        children.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onTaskGroupChildrenLoaded(taskGroup, children, nbDays);
            }
        });
        children.reload();
    }

    /**
     * When task group are loaded they are added as children of <i>taskGroup</i>. Then group tasks are loaded.
     * 
     * @param taskGroup
     *            The parent task group.
     * @param children
     *            The loaded children task groups.
     * @param nbDays
     */
    protected void onTaskGroupChildrenLoaded(final TaskGroupModelData taskGroup,
            ServiceStore<TaskGroupModelData> children, final Integer nbDays)
    {
        if (children.getCount() > 0)
        {
            for (TaskGroupModelData child : children.getModels())
            {
                child.setType(EPlanningDisplayMode.TASK_TYPE);
                this.view.addTaskGroup(child);
            }
        }

        final ServiceStore<TaskModelData> tasks = new ServiceStore<TaskModelData>(new TaskReader());
        tasks.setPath(MainModel.currentProject.getDefaultPath() + "/" + ServicesPath.TASKS);

        final Long taskTypeid = taskGroup.getTaskTypeID();
        EqConstraint taskTypeConstraint = new EqConstraint("taskType.id", taskTypeid);

        Date startDate = PlanningMainModel.currentPlanning.getStartDate();
        Date endDate = PlanningMainModel.currentPlanning.getEndDate();

        OrConstraint dateConstraint = new OrConstraint(new Constraint(EConstraintOperator.btw,
                TaskModelData.START_DATE_FIELD, startDate, endDate), new Constraint(EConstraintOperator.btw,
                TaskModelData.END_DATE_FIELD, startDate, endDate));
        if (BasePlanningWidget.isPreviewMode())
        {
            tasks.addParameter(new AndConstraint(taskTypeConstraint, dateConstraint));
        }
        else
        {
            OrConstraint actualDateConstraint = new OrConstraint(new Constraint(EConstraintOperator.btw,
                    TaskModelData.ACTUAL_START_DATE_FIELD, startDate, endDate), new Constraint(EConstraintOperator.btw,
                    TaskModelData.ACTUAL_END_DATE_FIELD, startDate, endDate));
            tasks.addParameter(new AndConstraint(taskTypeConstraint, new OrConstraint(dateConstraint,
                    actualDateConstraint)));
        }

        tasks.addParameter(new OrderBy(Arrays.asList("worker.lastName", "boundEntityTaskLinks.woName")));
        tasks.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
        tasks.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onGroupTasksLoaded(taskGroup, tasks, nbDays);
            }
        });
        tasks.reload();
    }

    /**
     * When group tasks are loaded, extra lines are added for each worker to whom task is assigned. The task is added to
     * extra line.
     * 
     * @param taskGroup
     *            The task group on which line should be added.
     * @param tasks
     *            The tasks to add inside task group.
     * @param nbDays
     */
    protected void onGroupTasksLoaded(TaskGroupModelData taskGroup, ServiceStore<TaskModelData> tasks, Integer nbDays)
    {
        this.view.setTaskGroupLoaded(taskGroup);

        if (tasks.getCount() > 0)
        {
            tasks.setStoreSorter(new TaskTypePlanningTaskSorter());
            // tasks.applySort(true);
            tasks.sort(TaskModelData.START_DATE_FIELD, SortDir.ASC);
            for (TaskModelData task : tasks.getModels())
            {
                if (task.getWorkerID() != null)
                {
                    this.view.addTaskPanel(taskGroup, task);

                    TaskModelData modifiedTask = (TaskModelData) PlanningMainModel.getPlanningModifiedTask(task);
                    if (modifiedTask != null)
                        this.view.refreshTask(modifiedTask);
                }
            }
        }
        this.view.layoutTaskGroup(taskGroup);

        if (nbDays == null)
        {
            this.view.setTaskGroupOpen(taskGroup);
            this.view.expandTaskGroup(taskGroup);
        }
        else
        {
            this.view.moveAllGroupTasks(taskGroup, nbDays);
            // this.view.setTaskGroupOpen(taskGroup);
        }
    }

    /**
     * When a task is dropped, its tool tip is refreshed.
     * 
     * @param event
     *            Task dropped event.
     */
    @Override
    protected void onTaskFromGridDropped(AppEvent event)
    {
        TaskModelData task = event.getData();
        if (task != null)
            this.view.refreshToolTip(task);
    }
}
