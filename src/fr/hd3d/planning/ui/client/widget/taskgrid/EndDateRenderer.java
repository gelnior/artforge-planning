package fr.hd3d.planning.ui.client.widget.taskgrid;

import java.util.Date;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;

/**
 * Display end date if actual end date is not set. If actual end date is set and 
 * task status is OK, CLOSE or CANCELLED, it is display as a grey text.
 * 
 * @author Artforge
 */
public class EndDateRenderer implements GridCellRenderer<TaskModelData> {

	@Override
	public Object render(TaskModelData model, String property, ColumnData config,
			int rowIndex, int colIndex, ListStore<TaskModelData> store, Grid<TaskModelData> grid) 
	{

		property = TaskModelData.END_DATE_FIELD;
		if(model.getActualEndDate() != null && 
    			(ETaskStatus.OK.toString().equals(model.getStatus()) 
   			 || ETaskStatus.CLOSE.toString().equals(model.getStatus()) 
   			 || ETaskStatus.CANCELLED.toString().equals(model.getStatus())))
			property = TaskModelData.ACTUAL_END_DATE_FIELD;
		
        Object obj = model.get(property);

        String stringValue = "";
        if (obj != null)
        {
            if (obj instanceof Date)
            {
                Date date = (Date) obj;
                obj = DateFormat.FRENCH_DATE.format(date);
            }
            
            if(property == TaskModelData.ACTUAL_END_DATE_FIELD)
            	stringValue = "<div style= 'padding: 3px; color: #666;'>" + obj + "</div>";
            else 
            	stringValue = "<div style= 'padding: 3px;'>" + obj + "</div>";
        }

        return stringValue;
	}

}
