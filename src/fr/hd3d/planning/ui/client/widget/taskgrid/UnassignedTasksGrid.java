package fr.hd3d.planning.ui.client.widget.taskgrid;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridViewConfig;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.listener.KeyUpListener;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BaseToolBar;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.RefreshButton;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskTypeRenderer;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;
import fr.hd3d.common.ui.client.widget.grid.renderer.BigTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.DurationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;
import fr.hd3d.common.ui.client.widget.identitydialog.MultiTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.planning.ui.client.Planning;
import fr.hd3d.planning.ui.client.config.EUnassignedFilter;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.dnd.insert.UnassignedTaskGridDragSource;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.widget.BaseRemoteEditorGrid;


/**
 * Task grid displayed unplanned and unassigned tasks to help user to select tasks that does not appear in planning.
 * User can drag and drop tasks from this list on planning, to add task to planning.
 * 
 * @author HD3D
 */
public class UnassignedTasksGrid extends ContentPanel
{
    /** Task grid contains task to allow */
    private BaseRemoteEditorGrid<TaskModelData> taskGrid;
    /** Paging toolbar to browse data inside grid. */
    private final PagingToolBar pagingToolbar = new PagingToolBar(50);

    /** Combo box to display only unassigned tasks and/or unplanned tasks */
    private MultiTaskTypeComboBox taskTypeComboBox;

    /** Combo box to display only unassigned tasks and/or unplanned tasks */
    private final FieldComboBox assignationFilterComboBox = new FieldComboBox();

    /** Work object filter field. */
    private final TextField<String> workObjectFilterField = new TextField<String>();
    /** Refresh button used to refresh only task grid. */
    private RefreshButton<TaskModelData> refreshButton;
    /** Button that clear task type combo box when it is clicked. */
    private final ToolBarButton cancelTypeButton = new ToolBarButton(Hd3dImages.getDeleteIcon(),
            "Cancel task type filter", PlanningAppEvents.TASK_TASK_TYPE_FILTER_CHANGED);

    /** Needed to set task grid a drag source to drag and drop task from grid to planning. */
    private UnassignedTaskGridDragSource dragSource;

    /**
     * Default constructor.
     * 
     * @param store
     *            The store used by the grid to display data.
     */
    public UnassignedTasksGrid(ServicesPagingStore<TaskModelData> store, ServiceStore<TaskTypeModelData> taskTypeStore)
    {
        this.setStyles();
        this.setPagingToolbar();
        this.createTaskDataGrid(store);
        this.setToolBar(taskTypeStore);
        this.setListeners();

        this.setGridView();
        this.setGridOrderColumns();
    }

    /**
     * @return Drag source object.
     */
    public UnassignedTaskGridDragSource getDragSource()
    {
        return dragSource;
    }

    /**
     * Clear displayed value on task type combo.
     */
    public void clearTaskTypeCombo()
    {
        this.taskTypeComboBox.setRawValue(null);
    }

    public void enableGrid()
    {
        this.unmask();
        this.pagingToolbar.enable();
    }


    /**
     * Before a task date is going to be modified, if actual start is set, start date cannot be modified. 
     * If actual end date is set and task status is OK, CANCEL, CLOSED end date cannot be modified.
     * 
     * @param ge
     *            The grid event notifying about cell edition.
     */
    protected void onBeforeEdit(GridEvent<TaskModelData> ge)
    {
		boolean isEditable = true;
		
    	if(ge.getProperty() == TaskModelData.START_DATE_FIELD) 
    	{
            for (TaskModelData task : taskGrid.getSelection())
            {
            	if(task.getActualStartDate() != null) 
            	{
            		isEditable = false;
            		break;
            	}	
            }            
    	}
    	
    	else if(ge.getProperty() == TaskModelData.END_DATE_FIELD) 
    	{
            for (TaskModelData task : taskGrid.getSelection())
            {
                if(task.getActualEndDate() != null && 
            	   (ETaskStatus.OK.toString().equals(task.getStatus()) 
            	    || ETaskStatus.CLOSE.toString().equals(task.getStatus()) 
                    || ETaskStatus.CANCELLED.toString().equals(task.getStatus())))
            	{
            		Logger.log(task.getStatus());
            		isEditable = false;
            		break;
            	}	
            }            
    	}
    	
        if(!isEditable) 
        {
        	ge.setCancelled(true);
        }
    }
    
    /**
     * When a task is modified, plannings are notified (to make it move to its new place). If end date is not set and if
     * there is a start date with a duration, it automatically sets the end date by adding duration to start date and by
     * skipping weekends.
     * 
     * @param ge
     *            The grid event notifying about cell edition.
     */
    protected void onAfterEdit(GridEvent<TaskModelData> ge)
    {
        for (TaskModelData task : taskGrid.getSelection())
        {
            Record rec = taskGrid.getStore().getRecord(task);
            if (ge.getModel() != task)
            {
                rec.set(ge.getProperty(), ge.getValue());
            }

            DateWrapper endDate = task.calculateEndDate();
            if (endDate != null)
                rec.set(TaskModelData.END_DATE_FIELD, endDate.asDate());

            AppEvent event = new AppEvent(PlanningCommonEvents.TASK_MOVED, task);
            event.setData(PlanningConfig.PLANNING_TYPE_EVENT_VAR_NAME, EPlanningDisplayMode.ALL);
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * Set panel and grid styles.
     */
    private void setStyles()
    {
        this.setHeading("Pending tasks");

        this.setBorders(false);
        this.setBodyBorder(true);
        this.setHeaderVisible(true);
        this.setScrollMode(Scroll.NONE);
        this.setLayout(new FitLayout());
    }

    /**
     * Create a task data grid
     * 
     * @param store
     */
    private void createTaskDataGrid(ServicesPagingStore<TaskModelData> store)
    {
        this.refreshButton = new RefreshButton<TaskModelData>(store);
        this.taskGrid = new BaseRemoteEditorGrid<TaskModelData>(store, getColumnModel());
        TaskGridWidgetView view = new TaskGridWidgetView(store, pagingToolbar);
        PagingLoader<?> loader = (PagingLoader<?>) store.getLoader();

        GridViewConfig viewConfig = new GridViewConfig() {
            @Override
            public String getRowStyle(ModelData model, int rowIndex, ListStore<ModelData> ds)
            {
                String style = super.getRowStyle(model, rowIndex, ds);
                TaskModelData task = (TaskModelData) model;
                if (task.getWorkerID() == null)
                {
                    style = "NotPlannedTaskStyle";
                }
                return style;
            }
        };

        this.taskGrid.setDefaultSort(CollectionUtils.asList("taskType.name", "name"));
        this.pagingToolbar.bind(loader);
        view.setAutoFill(false);

        view.setViewConfig(viewConfig);
        this.taskGrid.setView(view);
        this.taskGrid.getSelectionModel().setSelectionMode(SelectionMode.SIMPLE);
        this.taskGrid.setBorders(false);
        this.taskGrid.setLoadMask(true);
        this.taskGrid.setAutoExpandColumn(TaskModelData.WORK_OBJECT_NAME_FIELD);
        this.taskGrid.setAutoExpandMax(2000);

        this.dragSource = new UnassignedTaskGridDragSource(taskGrid);
        this.add(taskGrid);
        this.layout(true);
    }

    /**
     * Build top toolbar containing grid filters.
     * 
     * @param taskTypeStore
     */
    private void setToolBar(ServiceStore<TaskTypeModelData> taskTypeStore)
    {
        BaseToolBar toolBar = new BaseToolBar();

        toolBar.add(refreshButton);
        toolBar.add(new FillToolItem());

        FieldModelData allField = new FieldModelData(1, "All", EUnassignedFilter.ALL);
        this.assignationFilterComboBox.getStore().add(allField);
        this.assignationFilterComboBox.getStore()
                .add(new FieldModelData(2, "Unassigned", EUnassignedFilter.UNASSIGNED));
        this.assignationFilterComboBox.getStore().add(new FieldModelData(3, "Unplanned", EUnassignedFilter.UNPLANNED));
        this.assignationFilterComboBox.setValue(allField);
        this.assignationFilterComboBox.addSelectionChangedListener(new EventSelectionChangedListener<FieldModelData>(
                PlanningAppEvents.TASK_FILTER_CHANGED));
        toolBar.add(this.assignationFilterComboBox);

        this.taskTypeComboBox = new MultiTaskTypeComboBox(taskTypeStore);
        this.taskTypeComboBox.setSelectionChangedEvent(PlanningAppEvents.TASK_TASK_TYPE_FILTER_CHANGED);
        toolBar.add(this.taskTypeComboBox);

        toolBar.add(this.cancelTypeButton);

        this.workObjectFilterField.addKeyListener(new KeyUpListener(PlanningAppEvents.TASK_WORK_OBJECT_FILTER_CHANGED));
        toolBar.add(this.workObjectFilterField);

        this.setTopComponent(toolBar);
    }

    /** Place at bottom grid paging tool bar. */
    private void setPagingToolbar()
    {
        this.setBottomComponent(pagingToolbar);
    }

    /**
     * Listen for edition in task grid. If task is edited, it changes its representations inside plannings.
     */
    private void setListeners()
    {
        this.taskGrid.addListener(Events.AfterEdit, new Listener<GridEvent<TaskModelData>>() {
            public void handleEvent(GridEvent<TaskModelData> ge)
            {
                onAfterEdit(ge);
            }
        });

        this.taskGrid.addListener(Events.BeforeEdit, new Listener<GridEvent<TaskModelData>>() {
            public void handleEvent(GridEvent<TaskModelData> ge)
            {
                onBeforeEdit(ge);
            }
        });
    }

    /**
     * Initialize grid order column conversion (order field is not the same as displayed column).
     */
    private void setGridOrderColumns()
    {
        this.taskGrid.setOrderColumn(TaskModelData.WORKER_NAME_FIELD, "worker.lastName");
        this.taskGrid.setOrderColumn(TaskModelData.TASK_TYPE_NAME_FIELD,
                CollectionUtils.asList("taskType.name", "name"));
        this.taskGrid.setOrderColumn(TaskModelData.WORK_OBJECT_NAME_FIELD,
                CollectionUtils.asList("taskType.name", "name"));
    }

    /**
     * Set a multi-selection grid selection model on grid by setting a specific grid view on grid.
     */
    private void setGridView()
    {
        SelectableGridView gridView = new SelectableGridView();
        this.taskGrid.setView(gridView);
        this.taskGrid.setSelectionModel(new MultiLineCellSelectionModel<TaskModelData>(gridView, null));
        this.taskGrid.getSelectionModel().setSelectionMode(SelectionMode.MULTI);
    }

    /**
     * @return The list of selected task types inside task type combo box.
     */
    public List<TaskTypeModelData> getSelectTaskTypes()
    {
        return this.taskTypeComboBox.getSelection();
    }

    /**
     * @return Column model for task grid.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columnConfigList = new ArrayList<ColumnConfig>();
        GridCellRenderer<TaskModelData> workerNameRenderer = new GridCellRenderer<TaskModelData>() {
            public Object render(TaskModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                    ListStore<TaskModelData> store, Grid<TaskModelData> grid)
            {
                String workerName = model.get(property);
                if (workerName == null)
                {
                    workerName = "<i>" + Planning.CONSTANTS.unAssigned() + "</i>";
                }

                return "<div style='padding: 3px;'>" + workerName + "</div>";

            }
        };

        ColumnConfig path = GridUtils.addColumnConfig(columnConfigList, TaskModelData.WORK_OBJECT_PARENTS_NAME_FIELD,
                "Path");
        path.setSortable(false);
        path.setHidden(true);
        path.setRenderer(new PaddingTextRenderer<TaskModelData>());

        ColumnConfig workObject = GridUtils.addColumnConfig(columnConfigList, TaskModelData.WORK_OBJECT_NAME_FIELD,
                "Work Object", 120);
        workObject.setSortable(false);
        workObject.setRenderer(new BigTextRenderer<TaskModelData>());

        ColumnConfig taskType = GridUtils.addColumnConfig(columnConfigList, TaskModelData.TASK_TYPE_NAME_FIELD, "Type");
        path.setSortable(false);
        taskType.setRenderer(new TaskTypeRenderer());

        ColumnConfig userNameColumnConfig = new ColumnConfig();
        userNameColumnConfig.setId("workerName");
        userNameColumnConfig.setHeader(Planning.CONSTANTS.workerField());
        userNameColumnConfig.setWidth(180);
        userNameColumnConfig.setRenderer(workerNameRenderer);
        userNameColumnConfig.setSortable(false);
        columnConfigList.add(userNameColumnConfig);
        ColumnConfig startDateColumnConfig = new ColumnConfig();

        ColumnConfig durationColumnConfig = new ColumnConfig();
        durationColumnConfig.setId("duration");
        durationColumnConfig.setHeader(Planning.CONSTANTS.durationField());
        durationColumnConfig.setWidth(60);
        durationColumnConfig.setRenderer(new DurationRenderer());
        durationColumnConfig.setEditor(new CellEditor(new TextField<Long>()));
        durationColumnConfig.setEditor(new CellEditor(new DurationField()));
        durationColumnConfig.setSortable(false);
        columnConfigList.add(durationColumnConfig);

        startDateColumnConfig.setId("startDate");
        startDateColumnConfig.setHeader(Planning.CONSTANTS.startDateField());
        startDateColumnConfig.setWidth(70);
        startDateColumnConfig.setRenderer(new StartDateRenderer());
        startDateColumnConfig.setEditor(new CellEditor(new DateField()));
        startDateColumnConfig.setSortable(false);
        columnConfigList.add(startDateColumnConfig);

        ColumnConfig endDateColumnConfig = new ColumnConfig();
        endDateColumnConfig.setId("endDate");
        endDateColumnConfig.setHeader(Planning.CONSTANTS.endDateField());
        endDateColumnConfig.setWidth(70);
        endDateColumnConfig.setRenderer(new EndDateRenderer());
        endDateColumnConfig.setEditor(new CellEditor(new DateField()));
        endDateColumnConfig.setSortable(false);
        columnConfigList.add(endDateColumnConfig);

        ColumnModel taskGridColumnModel = new ColumnModel(columnConfigList);

        return taskGridColumnModel;
    }
}
