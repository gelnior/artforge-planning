package fr.hd3d.planning.ui.client.widget.taskgrid;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.grid.BaseGridView;


public class TaskGridWidgetView extends BaseGridView
{

    private final ListStore<TaskModelData> store;

    private final PagingToolBar pagingToolBar;

    public TaskGridWidgetView(ListStore<TaskModelData> store, PagingToolBar pagingToolBar)
    {

        this.store = store;
        this.pagingToolBar = pagingToolBar;
        // this.setEmptyText("No rows available on the server.");
    }

    @Override
    protected void onHeaderClick(Grid<ModelData> grid, int column)
    {
        if (!headerDisabled && cm.isSortable(column))
        {
            String field = cm.getDataIndex(column);
            SortInfo sortInfo = new SortInfo(ds.getSortField(), ds.getSortDir());
            SortDir sortDir = null;

            if (sortInfo.getSortField() != null && !sortInfo.getSortField().equals(field))
            {
                sortInfo.setSortDir(SortDir.NONE);
            }

            switch (sortInfo.getSortDir())
            {
                case ASC:
                    sortDir = SortDir.DESC;
                    break;
                case NONE:
                case DESC:
                    sortDir = SortDir.ASC;
                    break;
                default:
                    sortDir = SortDir.ASC;
            }
            this.doSort(column, sortDir);
        }
    }

    @Override
    protected void doSort(int colIndex, SortDir sortDir)
    {
        String field = cm.getDataIndex(colIndex);
        SortInfo sortInfo = new SortInfo(ds.getSortField(), ds.getSortDir());

        sortInfo.setSortField(field);
        sortInfo.setSortDir(sortDir);

        if (this.pagingToolBar.getTotalPages() > 1)
        {
            if (TaskModelData.WORKER_NAME_FIELD.equals(field))
            {
                sortInfo.setSortField(TaskModelData.WORKER_NAME_FIELD);
            }
            this.loadData(sortInfo);
        }
        else
        {
            super.doSort(colIndex, sortInfo.getSortDir());
        }
    }

    public void loadData(SortInfo sortInfo)
    {
        BasePagingLoadConfig config = null;
        if (sortInfo != null)
        {
            PagingLoader<?> loader = (PagingLoader<?>) this.store.getLoader();
            config = new BasePagingLoadConfig(0, loader.getLimit());
            config.setSortInfo(sortInfo);
        }
        this.store.getLoader().load(config);
    }

}
