package fr.hd3d.planning.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;


public class BaseRemoteEditorGrid<M extends Hd3dModelData> extends BaseEditorGrid<M>
{
    protected OrderBy orderBy = new OrderBy("");
    protected FastMap<List<String>> orderColumns = new FastMap<List<String>>();
    protected FastMap<SortDir> orderDirs = new FastMap<SortDir>();

    public BaseRemoteEditorGrid(BaseStore<M> baseStore, ColumnModel cm)
    {
        super(baseStore, cm);

        this.setSortListener();

        baseStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onStoreLoaded();
            }
        });
    }

    private void onStoreLoaded()
    {
        this.unmask();
    }

    private void setSortListener()
    {
        this.addListener(Events.HeaderClick, new Listener<GridEvent<M>>() {
            public void handleEvent(GridEvent<M> ge)
            {
                onSortChange(ge);
                ge.setCancelled(true);
                ge.cancelBubble();
            }
        });

        this.getServiceStore().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent event)
            {
                unmask();
            }
        });
    }

    private void onSortChange(GridEvent<M> ge)
    {
        String property = this.getColumnModel().getColumn(ge.getColIndex()).getDataIndex();
        List<String> changeColumns = new ArrayList<String>();

        if (this.getColumnModel().getColumn(ge.getColIndex()).isSortable())
        {
            if (SortDir.DESC == orderDirs.get(property))
            {
                orderDirs.put(property, SortDir.ASC);
            }
            else
            {
                orderDirs.put(property, SortDir.DESC);
            }
            ((SelectableGridView) this.getView()).updateSortIcon(ge.getColIndex(), orderDirs.get(property));

            if (orderColumns.get(property) != null)
            {
                changeColumns = orderColumns.get(property);

                if (!this.getServiceStore().containsParameter(orderBy))
                    this.getServiceStore().addParameter(orderBy);

                this.orderBy.clearColumns();

                for (String sortColumn : changeColumns)
                {
                    if (SortDir.DESC == orderDirs.get(property))
                        sortColumn = "-" + sortColumn;
                    this.orderBy.addColumn(sortColumn);
                }
            }
            else
            {
                if (SortDir.DESC == orderDirs.get(property))
                    property = "-" + property;

                this.orderBy.clearColumns();
                this.orderBy.addColumn(property);
            }

            this.mask("Loading...");
            this.getServiceStore().reload();
        }
    }

    public void setOrderColumn(String field, String sortColumn)
    {
        this.orderColumns.put(field, CollectionUtils.asList(sortColumn));
    }

    public void setOrderColumn(String field, List<String> sortColumns)
    {
        this.orderColumns.put(field, sortColumns);
    }

    public void setDefaultSort(List<String> columnName)
    {
        if (!this.getServiceStore().containsParameter(orderBy))
            this.getServiceStore().addParameter(orderBy);
        this.orderBy.clearColumns();

        this.orderBy.addColumns(columnName);
    }
}
