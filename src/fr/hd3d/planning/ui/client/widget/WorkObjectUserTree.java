package fr.hd3d.planning.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.widget.ToggleButtonList;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.BasePlanningTree;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.PlanningTreePanel;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.ExtraLineItem;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.NoTaskGroupWidget;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;
import fr.hd3d.planning.ui.client.constants.PlanningLocaleConstants;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.model.PlanningPanelModel;


public class WorkObjectUserTree extends PlanningTreePanel
{

    private boolean isInit = false;
    private final NoTaskGroupWidget noTaskGroupWidget = new NoTaskGroupWidget();

    private static final PlanningLocaleConstants CONSTANTS = GWT.create(PlanningLocaleConstants.class);

    private static final String showConstituentPlanningString = CONSTANTS.showConstituentPlanning();
    private static final String showShotPlanningString = CONSTANTS.showShotPlanning();
    private static final String showTaskTypePlanningString = "Show task type planning";
    private static final String showTaskTypeShotPlanningString = "Show shot task type planning";

    private final ToggleButtonList displayButtonList = new ToggleButtonList();
    private final ToolBarToggleButton taskTypeModeButton = new ToolBarToggleButton(
            Hd3dImages.getTaskTypeConstituentIcon(), showTaskTypePlanningString,
            PlanningAppEvents.TASK_TYPE_MODE_CLICKED);
    private final ToolBarToggleButton taskTypeShotModeButton = new ToolBarToggleButton(
            Hd3dImages.getTaskTypeShotIcon(), showTaskTypeShotPlanningString,
            PlanningAppEvents.TASK_TYPE_SHOT_MODE_CLICKED);
    private final ToolBarToggleButton constituentModeButton = new ToolBarToggleButton(Hd3dImages.getConsituentIcon(),
            showConstituentPlanningString, PlanningAppEvents.CONSTITUENT_MODE_CLICKED);
    private final ToolBarToggleButton shotModeButton = new ToolBarToggleButton(Hd3dImages.getShotIcon(),
            showShotPlanningString, PlanningAppEvents.SHOT_MODE_CLICKED);

    private final BasePlanningTree taskTypeTree = new BasePlanningTree();
    private final BasePlanningTree taskTypeShotTree = new BasePlanningTree();
    private final BasePlanningTree constituentTree = new BasePlanningTree();
    private final BasePlanningTree shotTree = new BasePlanningTree();

    private final FastMap<BasePlanningTree> treeMap = new FastMap<BasePlanningTree>();

    private BasePlanningTree lastSelectedTree = taskTypeTree;

    public WorkObjectUserTree()
    {
        super();

        this.setToolbar();

        this.treeMap.put(EPlanningDisplayMode.TASK_TYPE.toString(), this.taskTypeTree);
        this.treeMap.put(EPlanningDisplayMode.TASK_TYPE_SHOT.toString(), this.taskTypeShotTree);
        this.treeMap.put(EPlanningDisplayMode.CONSTITUENT.toString(), this.constituentTree);
        this.treeMap.put(EPlanningDisplayMode.SHOT.toString(), this.shotTree);
    }

    private void setToolbar()
    {
        this.getToolBar().setVisible(true);

        this.displayButtonList.addButton(this.taskTypeModeButton);
        this.displayButtonList.addButton(this.taskTypeShotModeButton);
        this.displayButtonList.addButton(this.constituentModeButton);
        this.displayButtonList.addButton(this.shotModeButton);
        this.displayButtonList.addListToToolBar(this.getToolBar());
        this.displayButtonList.toggle(this.taskTypeModeButton);
    }

    public void init(PlanningPanelModel model)
    {
        if (!isInit)
        {
            this.createWorkObjectUserTree();
            this.isInit = true;
        }
    }

    private void createWorkObjectUserTree()
    {
        this.getScrollPanel().add(taskTypeTree);
    }

    public BasePlanningTree getTaskTypeTree()
    {
        return taskTypeTree;
    }

    public void buildTaskGroupTree(List<TaskGroupModelData> data)
    {
        if (data.size() > 0)
        {
            this.remove(this.noTaskGroupWidget);
            this.taskTypeTree.buildTaskGroupTree(data);
        }
    }

    public ExtraLineItem addExtraLine(TaskGroupItem taskGroupItem, ExtraLineModelData extraLine)
    {
        return this.taskTypeTree.addExtraLine(taskGroupItem, extraLine);
    }

    public void removeRootTaskGroup(TaskGroupItem taskGroupItem)
    {
        BasePlanningTree tree = this.treeMap.get(taskGroupItem.getTaskGroup().getType().toString());
        if (tree.getWidgetIndex(taskGroupItem) >= 0)
            tree.remove(taskGroupItem);
    }

    public void setDisplayMode(EPlanningDisplayMode mode)
    {
        BasePlanningTree selectedTree = this.treeMap.get(mode.toString());

        this.getScrollPanel().remove(this.lastSelectedTree);
        this.getScrollPanel().add(selectedTree);
        this.lastSelectedTree = selectedTree;
    }

    public void resetDisplayButtons()
    {
        this.displayButtonList.toggle(this.taskTypeModeButton);
    }

    public BasePlanningTree getCustomTree(EPlanningDisplayMode mode)
    {
        return this.treeMap.get(mode.toString());
    }
}
