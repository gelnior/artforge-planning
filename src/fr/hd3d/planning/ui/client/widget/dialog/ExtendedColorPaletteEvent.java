package fr.hd3d.planning.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.google.gwt.user.client.Event;


/**
 * ColorPalette event type.
 */
public class ExtendedColorPaletteEvent extends ComponentEvent
{

    private ExtendedColorPalette colorPalette;
    private String color;

    public ExtendedColorPaletteEvent(ExtendedColorPalette colorPalette)
    {
        super(colorPalette);
        setColorPalette(colorPalette);
    }

    public ExtendedColorPaletteEvent(ExtendedColorPalette colorPalette, Event event)
    {
        super(colorPalette, event);
        setColorPalette(colorPalette);
    }

    /**
     * Returns the selected color.
     * 
     * @return the selected color
     */
    public String getColor()
    {
        return color;
    }

    /**
     * Sets the selected color.
     * 
     * @param color
     *            the selected color
     */
    public void setColor(String color)
    {
        this.color = color;
    }

    /**
     * Sets the color palette.
     * 
     * @param colorPalette
     *            the color palette
     */
    public void setColorPalette(ExtendedColorPalette colorPalette)
    {
        this.colorPalette = colorPalette;
    }

    /**
     * Returns the color palette.
     * 
     * @return the color palette
     */
    public ExtendedColorPalette getColorPalette()
    {
        return colorPalette;
    }

}
