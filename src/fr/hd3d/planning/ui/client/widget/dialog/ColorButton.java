package fr.hd3d.planning.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.ColorPalette;
import com.extjs.gxt.ui.client.widget.button.Button;


/**
 * Button that allows to select color inside a palette and that have a field behavior (from user point of view).
 * 
 * @author HD3D
 */
public class ColorButton extends Button
{

    /** Selected color */
    private String selectedColor;

    /**
     * Constructor set styles and menu.
     */
    public ColorButton()
    {
        super();

        this.setWidth(40);

        this.setColorMenu();
    }

    /**
     * @return Currently selected color.
     */
    public String getSelectedColor()
    {
        return this.selectedColor;
    }

    /**
     * Set currently selected color.
     * 
     * @param color
     *            The color to select.
     */
    public void setValue(String color)
    {
        this.selectedColor = color;
        setText("<div style='width:100%; height:100%; text-align:center; background-color:" + color + ";'>" + "&nbsp;"
                + "</div>");
    }

    /**
     * Set color menu displayed when user click on the button.
     */
    private void setColorMenu()
    {
        ExtendedColorMenu menu = new ExtendedColorMenu();

        menu.getColorPalette().addListener(Events.Select, new Listener<ComponentEvent>() {

            public void handleEvent(ComponentEvent be)
            {
                setValue(((ColorPalette) be.getComponent()).getValue());
            }
        });
        this.setMenu(menu);
    }
}
