package fr.hd3d.planning.ui.client.widget.dialog;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PutModelDataCallback;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.planning.ui.client.Planning;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;


/**
 * Dialog allowing user to modify or create a milestone.
 * 
 * @author HD3D
 */
public class MilestoneEditorDialog extends FormDialog
{
    /**
     * Dialog instance reused each time dialog is displayed.
     */
    private static MilestoneEditorDialog instance;

    /** Field for milestone title. */
    private TextField<String> titleField;
    /** Field for milestone description. */
    private TextArea descriptionEditor;
    /** Field for milestone date. */
    private DateField dateField;
    /** Field for selected color. */
    final ColorButton colorField = new ColorButton();

    /** The milestone edited. */
    private MilestoneModelData milestone;

    /**
     * @return The milestone dialog instance to reuse for each displaying.
     */
    public static MilestoneEditorDialog getInstance(MilestoneModelData milestone)
    {
        if (instance == null)
        {
            instance = new MilestoneEditorDialog(milestone);
        }
        instance.setMilestone(milestone);
        return instance;
    }

    /**
     * Set edited milestone.
     * 
     * @param milestone
     *            The milestone to set.
     */
    private void setMilestone(MilestoneModelData milestone)
    {
        this.milestone = milestone;
    }

    /**
     * Fields are cleared when dialog is hidden.
     */
    @Override
    public void hide()
    {
        reset();
        super.hide();
    }

    /**
     * When dialog is shown, all field are initialized with milestone fields, ok button is disabled if title is empty
     * and focus is set on title field.
     */
    @Override
    public void show()
    {
        super.show();
        if (milestone != null)
        {
            this.titleField.setValue(milestone.getTitle());
            this.dateField.setValue(milestone.getDate());
            this.descriptionEditor.setValue(milestone.getDescription());
            this.colorField.setValue(milestone.getColor());

            if (milestone.getId() != null)
                this.dateField.enable();
            else
                this.dateField.disable();
        }
        this.onFieldChanged();
        this.titleField.focus();
    }

    /**
     * When OK button is clicked, it shows saving indicator, disable OK button, then milestone is saved. When save ends,
     * a MILESTONE_SAVED event is dispatched to controllers and dialog is hidden. When saving ends or if error occurs,
     * OK button is enabled and saving indicator is hidden.
     */
    @Override
    protected void onOkClicked()
    {
        this.showSaving();
        this.okButton.disable();
        if (this.milestone.getId() == null)
        {
            this.updateMileStoneWithFields();
            String path = PlanningMainModel.getCurrentPlanning().getDefaultPath() + "/" + ServicesPath.MILESTONES;
            this.milestone.setDefaultPath(path);
            this.milestone.set(MilestoneModelData.IS_NEW, Boolean.TRUE);
            this.milestone.save(new PostModelDataCallback(milestone) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);

                    onSaveSuccess();
                }

                @Override
                protected void onError()
                {
                    onSaveFailed();
                }
            });
        }
        else
        {
            this.milestone.set(MilestoneModelData.IS_NEW, Boolean.FALSE);
            this.milestone.set(MilestoneModelData.PREVIOUS_DATE, this.milestone.getDate());
            this.updateMileStoneWithFields();

            String path = PlanningMainModel.getCurrentPlanning().getDefaultPath() + "/" + ServicesPath.MILESTONES;
            path += this.milestone.getId();
            this.milestone.setDefaultPath(path);
            this.milestone.save(new PutModelDataCallback(milestone, null) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    onSaveSuccess();
                }

                @Override
                protected void onError()
                {
                    onSaveFailed();
                }
            });
        }
    }

    /**
     * Update edited milestone with current field values.
     */
    private void updateMileStoneWithFields()
    {
        this.milestone.setTitle(titleField.getValue());
        this.milestone.setDescription(descriptionEditor.getValue());
        this.milestone.setDate(dateField.getValue());
        this.milestone.setColor(this.colorField.getSelectedColor());
        this.milestone.setPlanningID(PlanningMainModel.currentPlanning.getId());

    }

    /**
     * When save succeeds, it hides saving indicator and dialog, enable OK button and dispatches MILESTONE_SAVED event
     * to controllers.
     */
    protected void onSaveSuccess()
    {
        this.hide();
        this.hideSaving();
        this.okButton.enable();
        EventDispatcher.forwardEvent(okEvent, milestone);
    }

    /**
     * When save failed, saving indicator is hidden and OK button is enabled.
     */
    protected void onSaveFailed()
    {
        this.hideSaving();
        this.okButton.enable();
    }

    /**
     * When CANCEL button is clicked, the dialog is hidden.
     */
    @Override
    protected void onCancelClicked()
    {
        this.hide();
    }

    /**
     * When a field changed it checks that title is not empty and disable OK button if it is.
     */
    @Override
    protected void onFieldChanged()
    {
        if (Util.isEmptyString(titleField.getValue()))
        {
            this.okButton.disable();
        }
        else
        {
            this.okButton.enable();
        }
    }

    /**
     * Constructor : set up dialog styles and milestone form.
     * 
     * @param event
     *            Event to forward when creation/update is confirmed.
     */
    private MilestoneEditorDialog(MilestoneModelData milestone)
    {
        super(PlanningCommonEvents.MILESTONE_SAVED, Planning.COMMON_CONSTANTS.AddMileStone());

        this.milestone = milestone;

        this.setStyles();
        this.setForm();
    }

    /**
     * Set dialog styles.
     */
    private void setStyles()
    {
        this.setWidth(600);
        this.setAutoHeight(true);
        this.setClosable(false);
        this.setResizable(false);
    }

    /**
     * Build each field of the milestone form.
     */
    private void setForm()
    {
        this.titleField = new TextField<String>();
        this.titleField.setOriginalValue("");
        this.titleField.setFieldLabel(Planning.COMMON_CONSTANTS.Title());
        this.titleField.setMessageTarget("tooltip");
        this.addKeyField(titleField, new FormData("100%"));

        this.dateField = new DateField();
        this.dateField.setFieldLabel(Planning.COMMON_CONSTANTS.Date());
        this.dateField.setFormatValue(true);
        this.dateField.setMessageTarget("tooltip");
        this.dateField.getPropertyEditor().setFormat(DateTimeFormat.getFormat("dd/MM/yyyy"));
        this.dateField.disable();
        this.panel.add(dateField, new FormData("100%"));

        ColumnLayout layout = new ColumnLayout();
        LayoutContainer container = new LayoutContainer(layout);
        container.addStyleName("x-form-item");
        Label colorLabel = new Label("Color: ");
        colorLabel.setWidth(80);
        colorLabel.addStyleName("x-form-item-label");
        container.add(colorLabel);
        container.add(colorField);
        this.panel.add(container);

        this.descriptionEditor = new TextArea();
        this.descriptionEditor.setFieldLabel(Planning.COMMON_CONSTANTS.Description());
        this.descriptionEditor.setHeight(200);
        this.panel.add(descriptionEditor, new FormData("100%"));
    }

    /**
     * Clear milestone form field values.
     */
    private void reset()
    {
        this.titleField.clear();
        this.dateField.clear();
        this.descriptionEditor.clear();
        this.colorField.setText("");
    };
}
