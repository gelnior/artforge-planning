package fr.hd3d.planning.ui.client.dnd;

import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.util.Rectangle;

import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.PlanningAbsolutePanel;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;


/**
 * Utility methods to calculate right coordinate for task drag and dropping.
 * 
 * @author HD3D
 */
public class DragUtils
{

    /**
     * @param event
     *            Drag and drop event.
     * @param extraLine
     * @param planningPanel
     * @param taskDragged
     * @return X coordinate depending on mouse position.
     */
    public static int getXCoordinate(DNDEvent event, TaskBar taskDragged, PlanningAbsolutePanel planningPanel,
            ExtraLine extraLine)
    {
        int panelWidth = taskDragged.getWidth();
        int x = event.getDragEvent().getX();
        Rectangle containerRectangle = planningPanel.getBounds(false);
        x -= containerRectangle.x;
        x -= panelWidth / 2;
        x = (int) Math.floor((float) x / BasePlanningWidget.getCellWidth()) * BasePlanningWidget.getCellWidth();

        // x = BarUtils.skipMoveWeekend(x);

        if (x < 0)
            x = 0;
        if (x + panelWidth > extraLine.getWidth())
            x = extraLine.getWidth() - panelWidth;

        return x + 1;
    }
}
