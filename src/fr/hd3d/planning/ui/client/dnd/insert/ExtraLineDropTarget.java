package fr.hd3d.planning.ui.client.dnd.insert;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.dnd.DropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Util;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.util.BarUtils;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.dnd.DragUtils;
import fr.hd3d.planning.ui.client.dnd.move.TaskDraggable;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.view.TaskTypePlanningView;


/**
 * User drop target handles DND actions when task is drag from task grid and drop inside planning extra lines.
 * 
 * @author HD3D
 */
public class ExtraLineDropTarget extends DropTarget
{
    /** Planning view */
    private final TaskTypePlanningView planningView;

    /** Line on which task is dropped. */
    private final ExtraLine extraLine;

    /** Task bars actually dragged. */
    private final List<TaskBar> taskBars = new ArrayList<TaskBar>();

    /** Task bars removed when task bars entered in task type group. */
    private static final FastMap<TaskBar> initialTaskBars = new FastMap<TaskBar>();

    /**
     * Previous position is used to know if bar is moving from a week-end or not (in this case, the bar should be
     * reduced).
     */
    int previousX = 0;

    /**
     * Constructor.
     * 
     * @param target
     *            Line on which task is dropped.
     * @param planningView
     *            Planning view.
     */
    public ExtraLineDropTarget(ExtraLine target, TaskTypePlanningView planningView)
    {
        super(target);

        this.planningView = planningView;
        extraLine = target;
    }

    /**
     * On drag enter, if task is correct, it inserts task inside line.
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onDragEnter(DNDEvent event)
    {
        UnassignedTaskGridDragSource dragSource = (UnassignedTaskGridDragSource) event.getDragSource();
        event.getStatus().setVisible(false);

        if (dragSource != null && dragSource.isStarted())
        {
            List<TaskModelData> list = (List<TaskModelData>) event.getData();
            if (list.size() > 0)
            {
                super.onDragEnter(event);

                for (TaskModelData task : list)
                {
                    Long taskTaskTypeId = task.getTaskTypeId();
                    Long taskGroupTaskTypeId = this.extraLine.getTaskGroup().getTaskTypeID();
                    if (task.getTaskTypeId() != null && this.extraLine.getTaskGroup() != null
                            && extraLine.getTaskGroup().getId() != null
                            && taskTaskTypeId.longValue() == taskGroupTaskTypeId.longValue())
                    {
                        TaskBar removedTaskBar = this.planningView.removeTaskBar(task);
                        if (removedTaskBar != null)
                            initialTaskBars.put(task.getId().toString(), removedTaskBar);

                        TaskBar taskBar = this.insertTaskPanelInExtraLine(event, task);
                        this.previousX = BarUtils.getTaskBarRight(taskBar);
                    }
                }
                this.previousX = 0;
            }
        }
        else
        {
            event.cancelBubble();
        }
    }

    /**
     * Create a new task panel inside current extra line.
     * 
     * @param task
     *            The task linked to task panel.
     */
    private TaskBar insertTaskPanelInExtraLine(DNDEvent event, TaskModelData task)
    {
        TaskBar taskBar = TaskBar.createInstance(BasePlanningWidget.timeLine, task, task.getWorkObjectName(),
                this.planningView, PlanningMainModel.displayMode, true);
        TaskDraggable.createDraggableResizable(taskBar, this.planningView, EPlanningDisplayMode.TASK_TYPE);

        int durationDays = 1;
        if (task.getDuration() != null)
        {
            if (task.getDuration() > DatetimeUtil.DAY_SECONDS)
            {
                float nbDaysFloat = task.getDuration() / (float) DatetimeUtil.DAY_SECONDS;
                durationDays = (int) Math.ceil(nbDaysFloat);
            }
            if (durationDays < 1)
                durationDays = 1;
        }
        taskBar.setWidth(durationDays * BasePlanningWidget.getCellWidth());
        this.taskBars.add(taskBar);
        this.extraLine.addTask(taskBar);
        taskBar.setLabel(task.getWorkObjectName());
        this.extraLine.layout();

        int x = 0;
        if (this.previousX > 0)
        {
            x = this.previousX;
        }
        else
        {
            x = DragUtils.getXCoordinate(event, taskBar, this.planningView, this.extraLine);
        }
        taskBar.setLeft(x);
        taskBar.setTop(0);

        return taskBar;
    }

    /**
     * When drag move, task panel if task panel is inside current line, its x coordinate change.
     */
    @Override
    protected void onDragMove(DNDEvent event)
    {
        for (TaskBar taskBar : taskBars)
        {
            if (taskBar != null)
            {
                super.onDragMove(event);

                int x = 0;
                if (previousX > 0)
                {
                    x = previousX;
                    DateWrapper startDate = BarUtils.getDate(x);
                    if (DatetimeUtil.isSaturday(startDate))
                        x += 2 * BasePlanningWidget.getCellWidth();
                    else if (DatetimeUtil.isSunday(startDate))
                        x += BasePlanningWidget.getCellWidth();
                }
                else
                {
                    x = DragUtils.getXCoordinate(event, taskBar, planningView, this.extraLine);
                    x = BarUtils.skipMoveWeekend(x);
                }

                int panelWidth = BarUtils.getOpenDayWidth(x, taskBar.getDayDuration());

                taskBar.setWidth(panelWidth);
                this.extraLine.layout();

                taskBar.setStyleAttribute("left", x + "px");
                taskBar.setStyleAttribute("top", "0px");
                previousX = x + taskBar.getWidth();
            }
        }
        previousX = 0;
    }

    /**
     * When drag leave, task is removed from current line.
     */
    @Override
    protected void onDragLeave(DNDEvent event)
    {
        event.getStatus().setVisible(true);
        for (TaskBar taskBar : taskBars)
        {
            this.extraLine.removeTask(taskBar);

            TaskBar initialTaskBar = initialTaskBars.get(taskBar.getTask().getId().toString());
            if (initialTaskBar != null)
            {
                initialTaskBar.getExtraLine().addTask(initialTaskBar);
                initialTaskBar.getExtraLine().moveToRightLine(initialTaskBar.getLeft(),
                        initialTaskBar.getLeft() + initialTaskBar.getWidth(), initialTaskBar, Boolean.TRUE);

                initialTaskBar.getExtraLine().layout();
                this.planningView.registerTaskBar(initialTaskBar);
            }
        }
        this.taskBars.clear();
        initialTaskBars.clear();
    }

    /**
     * When task id dropped, it is registered to current line. Then TASK_FROM_GRID_DROPPED event is forwarded to
     * controllers.
     */
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        for (TaskBar taskBar : taskBars)
        {
            int left = this.getLeft(taskBar);
            int nbDaysFromTimeLineBeggining = BarUtils.getStartDayOccupation(left);
            int durationDays = taskBar.getWidth() / BasePlanningWidget.getCellWidth();

            this.extraLine.moveToRightLine(left, left + taskBar.getWidth() + 1, taskBar, Boolean.TRUE);
            taskBar.setLeft(left);
            taskBar.setWorkerFromExtraLine();
            taskBar.setDirty();

            this.planningView.registerTaskBar(taskBar);

            AppEvent mvcEvent = new AppEvent(PlanningAppEvents.TASK_FROM_GRID_DROPPED);
            mvcEvent.setData(taskBar.getTask());
            mvcEvent.setData(PlanningConfig.NB_DAYS_EVENT_VAR_NAME, nbDaysFromTimeLineBeggining);
            mvcEvent.setData(PlanningConfig.DURATION_DAYS_EVENT_VAR_NAME, durationDays);
            EventDispatcher.forwardEvent(mvcEvent);
        }
        this.taskBars.clear();
        initialTaskBars.clear();
    }

    public Integer getLeft(TaskBar taskBar)
    {
        String left = DOM.getStyleAttribute(taskBar.getElement(), "left");
        if (!Util.isEmptyString(left))
        {
            left = left.replaceAll("px", "");
            return Integer.parseInt(left);
        }
        return 0;
    }
}
