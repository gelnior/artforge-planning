package fr.hd3d.planning.ui.client.dnd.insert;

import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;


public class UnassignedTaskGridDragSource extends GridDragSource
{

    private DNDEvent event;
    private boolean started = false;

    public UnassignedTaskGridDragSource(Grid<TaskModelData> grid)
    {
        super(grid);
    }

    @Override
    protected void onDragStart(DNDEvent e)
    {
        BaseTaskModelData baseTaskModelData = (BaseTaskModelData) grid.getSelectionModel().getSelectedItem();

        if (baseTaskModelData != null
                && (BasePlanningWidget.isPreviewMode() || baseTaskModelData.get(TaskModelData.ACTUAL_START_DATE_FIELD) == null))
        {
            started = true;
            super.onDragStart(e);
        }
        else
        {
            e.setCancelled(true);
        }

    }

    public boolean isStarted()
    {
        return started;
    }

    @Override
    protected void onDragDrop(DNDEvent e)
    {}

    public void setEvent(DNDEvent event)
    {
        this.event = event;
    }

    public DNDEvent getEvent()
    {
        return event;
    }

    public void doDragDrop()
    {
        if (this.event != null)
        {
            grid.getView().refresh(false);
            this.event = null;
            this.started = false;
        }
    }
}
