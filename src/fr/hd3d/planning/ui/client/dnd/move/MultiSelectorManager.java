package fr.hd3d.planning.ui.client.dnd.move;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.core.DomQuery;
import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.core.XDOM;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.Region;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.widget.planning.util.ColorUtil;


public class MultiSelectorManager
{
    private static MultiSelectorManager INSTANCE;
    public static final String CAN_BE_IN_SELECTION = "canBeInSelection";
    private String selectedColor = "#66FF66";
    private String defaultColor = "#FF6666";
    private boolean isDragging;
    private El proxyEl;
    private LayoutContainer layoutContainer;
    private int startX;
    private int startY;
    private final List<Component> selectableElements;
    private final List<Component> selectedItems;
    private String changeProperty = "backgroundColor";

    private MultiSelectorManager()
    {
        this.selectableElements = new ArrayList<Component>();
        this.selectedItems = new ArrayList<Component>();
    }

    /**
     * Set the select color and the default color, both values must be in the following format #ffffff
     * 
     * @param selectedColor
     * @param defaultColor
     */
    public void setColors(String selectedColor, String defaultColor)
    {
        this.selectedColor = selectedColor;
        this.defaultColor = defaultColor;
    }

    public List<Component> getSelectedItems()
    {
        return selectedItems;
    }

    /**
     * Use this function if you want to change other css property than backgroundColor
     * 
     * @param cssProperty
     *            , default value 'backgroundColor'
     */
    public void setChangeBackground(String cssProperty)
    {
        this.changeProperty = cssProperty;
    }

    public static MultiSelectorManager getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new MultiSelectorManager();
        }
        return INSTANCE;
    }

    /**
     * Adds all the listeners to support multi selection
     * 
     * @param layoutContainer
     */
    public void addListeners(LayoutContainer layoutContainer)
    {
        layoutContainer.addListener(Events.OnMouseDown, new MouseDownListener());
        layoutContainer.addListener(Events.OnMouseMove, new MouseMoveListener());
        layoutContainer.addListener(Events.OnMouseUp, new MouseUpListener());
        this.layoutContainer = layoutContainer;
    }

    /**
     * Add a element to the selectable list
     * 
     * @param selectableElement
     */
    public void addSelectableElement(Component selectableElement)
    {
        this.selectableElements.add(selectableElement);
        selectableElement.addStyleName(CAN_BE_IN_SELECTION);
    }

    protected El createProxy()
    {
        proxyEl = new El(DOM.createDiv());
        proxyEl.setVisibility(false);
        proxyEl.dom.setClassName("x-drag-proxy");
        proxyEl.disableTextSelection(true);
        return proxyEl;
    }

    private class MouseDownListener implements Listener<ComponentEvent>
    {
        public void handleEvent(ComponentEvent be)
        {
            isDragging = true;
            startX = be.getXY().x;
            startY = be.getXY().y;
            if (proxyEl == null)
            {
                proxyEl = createProxy();
                proxyEl.setVisibility(true);
                proxyEl.setZIndex(XDOM.getTopZIndex());
                proxyEl.makePositionable(true);
                proxyEl.setXY(startX, startY);
            }
            DOM.appendChild(layoutContainer.getElement(), proxyEl.dom);
        }
    }

    private class MouseMoveListener implements Listener<ComponentEvent>
    {
        public void handleEvent(ComponentEvent be)
        {
            if (isDragging)
            {
                int x = be.getXY().x;
                int y = be.getXY().y;
                int deltaX = x - startX;
                int deltaY = y - startY;
                if (deltaX > 0 && deltaY > 0)
                {
                    proxyEl.setSize(deltaX, deltaY);
                }
                else if (deltaX > 0 && deltaY < 0)
                {
                    proxyEl.setTop(y);
                    proxyEl.setSize(deltaX, -deltaY);
                }
                else if (deltaX < 0 && deltaY < 0)
                {
                    proxyEl.setLeftTop(x, y);
                    proxyEl.setSize(-deltaX, -deltaY);
                }
                else if (deltaX < 0 && deltaY > 0)
                {
                    proxyEl.setLeft(x);
                    proxyEl.setSize(-deltaX, deltaY);
                }

                for (Iterator<Component> iterator = selectableElements.iterator(); iterator.hasNext();)
                {
                    Component component = iterator.next();
                    if (DomQuery.is(component.getElement(), "div." + CAN_BE_IN_SELECTION))
                    {
                        String changePropertyColorString = DOM
                                .getStyleAttribute(component.getElement(), changeProperty);
                        if (changePropertyColorString != null && !"".equals(changePropertyColorString))
                        {
                            if (!changePropertyColorString.startsWith("#"))
                            {
                                changePropertyColorString = ColorUtil.RGBtoHex(changePropertyColorString);
                            }
                            Region componentRegion = component.el().getRegion();
                            Region proxyRegion = proxyEl.getRegion();
                            int x1 = Math.max(componentRegion.right, proxyRegion.right);
                            int y1 = Math.max(componentRegion.bottom, proxyRegion.bottom);
                            int x2 = Math.min(componentRegion.left, proxyRegion.left);
                            int y2 = Math.min(componentRegion.top, proxyRegion.top);
                            if (x1 - x2 <= component.el().getWidth() + proxyEl.getWidth()
                                    && y1 - y2 <= component.el().getHeight() + proxyEl.getHeight())
                            {
                                if (defaultColor.equalsIgnoreCase(changePropertyColorString))
                                {
                                    component.setStyleAttribute(changeProperty, selectedColor);
                                }
                            }
                            else
                            {
                                if (selectedColor.equalsIgnoreCase(changePropertyColorString))
                                {
                                    component.setStyleAttribute(changeProperty, defaultColor);
                                }
                            }
                        }
                        else
                        {
                            GWT.log("MultiSelectorManager : property " + changeProperty + " must be initialize", null);
                        }
                    }
                }
            }
        }
    }

    private class MouseUpListener implements Listener<ComponentEvent>
    {
        public void handleEvent(ComponentEvent be)
        {
            if (isDragging)
            {
                isDragging = false;
                DOM.removeChild(layoutContainer.getElement(), proxyEl.dom);
                proxyEl = null;
            }
        }
    }
}
