package fr.hd3d.planning.ui.client.dnd.move;

import com.extjs.gxt.ui.client.dnd.DragSource;
import com.extjs.gxt.ui.client.event.DragEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.fx.Draggable;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Rectangle;
import com.extjs.gxt.ui.client.widget.Component;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.util.BarUtils;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.BaseTaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskGroupBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.dnd.resize.TaskResizable;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.view.WorkObjectPlanningView;


public class TaskDraggable extends Draggable
{
    private final BaseTaskBar taskBar;
    private ExtraLine extraLine;

    private WorkObjectPlanningView planning;
    private final EPlanningDisplayMode planningMode;

    private DragEvent de;

    /**
     * Needed to adapt task size depending if user is dragging it above week-end or not. If there are week-ends beneath
     * task, task is getting bigger, else it get back to its initial width.
     */
    private int initialWidth;
    private int proxyWidth = 0;

    public static TaskDraggable createDraggableResizable(TaskBar taskBar, WorkObjectPlanningView planningPanel,
            EPlanningDisplayMode planningMode)
    {

        TaskModelData task = taskBar.getTask();
        if (BasePlanningWidget.isPreviewMode()
                || (!task.getStatus().equals(ETaskStatus.CLOSE.toString())
                        && !task.getStatus().equals(ETaskStatus.CANCELLED.toString()) && !task.getStatus().equals(
                        ETaskStatus.OK.toString())))
        {
            String handles = "e";
            if (BasePlanningWidget.isPreviewMode() || task.getActualStartDate() == null)
                handles += " w";

            new TaskResizable(taskBar, handles);
            // resizable.setContainer(planningPanel);
        }

        if (BasePlanningWidget.isPreviewMode() || taskBar.getTask().getActualStartDate() == null)
        {
            TaskDraggable taskDraggable = new TaskDraggable(taskBar, taskBar, planningMode);
            taskDraggable.setContainer(planningPanel);
            taskDraggable.setUseProxy(true);
            taskDraggable.registerView(planningPanel);

            if (planningMode != EPlanningDisplayMode.TASK_TYPE)
                taskDraggable.setConstrainVertical(true);
            new DragSource(taskBar);
            return taskDraggable;
        }

        return null;
    }

    public static TaskDraggable createTaskGroupDraggableResizable(TaskGroupBar taskGroupBar,
            WorkObjectPlanningView planningPanel)
    {

        TaskDraggable taskDraggable = new TaskDraggable(taskGroupBar, planningPanel, taskGroupBar.getTaskGroup()
                .getType());
        new TaskResizable(taskGroupBar, "e w");
        taskDraggable.setContainer(planningPanel);
        taskDraggable.setUseProxy(true);
        taskDraggable.registerView(planningPanel);
        taskDraggable.setConstrainVertical(true);

        new DragSource(taskGroupBar);
        return taskDraggable;
    }

    public TaskDraggable(BaseTaskBar dragComponent, Component handle, EPlanningDisplayMode planningMode)
    {
        super(dragComponent, dragComponent);

        this.taskBar = dragComponent;
        this.planningMode = planningMode;

        this.setStartDragDistance(2);
        this.setMoveAfterProxyDrag(false);
    }

    public void registerView(WorkObjectPlanningView planning)
    {
        this.planning = planning;
    }

    @Override
    protected void onMouseMove(Event event)
    {
        this.gxtOnMouseMove(event);

        if (proxyEl != null)
        {
            Rectangle proxyBounds = proxyEl.getBounds();

            int x = proxyBounds.x - this.conX;
            x = Math.round((float) x / BasePlanningWidget.getCellWidth()) * BasePlanningWidget.getCellWidth();
            x = BarUtils.skipMoveWeekend(x);

            int y = proxyBounds.y - this.conY;
            if (y < 0)
                y = 0;

            if (this.taskBar instanceof TaskBar)
            {
                y = Math.round(((float) y / (BasePlanningWidget.CELL_HEIGHT + 2)));
                y = y * (BasePlanningWidget.CELL_HEIGHT + 2);
                y += 1;

                this.updateTaskBarSize(x, this.initialWidth + 2);
            }
            else
            {
                y = Math.round((float) y / (BasePlanningWidget.CELL_HEIGHT + 1)) * (BasePlanningWidget.CELL_HEIGHT + 1);
            }

            this.proxyEl.setLeft(x);
            this.proxyEl.setTop(y);
        }
    }

    private void updateTaskBarSize(int x, int width)
    {
        int nbDays = width / BasePlanningWidget.getCellWidth();
        nbDays = nbDays - BarUtils.getNbWeekEndDays(taskBar.getLeft(), width);

        this.proxyEl.setWidth(BarUtils.getOpenDayWidth(x, nbDays));
        this.proxyWidth = this.proxyEl.getWidth();
    }

    @Override
    protected void startDrag(Event event)
    {
        super.startDrag(event);

        this.initialWidth = this.taskBar.getWidth();

        if (((BaseTaskModelData) this.taskBar.getTimeObject()).getDuration() != null)
        {
            this.proxyEl.setWidth(this.taskBar.getWidth());
            this.proxyEl.setHeight(BasePlanningWidget.CELL_HEIGHT - 4);
            this.proxyEl.setLeft(this.taskBar.getLeft());
        }
        this.de = new DragEvent(this);
        this.de.setComponent(this.taskBar);
        this.de.setEvent(event);
        this.de.setX(this.startBounds.x);
        this.de.setY(this.startBounds.y);
    }

    @Override
    protected void stopDrag(Event event)
    {
        Rectangle proxyBounds = null;

        if (proxyEl != null)
        {
            proxyBounds = proxyEl.getBounds();

            if (this.planningMode == EPlanningDisplayMode.TASK_TYPE)
                this.extraLine = this.planning.getExtraLine(proxyEl.getRegion(), this.planningMode);
            else
                this.extraLine = taskBar.getExtraLine();
        }

        if (proxyBounds != null && isDragging())
        {
            super.stopDrag(event);

            int proxyLeft = proxyBounds.x - conX;
            int nbDaysFromTimeLineBeggining = BarUtils.getStartDayOccupation(proxyLeft);
            DateWrapper firstDate = BasePlanningWidget.timeLine.getWrappers()[0];

            if (taskBar instanceof TaskBar)
                this.onTaskPanelDrop((TaskBar) taskBar, firstDate, proxyLeft, nbDaysFromTimeLineBeggining);
            else if (taskBar instanceof TaskGroupBar)
                this.onTaskGroupPanelDrop(((TaskGroupBar) taskBar), firstDate, nbDaysFromTimeLineBeggining,
                        event.getCtrlKey());
        }
        super.stopDrag(event);
    }

    private void onTaskGroupPanelDrop(TaskGroupBar taskGroupBar, DateWrapper firstDate, int nbDays, boolean areTaskMoved)
    {
        TaskGroupModelData taskGroup = taskGroupBar.getTaskGroup();
        int length = CalendarUtil.getDaysBetween(taskGroup.getStartDate(), taskGroup.getEndDate());

        AppEvent mvcEvent = new AppEvent(PlanningAppEvents.TASK_GROUP_MOVED, taskGroup);
        mvcEvent.setData(PlanningConfig.MOVE_TASK_EVENT_VAR_NAME, areTaskMoved);
        mvcEvent.setData(PlanningConfig.PREVIOUS_START_DATE_EVENT_VAR_NAME, taskGroup.getStartDate());
        mvcEvent.setData(PlanningConfig.PREVIOUS_END_DATE_EVENT_VAR_NAME, taskGroup.getEndDate());

        DateWrapper startDate = firstDate.addDays(nbDays);
        DateWrapper endDate = startDate.addDays(length);

        taskGroupBar.setPosition(startDate.asDate(), endDate.asDate());

        taskGroup.setStartDate(startDate.asDate());
        taskGroup.setEndDate(endDate.asDate());

        EventDispatcher.forwardEvent(mvcEvent);
    }

    private void onTaskPanelDrop(TaskBar taskBar, DateWrapper firstDate, int proxyLeft, int nbDays)
    {
        TaskModelData task = taskBar.getTask();

        if (this.extraLine != null && this.extraLine.getTaskGroup() != null
                && this.extraLine.getTaskGroup().getTaskTypeID() != null && task.getTaskTypeId() != null
                && this.extraLine.getTaskGroup().getTaskTypeID().longValue() != task.getTaskTypeId().longValue())
        {
            return;
        }
        int length = (this.proxyWidth - 2) / BasePlanningWidget.getCellWidth();
        DateWrapper startDate = firstDate.addDays(nbDays);
        DateWrapper endDate = startDate.addDays(length - 1);

        if (this.extraLine != null)
        {
            taskBar.getExtraLine().removeOccupation(taskBar);
            taskBar.setPosition(startDate.asDate(), endDate.asDate());
            taskBar.getExtraLine().removeTask(taskBar);

            this.extraLine.addTask(taskBar);
            this.extraLine.layout();
            this.extraLine.moveToRightLine(proxyLeft, proxyLeft + taskBar.getWidth() + 1, taskBar, Boolean.TRUE);

            if (this.extraLine.getTaskGroup().getType() == EPlanningDisplayMode.TASK_TYPE)
                taskBar.setWorkerFromExtraLine();
        }
        task.setStartDate(startDate.asDate());
        task.setEndDate(endDate.asDate());

        taskBar.setDirtyMarker();

        AppEvent event = new AppEvent(PlanningCommonEvents.TASK_MOVED, task);
        if (this.extraLine != null)
            event.setData(PlanningConfig.PLANNING_TYPE_EVENT_VAR_NAME, this.extraLine.getTaskGroup().getType());
        EventDispatcher.forwardEvent(event);

    }

    // This method should not be there. It has been rewritten for GXT debugging purpose.
    @Override
    public void setXConstraint(int left, int right)
    {
        // xLeft = left;
        // xRight = right;
    }

    // This method should not be there. It has been rewritten for GXT debugging purpose.
    @Override
    public void setYConstraint(int top, int bottom)
    {}

    // This method should not be there. It has been rewritten for GXT debugging purpose.
    private void gxtOnMouseMove(Event event)
    {
        String cls = ((Element) event.getEventTarget().cast()).getClassName();
        if (cls != null && cls.contains("x-insert"))
        {
            return;
        }

        int x = DOM.eventGetClientX(event);
        int y = DOM.eventGetClientY(event);

        if (!isDragging()
                && (Math.abs(dragStartX - x) > getStartDragDistance() || Math.abs(dragStartY - y) > getStartDragDistance()))
        {
            startDrag(event);
        }

        if (isDragging())
        {
            // int left = isConstrainHorizontal() ? startBounds.x : startBounds.x + (x - dragStartX);
            int top = isConstrainVertical() ? startBounds.y : startBounds.y + (y - dragStartY);
            int left = startBounds.x + (x - dragStartX);

            if (left < conX)
                left = conX;

            if (left > conX + conWidth)
                left = conX + conWidth - BasePlanningWidget.getCellWidth();

            lastX = left;
            lastY = top;

            de.setSource(this);
            de.setComponent(taskBar);
            de.setEvent(event);
            de.setCancelled(false);
            de.setX(lastX);
            de.setY(lastY);
            fireEvent(Events.DragMove, de);

            if (de.isCancelled())
            {
                cancelDrag();
                return;
            }

            int tl = de.getX() != lastX ? de.getX() : lastX;
            int tt = de.getY() != lastY ? de.getY() : lastY;

            proxyEl.setPagePosition(tl, tt);
        }
    }

}
