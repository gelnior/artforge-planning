package fr.hd3d.planning.ui.client.dnd.resize;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.google.gwt.user.client.Event;


public class TaskResizeEvent extends BaseEvent
{
    /**
     * The resizable instance.
     */
    public TaskResizable resizable;

    /**
     * The component being resized.
     */
    public Component component;

    /**
     * The DOM event.
     */
    public Event event;

    public TaskResizeEvent(TaskResizable resizable)
    {
        super(resizable);
        this.resizable = resizable;
    }

    public TaskResizeEvent(TaskResizable resizable, Component component, Event event)
    {
        super(resizable);
        this.resizable = resizable;
        this.component = component;
        this.event = event;
    }
}
