package fr.hd3d.planning.ui.client.dnd.resize;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.PreviewEvent;
import com.extjs.gxt.ui.client.event.ResizeListener;
import com.extjs.gxt.ui.client.util.BaseEventPreview;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Point;
import com.extjs.gxt.ui.client.util.Rectangle;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ComponentHelper;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Shim;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.BaseTaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskGroupBar;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;


public class TaskResizable extends BaseObservable
{
    protected enum Dir
    {
        E, W;
    }

    private class ResizeHandle extends Component
    {

        public Dir dir;

        @Override
        public void onBrowserEvent(Event event)
        {
            switch (DOM.eventGetType(event))
            {
                case Event.ONMOUSEDOWN:
                    DOM.eventCancelBubble(event, true);
                    DOM.eventPreventDefault(event);
                    handleMouseDown(event, this);
                    break;
            }
        }

        @Override
        protected void onRender(Element target, int index)
        {
            super.onRender(target, index);
            Element el = DOM.createDiv();
            setElement(el, target, index);
            sinkEvents(Event.MOUSEEVENTS);
        }
    }

    private Dir dir;
    private boolean dynamic;
    private final boolean enabled = true;

    private List<ResizeHandle> handleList;
    private String handles;
    private final Listener<ComponentEvent> listener;

    private int minWidth = 0;
    private boolean preserveRatio = false;

    private BaseEventPreview preview;
    private El proxyEl;
    private String proxyStyle = "x-resizable-proxy";

    private final BaseTaskBar planningBar;
    private boolean resizing;
    private Rectangle startBox;
    private Point startPoint;

    // private int containerWidth = 0;
    // private int containerX;

    public TaskResizable(final BaseTaskBar resize, String handles)
    {
        this.planningBar = resize;
        this.handles = handles;
        this.minWidth = BasePlanningWidget.getCellWidth();

        this.addResizeListener(new TaskResizeListener() {
            @Override
            public void resizeEnd(TaskResizeEvent re)
            {
                super.resizeEnd(re);
                planningBar.addStyleName(CSSUtils.MODIFIED_STYLE);
            }
        });

        this.listener = new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent e)
            {
                EventType type = e.getType();
                if (type == Events.Render)
                {
                    init();
                }
                else if (type == Events.Attach)
                {
                    onAttach();
                }
                else if (type == Events.Detach)
                {
                    onDetach();
                }
            }
        };

        resize.addListener(Events.Render, listener);
        resize.addListener(Events.Attach, listener);
        resize.addListener(Events.Detach, listener);

        if (resize.isRendered())
            init();

        if (resize.isAttached())
            onAttach();
    }

    /**
     * Adds a resize listener.
     * 
     * @param listener
     *            the listener
     */
    public void addResizeListener(TaskResizeListener listener)
    {
        addListener(Events.ResizeStart, listener);
        addListener(Events.ResizeEnd, listener);
    }

    /**
     * Returns the min width.
     * 
     * @return the min width
     */
    public int getMinWidth()
    {
        return minWidth;
    }

    /**
     * Returns the proxy style.
     * 
     * @return the proxy style
     */
    public String getProxyStyle()
    {
        return proxyStyle;
    }

    /**
     * Returns true if widget is being resized directly.
     * 
     * @return the dynamic state
     */
    public boolean isDynamic()
    {
        return dynamic;
    }

    /**
     * Returns true if the aspect ratio is being preserved.
     * 
     * @return true if the aspect ratio is being preserved
     */
    public boolean isPreserveRatio()
    {
        return preserveRatio;
    }

    /**
     * Returns <code>true</code> if if resizing.
     * 
     * @return the resize state
     */
    public boolean isResizing()
    {
        return resizing;
    }

    /**
     * Removes the drag handles.
     */
    public void release()
    {
        onDetach();

        planningBar.removeListener(Events.Attach, listener);
        planningBar.removeListener(Events.Detach, listener);
        planningBar.removeListener(Events.Render, listener);

        Iterator<ResizeHandle> iter = handleList.iterator();
        while (iter.hasNext())
        {
            ResizeHandle handle = iter.next();
            iter.remove();
            DOM.removeChild(planningBar.getElement(), handle.getElement());
        }
    }

    /**
     * Removes a resize listener.
     * 
     * @param listener
     *            the listener
     */
    public void removeResizeListener(ResizeListener listener)
    {
        removeListener(Events.ResizeStart, listener);
        removeListener(Events.ResizeEnd, listener);
    }

    /**
     * True to resize the widget directly instead of using a proxy (defaults to false).
     * 
     * @param dynamic
     *            true to resize directly
     */
    public void setDynamic(boolean dynamic)
    {
        this.dynamic = dynamic;
    }

    /**
     * Enables or disables the drag handles.
     * 
     * @param enable
     *            <code>true</code> to enable
     */
    public void setEnabled(boolean enable)
    {
        for (ResizeHandle handle : handleList)
        {
            El.fly(handle.getElement()).setVisibility(enable);
        }
    }

    /**
     * Sets the min width (defaults to 50).
     * 
     * @param minWidth
     *            the min width
     */
    public void setMinWidth(int minWidth)
    {
        this.minWidth = minWidth;
    }

    /**
     * True to preserve the original ratio between height and width during resize (defaults to false).
     * 
     * @param preserveRatio
     *            true to preserve the original aspect ratio
     */
    public void setPreserveRatio(boolean preserveRatio)
    {
        this.preserveRatio = preserveRatio;
    }

    /**
     * Sets the style name used for proxy drags (defaults to 'x-resizable-proxy').
     * 
     * @param proxyStyle
     *            the proxy style
     */
    public void setProxyStyle(String proxyStyle)
    {
        this.proxyStyle = proxyStyle;
    }

    public void syncHandleHeight()
    {
        // int height = resize.getHeight(true);
        for (ResizeHandle r : handleList)
        {
            if (r.dir == Dir.E || r.dir == Dir.W)
            {
                r.el().setHeight("20px");
                r.el().setWidth("3px");
            }
        }
        planningBar.el().repaint();
    }

    // public void setContainer(PlanningAbsolutePanel boundaryPanel)
    // {
    // this.containerWidth = boundaryPanel.getOffsetWidth();
    // this.containerX = boundaryPanel.getAbsoluteLeft();
    // }

    protected Element createProxy()
    {
        Element elem = DOM.createDiv();
        El.fly(elem).setStyleName(proxyStyle, true);
        El.fly(elem).disableTextSelection(true);
        return elem;
    }

    protected void init()
    {
        this.planningBar.el().makePositionable();
        if (this.handleList == null)
        {
            this.handleList = new ArrayList<ResizeHandle>();
            if (this.handles.equals("all"))
            {
                this.handles = "n s e w ne nw se sw";
            }
            String[] temp = handles.split(" ");
            for (int i = 0; i < temp.length; i++)
            {
                if ("e".equals(temp[i]))
                {
                    create(Dir.E, "east");
                }
                else if ("w".equals(temp[i]))
                {
                    create(Dir.W, "west");
                }
            }

            this.preview = new BaseEventPreview() {
                @Override
                public boolean onPreview(PreviewEvent event)
                {
                    event.preventDefault();
                    switch (event.getEventTypeInt())
                    {
                        case Event.ONMOUSEMOVE:
                            int x = event.getClientX();
                            int y = event.getClientY();
                            handleMouseMove(x, y);
                            break;
                        case Event.ONMOUSEUP:
                            handleMouseUp(event.getEvent());
                            break;
                    }
                    return true;
                }

            };
            this.preview.setAutoHide(false);
        }

        syncHandleHeight();

    }

    protected void onAttach()
    {
        for (ResizeHandle handle : handleList)
        {
            ComponentHelper.doAttach(handle);
        }
    }

    protected void onDetach()
    {
        for (ResizeHandle handle : handleList)
        {
            ComponentHelper.doDetach(handle);
        }
    }

    // private int constrain(int v, int diff, int m)
    // {
    // if (v - diff < m)
    // {
    // diff = v - m;
    // }
    // return diff;
    // }

    private ResizeHandle create(Dir dir, String cls)
    {
        ResizeHandle rh = new ResizeHandle();
        rh.setStyleName("x-resizable-handle " + "x-resizable-handle-" + cls);
        rh.dir = dir;
        rh.render(planningBar.getElement());
        handleList.add(rh);
        return rh;
    }

    private void handleMouseDown(Event event, ResizeHandle handle)
    {
        if (!enabled)
        {
            return;
        }

        if (!fireEvent(Events.ResizeStart, new TaskResizeEvent(this, planningBar, event)))
        {
            return;
        }

        dir = handle.dir;

        startBox = planningBar.getBounds(false);
        int x = DOM.eventGetClientX(event);
        int y = DOM.eventGetClientY(event);
        startPoint = new Point(x, y);

        resizing = true;

        if (proxyEl == null)
        {
            proxyEl = new El(DOM.createDiv());
            proxyEl.setStyleName(proxyStyle, true);
            proxyEl.disableTextSelection(true);
            Element body = RootPanel.getBodyElement();
            DOM.appendChild(body, proxyEl.dom);
        }

        proxyEl.makePositionable(true);
        proxyEl.setLeft(startBox.x).setTop(startBox.y);
        proxyEl.setSize(startBox.width, startBox.height);
        proxyEl.setVisible(true);
        proxyEl.updateZIndex(5);
        proxyEl.setStyleAttribute("cursor", handle.el().getStyleAttribute("cursor"));

        preview.add();

        Shim.get().cover(false);
    }

    private void handleMouseMove(int xin, int yin)
    {
        if (this.resizing)
        {
            // initial position and size.
            Integer x = this.startBox.x;
            Integer y = this.startBox.y;
            Float proxyWidth = (float) this.startBox.width;
            Float h = (float) this.startBox.height;

            // Initialize minimum and maximum width possible.
            // Integer maxWidth = BasePlanningWidget.getCellWidth();
            this.minWidth = BasePlanningWidget.getCellWidth();
            Point eventXY = new Point(xin, yin);

            // Calculate distance between bar boundary and actual move position.
            Integer diffX = -(this.startPoint.x - Math.max(2, eventXY.x));
            // Set distance as a discrete value in term of cell distance.
            diffX = Math.round((float) diffX / BasePlanningWidget.getCellWidth()) * BasePlanningWidget.getCellWidth();

            switch (this.dir)
            {
                case E:
                    proxyWidth += diffX;

                    // // Bar starts in the planning.
                    // if (this.containerX <= x)
                    // {
                    // maxWidth = this.containerWidth - (x - this.containerX);
                    // }
                    // // Bar does not start in the planning.
                    // else
                    // {
                    // maxWidth = this.containerWidth + (this.containerX - x);
                    // this.minWidth = this.containerX - x + BasePlanningWidget.getCellWidth();
                    // }
                    // proxyWidth = Math.max(proxyWidth, minWidth);
                    // proxyWidth = Math.min(proxyWidth, maxWidth);
                    break;
                case W:
                    x += diffX;
                    proxyWidth = proxyWidth - diffX;
                    // if (x + diffX > this.containerX && x + diffX < (x + proxyWidth)
                    // && x + diffX < this.containerX + this.containerWidth)
                    // {
                    // x += diffX;
                    // proxyWidth = proxyWidth - diffX;
                    // }
                    // else if (x + diffX > x + proxyWidth)
                    // {
                    // x = (int) (x + proxyWidth - BasePlanningWidget.getCellWidth());
                    // proxyWidth = (float) BasePlanningWidget.getCellWidth();
                    // }
                    // else if (x + diffX > this.containerX + this.containerWidth)
                    // {
                    // proxyWidth = BasePlanningWidget.getCellWidth()
                    // + (x + proxyWidth - this.containerX - this.containerWidth);
                    // x = this.containerX + this.containerWidth - BasePlanningWidget.getCellWidth();
                    // }
                    // else if (x + diffX < this.containerX)
                    // {
                    // proxyWidth = proxyWidth + (x - this.containerX);
                    // x = this.containerX;
                    // }
                    break;
            }

            this.proxyEl.setBounds(x, y, proxyWidth.intValue(), h.intValue());
        }
    }

    private void handleMouseUp(Event event)
    {
        // Clean proxy, reset resizing.
        this.resizing = false;
        this.preview.remove();
        Shim.get().uncover();

        if (this.proxyEl != null)
            this.proxyEl.disableTextSelection(false);

        // Calculate number of days added or removed.
        int nbDays = 0;
        int delta = proxyEl.getWidth() - planningBar.getWidth();
        if (delta > 0)
            nbDays = (delta + 4) / BasePlanningWidget.getCellWidth();
        else
            nbDays = (delta - 4) / BasePlanningWidget.getCellWidth();

        // For task bars, notify extra line that task bar has changed.
        DateWrapper tmpDate = new DateWrapper(planningBar.getStartDate());
        if (planningBar instanceof TaskBar)
        {
            TaskBar taskBar = (TaskBar) planningBar;
            taskBar.getExtraLine().removeOccupation(taskBar);
            if (!BasePlanningWidget.isPreviewMode() && taskBar.getTask().getActualStartDate() != null)
                tmpDate = new DateWrapper(taskBar.getTask().getActualStartDate());
        }

        // Set new position
        if (this.dir == Dir.E)
            this.planningBar.addDaysToEndDate(nbDays);
        else if (this.dir == Dir.W)
        {
            Date newStartDate = tmpDate.addDays(-nbDays).asDate();
            planningBar.setPosition(newStartDate, planningBar.getEndDate());
            planningBar.resetSize();
        }

        // Set specific modifications depending on the bar type.
        if (this.planningBar instanceof TaskBar)
            this.resizeTaskBar();
        if (this.planningBar instanceof TaskGroupBar)
            this.resizeTaskGroupBar();

        // End mouse up handling.
        this.proxyEl.setVisible(false);

        TaskResizeEvent ce = new TaskResizeEvent(this);
        ce.component = this.planningBar;
        ce.event = event;
        fireEvent(Events.ResizeEnd, ce);
    }

    private void resizeTaskBar()
    {
        TaskBar taskBar = (TaskBar) planningBar;
        TaskModelData task = taskBar.getTask();

        taskBar.getExtraLine().moveToRightLine(taskBar.getLeft(), taskBar.getLeft() + taskBar.getWidth(), taskBar,
                Boolean.TRUE);

        if (taskBar.getOpenDays() < task.getDayDuration())
            MessageBox.info("Warning", "The task duration is too big for the dates you set.", null);

        EventDispatcher.forwardEvent(PlanningAppEvents.TASK_RESIZED, task);
    }

    private void resizeTaskGroupBar()
    {
        TaskGroupBar panel = (TaskGroupBar) planningBar;
        TaskGroupModelData taskGroup = panel.getTaskGroup();
        taskGroup.setStartDate(panel.getStartDate());
        taskGroup.setEndDate(panel.getEndDate());
        EventDispatcher.forwardEvent(PlanningAppEvents.TASK_GROUP_RESIZED, taskGroup);
    }

    // private int getProxyNbDaysDelta()
    // {
    // int nbDays = 0;
    // int delta = proxyEl.getWidth() - planningBar.getWidth();
    // if (delta > 0)
    // {
    // nbDays = (delta + 4) / BasePlanningWidget.getCellWidth();
    // }
    // else
    // {
    // nbDays = (delta - 4) / BasePlanningWidget.getCellWidth();
    // }
    //
    // if (planningBar instanceof TaskBar)
    // {
    // TaskBar taskBar = (TaskBar) planningBar;
    // taskBar.getExtraLine().removeOccupation(taskBar);
    // }
    //
    // return nbDays;
    // }

    // private void updateAdditionalTasks(TaskBar taskBar, TaskModelData task)
    // {
    // // List<TaskBar> additionalTaskBars = taskBar.getAdditionalTasks();
    //
    // if (additionalTaskBars.size() > 0)
    // {
    // for (TaskBar additionalTaskBar : additionalTaskBars)
    // {
    // TaskModelData additionalTask = additionalTaskBar.getTask();
    // additionalTask.setStartDate(task.getStartDate());
    // additionalTask.setEndDate(task.getEndDate());
    // }
    // // for (TaskBar additionalTaskBar : additionalTaskBars)
    // // EventDispatcher.forwardEvent(PlanningAppEvents.TASK_RESIZED, additionalTaskBar.getTask());
    // }
    // }
}
