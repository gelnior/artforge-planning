package fr.hd3d.planning.ui.client.dnd.resize;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;


public class TaskResizeListener implements Listener<TaskResizeEvent>
{
    public void handleEvent(TaskResizeEvent be)
    {
        if (Events.ResizeStart.equals(be.getType()))
        {
            resizeStart(be);
        }
        else if (Events.ResizeEnd.equals(be.getType()))
        {
            resizeEnd(be);
        }
    }

    public void resizeStart(TaskResizeEvent re)
    {

    }

    public void resizeEnd(TaskResizeEvent re)
    {

    }

}
