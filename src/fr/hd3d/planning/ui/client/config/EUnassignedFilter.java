package fr.hd3d.planning.ui.client.config;

/**
 * Enumeration to define which filter to apply to task list.
 * 
 * @author HD3D
 */
public enum EUnassignedFilter
{
    ALL, UNASSIGNED, UNPLANNED;
}
