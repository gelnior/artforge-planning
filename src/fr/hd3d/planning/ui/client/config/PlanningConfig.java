package fr.hd3d.planning.ui.client.config;

/**
 * Planning configuration contains main application constants.
 * 
 * @author HD3D
 */
public class PlanningConfig
{
    public static final String PLANNING_CREATION_EVENT_VAR_NAME = "isCreation";

    public static final String NB_DAYS_EVENT_VAR_NAME = "nb-days";
    public static final String MOVE_TASK_EVENT_VAR_NAME = "moveTask";
    public static final String PREVIOUS_START_DATE_EVENT_VAR_NAME = "previousStartDate";
    public static final String PREVIOUS_END_DATE_EVENT_VAR_NAME = "previousEndDate";
    public static final String TASK_GROUP_EVENT_VAR_NAME = "taskGroup";
    public static final String DURATION_DAYS_EVENT_VAR_NAME = "duration-days";
    public static final String PLANNING_TYPE_EVENT_VAR_NAME = "planning-type";

    public static final String AUTO_SAVE_SETTING_KEY = "planning.autosave";
    public static final String ZOOM_MODE_SETTING_KEY = "planning.zoommode";
    public static final String COLOR_MODE_SETTING_KEY = "planning.colormode";
    public static final String START_DATE_SETTING_KEY = "planning.startdate.";
    public static final String END_DATE_SETTING_KEY = "planning.enddate.";
}
