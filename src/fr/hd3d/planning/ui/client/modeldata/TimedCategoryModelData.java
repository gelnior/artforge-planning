package fr.hd3d.planning.ui.client.modeldata;

import java.util.ArrayList;
import java.util.List;


public class TimedCategoryModelData extends TimedModelData
{

    private static final long serialVersionUID = -8201148611448935484L;
    private String categoryName;
    private final List<Object> children;
    private Long parentID;

    public TimedCategoryModelData()
    {
        super();
        this.children = new ArrayList<Object>();
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }

    public void addChild(Object object)
    {
        this.children.add(object);
    }

    public List<Object> getChildren()
    {
        return children;
    }

    public Long getParentID()
    {
        return parentID;
    }

    public void setParentID(Long parentID)
    {
        this.parentID = parentID;
    }
}
