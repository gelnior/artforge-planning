package fr.hd3d.planning.ui.client.modeldata;

public class TimedConstituentModelData extends TimedModelData
{
    /**
     * 
     */
    private static final long serialVersionUID = -4021551766242312399L;
    private Long constituentID;
    private String constituentLabel;

    public TimedConstituentModelData()
    {
        super();
    }

    public Long getConstituentID()
    {
        return constituentID;
    }

    public void setConstituentID(Long constituentID)
    {
        this.constituentID = constituentID;
    }

    public String getConstituentLabel()
    {
        return constituentLabel;
    }

    public void setConstituentLabel(String constituentLabel)
    {
        this.constituentLabel = constituentLabel;
    }

}
