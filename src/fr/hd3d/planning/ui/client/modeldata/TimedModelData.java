package fr.hd3d.planning.ui.client.modeldata;

import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;


public class TimedModelData extends BaseTaskModelData
{
    private static final long serialVersionUID = -6375846071727472263L;

    private Long categoryID;
    private Long sequenceID;

    public Long getCategoryID()
    {
        return categoryID;
    }

    public void setCategoryID(Long categoryID)
    {
        this.categoryID = categoryID;
    }

    public Long getSequenceID()
    {
        return sequenceID;
    }

    public void setSequenceID(Long sequenceID)
    {
        this.sequenceID = sequenceID;
    }
}
