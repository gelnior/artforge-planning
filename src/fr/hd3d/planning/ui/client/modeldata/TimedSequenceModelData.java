package fr.hd3d.planning.ui.client.modeldata;

import java.util.ArrayList;
import java.util.List;


public class TimedSequenceModelData extends TimedModelData
{
    private static final long serialVersionUID = 2273394531273139259L;

    private String sequenceName;
    private List<Object> children;
    private Long parentID;

    public TimedSequenceModelData()
    {
        super();
        this.children = new ArrayList<Object>();
    }

    public String getSequenceName()
    {
        return sequenceName;
    }

    public void setSequenceName(String sequenceName)
    {
        this.sequenceName = sequenceName;
    }

    public List<Object> getChildren()
    {
        return children;
    }

    public void setChildren(List<Object> children)
    {
        this.children = children;
    }

    public Long getParentID()
    {
        return parentID;
    }

    public void setParentID(Long parentID)
    {
        this.parentID = parentID;
    }

    public void addChild(Object object)
    {
        this.children.add(object);
    }
}
