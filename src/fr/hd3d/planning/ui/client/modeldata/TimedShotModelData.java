package fr.hd3d.planning.ui.client.modeldata;

public class TimedShotModelData extends TimedModelData
{

    /**
     * 
     */
    private static final long serialVersionUID = -8088863934502284527L;
    private Long shotID;
    private String shotLabel;

    public Long getShotID()
    {
        return shotID;
    }

    public void setShotID(Long shotID)
    {
        this.shotID = shotID;
    }

    public String getShotLabel()
    {
        return shotLabel;
    }

    public void setShotLabel(String shotLabel)
    {
        this.shotLabel = shotLabel;
    }
}
