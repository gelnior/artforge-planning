package fr.hd3d.planning.ui.client.events;

import com.extjs.gxt.ui.client.event.EventType;


public class PlanningAppEvents
{
    public static final EventType TASK_GRID_DATA_LOADED = new EventType();
    public static final EventType RELOAD_PLANNING_STORE_EVENT = new EventType();
    public static final EventType PLANNING_CHANGED = new EventType();
    public static final EventType TASK_FROM_GRID_DROPPED = new EventType();
    public static final EventType TASK_RESIZED = new EventType();
    public static final EventType PLANNING_SAVE_CLICKED = new EventType();
    public static final EventType TASKS_SAVED = new EventType();
    public static final EventType PLANNING_DATE_CHANGED = new EventType();
    public static final EventType PLANNING_SAVED = new EventType();
    public static final EventType TASK_SAVE_CLICKED = new EventType();
    public static final EventType PLANNING_LOADED = new EventType();
    public static final EventType TASK_GROUP_MOVED = new EventType();
    public static final EventType TASK_GROUP_RESIZED = new EventType();
    public static final EventType TASK_GROUPS_SAVED = new EventType();
    public static final EventType EXTRA_LINES_CREATED = new EventType();
    public static final EventType TASK_FILTER_CHANGED = new EventType();

    public static final EventType COLOR_TASK_TASK_TYPE_CLICKED = new EventType();
    public static final EventType COLOR_TASK_STATUS_CLICKED = new EventType();
    public static final EventType COLOR_TASK_START_CLICKED = new EventType();
    public static final EventType REFRESH_PLANNING_CLICKED = new EventType();
    public static final EventType EXTRA_LINE_ADDED = new EventType();
    public static final EventType TASK_TASK_TYPE_FILTER_CHANGED = new EventType();

    public static final EventType TASK_TYPE_MODE_CLICKED = new EventType();
    public static final EventType TASK_TYPE_SHOT_MODE_CLICKED = new EventType();
    public static final EventType CONSTITUENT_MODE_CLICKED = new EventType();
    public static final EventType SHOT_MODE_CLICKED = new EventType();
    public static final EventType EXPORT_ODS_PLANNING_CLICKED = new EventType();
    public static final EventType EXPORT_ODS_FINISHED = new EventType();
    public static final EventType PLANNING_OPENED = new EventType();
    public static final EventType TASK_WORK_OBJECT_FILTER_CHANGED = new EventType();
    public static final EventType PLANNING_AUTO_SAVE_CLICKED = new EventType();
    public static final EventType TASK_ACTIVITIES_LOADED = new EventType();

    public static final EventType WEEK_DISPLAY_CLICKED = new EventType();
    public static final EventType MONTH_DISPLAY_CLICKED = new EventType();
    public static final EventType YEAR_DISPLAY_CLICKED = new EventType();
}
