package fr.hd3d.planning.ui.client.events;

import com.extjs.gxt.ui.client.event.EventType;


public class WorkObjectPlanningEvents
{
    public static final String TASK_GROUP_VAR = "task-group";
    public static final String CATEGORIES_VAR = "categories";
    public static final String SEQUENCES_VAR = "sequences";

    public static final EventType ROOT_TASK_GROUPS_LOADED = new EventType();
    public static final EventType TASK_GROUP_BOUNDS_SYNCHRONIZED = new EventType();
    public static final EventType TASK_GROUP_ADDING_USERS_REQUESTED = new EventType();

    public static final EventType MILESTONES_LOADED = new EventType();

    public static final EventType ROOT_CATEGORIES_LOADING_STARTED = new EventType();
    public static final EventType ROOT_CATEGORIES_LOADING_FINISHED = new EventType();
    public static final EventType ROOT_CATEGORIES_LOADED = new EventType();
    public static final EventType ROOT_CATEGORIES_DATA_LOADED = new EventType();
    public static final EventType CATEGORY_CATEGORIES_LOADED = new EventType();

    public static final EventType ROOT_SEQUENCES_LOADING_STARTED = new EventType();
    public static final EventType ROOT_SEQUENCES_LOADING_FINISHED = new EventType();
    public static final EventType ROOT_SEQUENCES_LOADED = new EventType();
    public static final EventType ROOT_SEQUENCES_DATA_LOADED = new EventType();
    public static final EventType CATEGORY_CONSTITUENTS_LOADED = new EventType();
    public static final EventType WORK_OBJECT_TASKS_LOADED = new EventType();
    public static final EventType SEQUENCE_SEQUENCES_LOADED = new EventType();
    public static final EventType SEQUENCE_SHOTS_LOADED = new EventType();

}
