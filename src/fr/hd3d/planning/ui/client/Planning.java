package fr.hd3d.planning.ui.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.planning.ui.client.constants.PlanningLocaleConstants;
import fr.hd3d.planning.ui.client.constants.PlanningLocaleMessages;
import fr.hd3d.planning.ui.client.view.PlanningMainView;


/**
 * Entry point for planning application
 */
public class Planning implements EntryPoint
{

    public static final PlanningLocaleConstants CONSTANTS = GWT.create(PlanningLocaleConstants.class);
    public static final PlanningLocaleMessages MESSAGES = GWT.create(PlanningLocaleMessages.class);
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public void onModuleLoad()
    {
        PlanningMainView mainView = new PlanningMainView();
        mainView.init();

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
