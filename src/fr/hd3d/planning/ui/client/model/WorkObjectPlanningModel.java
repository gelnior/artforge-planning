package fr.hd3d.planning.ui.client.model;

import java.util.List;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.IsNotNullConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.planning.ui.client.model.listener.CategoryTasksLoadListener;


public class WorkObjectPlanningModel
{
    protected final CategoryTasksLoadListener taskStoreListener = new CategoryTasksLoadListener();
    protected final ServiceStore<TaskModelData> categoryTasks = new ServiceStore<TaskModelData>(new TaskReader());

    /** Currently selected task group. */
    private TaskGroupModelData selectedTaskGroup;

    public WorkObjectPlanningModel()
    {
        this.categoryTasks.addLoadListener(taskStoreListener);
    }

    /** @return currently selected task group. */
    public TaskGroupModelData getSelectedTaskGroup()
    {
        return selectedTaskGroup;
    }

    /**
     * Set currently selected task group.
     * 
     * @param taskGroup
     *            The task group to select.
     */
    public void setSelectedTaskGroup(TaskGroupModelData taskGroup)
    {
        this.selectedTaskGroup = taskGroup;
    }

    public ServiceStore<TaskModelData> getTaskCategoryStore()
    {
        return categoryTasks;
    }

    public void loadTask(String type, TaskGroupModelData taskGroup, List<Long> constituentIds)
    {
        EqConstraint typeConstraint = new EqConstraint("boundEntityTaskLinks.boundEntityName", type);
        InConstraint idConstraint = new InConstraint("boundEntityTaskLinks.boundEntity", constituentIds);
        IsNotNullConstraint startDateConstraint = new IsNotNullConstraint(TaskModelData.START_DATE_FIELD);
        IsNotNullConstraint endDateConstraint = new IsNotNullConstraint(TaskModelData.END_DATE_FIELD);

        categoryTasks.setPath(MainModel.currentProject.getDefaultPath() + "/" + ServicesPath.TASKS);
        categoryTasks.clearParameters();
        categoryTasks.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
        this.categoryTasks.addParameter(new OrderBy("boundEntityTaskLinks.woName"));
        categoryTasks
                .addParameter(new Constraints(typeConstraint, idConstraint, startDateConstraint, endDateConstraint));

        taskStoreListener.setTaskGroup(taskGroup);

        categoryTasks.reload();
    }
}
