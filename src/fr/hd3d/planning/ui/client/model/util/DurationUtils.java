package fr.hd3d.planning.ui.client.model.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;


public class DurationUtils
{
    public static List<DurationModelData> sortMap(FastMap<DurationModelData> categories)
    {
        List<DurationModelData> durationList = new ArrayList<DurationModelData>();
        for (DurationModelData category : categories.values())
        {
            durationList.add(category);
        }
        Collections.sort(durationList, new NameSorter<DurationModelData>());
        return durationList;
    }
}
