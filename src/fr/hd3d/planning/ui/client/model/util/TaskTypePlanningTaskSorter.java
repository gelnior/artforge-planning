package fr.hd3d.planning.ui.client.model.util;

import java.util.Date;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * Sorter that put task to the bottom of sorting if their date are not rightly set (start or end date is missing or
 * start date is after end date). If both task has wrong dates, it compares their worker name.
 * 
 * @author HD3D
 */
public class TaskTypePlanningTaskSorter extends StoreSorter<TaskModelData>
{
    @Override
    public int compare(Store<TaskModelData> store, TaskModelData m1, TaskModelData m2, String property)
    {
        Date m1startDate = m1.getStartDate();
        Date m1endDate = m1.getEndDate();
        Date m2startDate = m2.getStartDate();
        Date m2endDate = m2.getEndDate();

        int comparaison = 0;
        if (m1startDate == null || m1endDate == null || m1endDate.before(m1startDate))
        {
            if (m2startDate == null || m2endDate == null || m2endDate.before(m2startDate))
            {
                comparaison = comparator.compare(m1.getWorkerName(), m2.getWorkerName());
            }
            else
            {
                comparaison = 1;
            }
        }
        else
        {
            if (m2startDate == null || m2endDate == null || m2endDate.before(m2startDate))
            {
                comparaison = -1;
            }
            else
            {
                comparaison = comparator.compare(m1startDate, m2startDate);
            }
        }
        return comparaison;
    }
}
