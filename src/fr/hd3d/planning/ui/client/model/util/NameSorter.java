package fr.hd3d.planning.ui.client.model.util;

import java.util.Comparator;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;


public class NameSorter<M extends DurationModelData> implements Comparator<M>
{
    public int compare(M o1, M o2)
    {
        // if (o1.getStartDate() == null)
        // {
        // return 1;
        // }
        // else if (o2.getStartDate() == null)
        // {
        // return -1;
        // }
        // else if (o1.getStartDate().before(o2.getStartDate()))
        // {
        // return -1;
        // }
        // else if (o1.getStartDate().after(o2.getStartDate()))
        // {
        // return 1;
        // }
        // else
        if (o1.getName() != null && o2.getName() != null)
        {
            return o1.getName().compareTo(o2.getName());
        }
        else
        {
            return 0;
        }
    }
}
