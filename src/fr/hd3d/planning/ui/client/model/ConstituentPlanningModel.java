package fr.hd3d.planning.ui.client.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.ext.json.JsonRepresentation;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.reader.CategoryReader;
import fr.hd3d.common.ui.client.modeldata.reader.ConstituentReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.listener.CategoryConstituentsLoadListener;
import fr.hd3d.planning.ui.client.modeldata.TimedCategoryModelData;


public class ConstituentPlanningModel extends WorkObjectPlanningModel
{

    /** Store used to temporary store categories retrieved for constituent planning. */
    private final ServiceStore<CategoryModelData> categoryStore = new ServiceStore<CategoryModelData>(
            new CategoryReader());

    /** Store used to temporary store constituents retrieved for constituent planning. */
    private final ServiceStore<ConstituentModelData> constituentStore = new ServiceStore<ConstituentModelData>(
            new ConstituentReader());

    private final CategoryConstituentsLoadListener constituentStoreListener = new CategoryConstituentsLoadListener();

    public ConstituentPlanningModel()
    {
        super();
        this.constituentStore.addLoadListener(constituentStoreListener);
        this.categoryStore.addEventLoadListener(WorkObjectPlanningEvents.ROOT_CATEGORIES_LOADED);
    }

    /** @return Store used to temporary store categories retrieved for constituent planning. */
    public ServiceStore<CategoryModelData> getCategoryStore()
    {
        return this.categoryStore;
    }

    public ServiceStore<ConstituentModelData> getConstituentStore()
    {
        return this.constituentStore;
    }

    // Categories
    public void loadCategoriesForConstituentMode()
    {
        categoryStore.setPath(ServicesPath.PROJECTS + MainModel.currentProject.getId() + "/" + ServicesPath.CATEGORIES);

        categoryStore.reload();
    }

    public void loadCategoriesCategory(final TaskGroupModelData taskGroup)
    {
        BaseCallback callback = new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                onCategoryCategoriesLoaded(taskGroup, response);
            }
        };
        RestRequestHandlerSingleton.getInstance().getRequest(
                PlanningMainModel.getCurrentPlanning().getPath() + "/" + ServicesPath.WORKOBJECT_CATEGORY_RANGE
                        + taskGroup.getObjectId(), callback);
    }

    private void onCategoryCategoriesLoaded(final TaskGroupModelData taskGroup, Response response)
    {
        try
        {
            JsonRepresentation jsonRepresentation = new JsonRepresentation(response.getEntity());
            JSONValue value = JSONParser.parse(jsonRepresentation.getText());
            JSONObject object = value.isObject();
            JSONObject workObjectPlanning = object.get("workObjectPlanning").isObject();

            JSONArray children = workObjectPlanning.get("childs").isArray();
            FastMap<Date> startDates = new FastMap<Date>();
            FastMap<Date> endDates = new FastMap<Date>();

            List<TimedCategoryModelData> categories = new ArrayList<TimedCategoryModelData>();

            if (children != null)
            {
                for (int i = 0; i < children.size(); i++)
                {
                    JSONObject child = children.get(i).isObject();
                    if (child != null)
                    {
                        JSONNumber idNumber = child.get("categoryID").isNumber();
                        Double idDouble = new Double(idNumber.doubleValue());
                        Long id = idDouble.longValue();
                        Date startDate = null;
                        Date endDate = null;

                        String startDateString = child.get("startDate").isString().stringValue();
                        if (!Util.isEmptyString(startDateString))
                        {
                            startDate = DateFormat.TIMESTAMP_FORMAT.parse(startDateString);
                            startDates.put(id.toString(), startDate);
                        }

                        String endDateString = child.get("endDate").isString().stringValue();
                        if (!Util.isEmptyString(endDateString))
                        {
                            endDate = DateFormat.TIMESTAMP_FORMAT.parse(endDateString);
                            endDates.put(id.toString(), endDate);
                        }

                        String name = child.get("categoryName").isString().stringValue();
                        if (startDate != null && endDate != null)
                        {
                            TimedCategoryModelData category = new TimedCategoryModelData();
                            category.setId(id);
                            category.setName(name);
                            category.setStartDate(startDate);
                            category.setEndDate(endDate);
                            categories.add(category);
                        }
                    }
                }
            }

            AppEvent event = new AppEvent(WorkObjectPlanningEvents.CATEGORY_CATEGORIES_LOADED);
            event.setData(WorkObjectPlanningEvents.TASK_GROUP_VAR, taskGroup);
            event.setData(WorkObjectPlanningEvents.CATEGORIES_VAR, categories);

            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing caegories data", e);
        }
    }

    public void loadCategoriesData(final ServiceStore<CategoryModelData> categories)
    {
        BaseCallback callback = new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                onRootCategoriesDataLoaded(categories.getModels(), response);
            }
        };
        RestRequestHandlerSingleton.getInstance().getRequest(
                PlanningMainModel.currentPlanning.getPath() + "/" + ServicesPath.WORKOBJECT_CATEGORY_RANGE
                        + categories.getAt(0).getParent(), callback);
    }

    private void onRootCategoriesDataLoaded(List<CategoryModelData> categories, Response response)
    {
        try
        {
            FastMap<TimedCategoryModelData> timedCategories = new FastMap<TimedCategoryModelData>();

            JsonRepresentation jsonRepresentation = new JsonRepresentation(response.getEntity());
            JSONValue value = JSONParser.parse(jsonRepresentation.getText());
            JSONObject object = value.isObject();
            if (object.get("workObjectPlanning") != null)
            {
                JSONObject workObjectPlanning = object.get("workObjectPlanning").isObject();
                JSONArray children = workObjectPlanning.get("childs").isArray();

                if (children != null)
                {
                    for (int i = 0; i < children.size(); i++)
                    {
                        JSONObject child = children.get(i).isObject();
                        if (child != null)
                        {
                            TimedCategoryModelData category = new TimedCategoryModelData();

                            JSONNumber idNumber = child.get("categoryID").isNumber();
                            Double idDouble = new Double(idNumber.doubleValue());
                            Long id = idDouble.longValue();

                            String name = child.get("categoryName").isString().stringValue();

                            String startDateString = child.get("startDate").isString().stringValue();
                            Date startDate = null;
                            if (!Util.isEmptyString(startDateString))
                            {
                                startDate = DateFormat.TIMESTAMP_FORMAT.parse(startDateString);
                            }

                            String endDateString = child.get("endDate").isString().stringValue();
                            Date endDate = null;
                            if (!Util.isEmptyString(endDateString))
                            {
                                endDate = DateFormat.TIMESTAMP_FORMAT.parse(endDateString);
                            }

                            category.setId(id);
                            category.setName(name);
                            category.setStartDate(startDate);
                            category.setEndDate(endDate);

                            timedCategories.put(id.toString(), category);
                        }
                    }
                }
            }
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_CATEGORIES_DATA_LOADED, timedCategories);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing categories data", e);
        }
    }

    public void loadConstituentsCategory(TaskGroupModelData taskGroup)
    {
        constituentStore.setPath(MainModel.currentProject.getDefaultPath() + "/" + ServicesPath.CONSTITUENTS);
        constituentStore.clearParameters();
        constituentStore.addParameter(new EqConstraint("category.id", taskGroup.getObjectId()));

        constituentStoreListener.setTaskGroup(taskGroup);
        constituentStore.reload();
    }

}
