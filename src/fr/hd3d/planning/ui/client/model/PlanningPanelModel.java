package fr.hd3d.planning.ui.client.model;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.modeldata.reader.MilestoneReader;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;


/**
 * Model handling planning widget data.
 * 
 * @author HD3D
 */
public class PlanningPanelModel
{
    /** Map used to know if a planning has been already loaded. Useful to not reload it at each time it is displayed. */
    private final FastMap<Boolean> isPlanningLoadedMap = new FastMap<Boolean>();

    /** Milestone editor store */
    private final ServiceStore<MilestoneModelData> milestoneStore = new ServiceStore<MilestoneModelData>(
            new MilestoneReader());
    // TODO: Add constraint on dates.
    /** Filter to retrieve only current planning milestones. */
    private final EqConstraint milestoneFilter = new EqConstraint("planning.id");

    /**
     * Constructor : set all temporary store and their parameters.
     */
    public PlanningPanelModel()
    {
        super();

        this.milestoneStore.addEventLoadListener(WorkObjectPlanningEvents.MILESTONES_LOADED);
        this.milestoneStore.addParameter(milestoneFilter);
        this.clearPlanningLoadedMarker();
    }

    /**
     * @return Milestones of current planning.
     */
    public List<MilestoneModelData> getMileStones()
    {
        return this.milestoneStore.getModels();
    }

    /**
     * @param displayMode
     *            The display mode of which planning loading is checked.
     * @return True if planning for display mode is already loaded.
     */
    public boolean isPlanningLoaded(EPlanningDisplayMode displayMode)
    {
        return this.isPlanningLoadedMap.get(displayMode.toString());
    }

    /**
     * Set if a planning has been loaded or not.
     * 
     * @param displayMode
     *            The display mode to mark as loaded or not.
     * @param isPlanningLoaded
     *            True if planning should be marked as loaded.
     */
    public void setIsPlanningLoaded(EPlanningDisplayMode displayMode, Boolean isPlanningLoaded)
    {
        this.isPlanningLoadedMap.put(displayMode.toString(), isPlanningLoaded);
    }

    /**
     * Reset all loading markers : only task type mode is marked as already loaded, others are marked to not loaded.
     */
    public void clearPlanningLoadedMarker()
    {
        this.isPlanningLoadedMap.put(EPlanningDisplayMode.TASK_TYPE.toString(), Boolean.TRUE);
        this.isPlanningLoadedMap.put(EPlanningDisplayMode.TASK_TYPE_SHOT.toString(), Boolean.FALSE);
        this.isPlanningLoadedMap.put(EPlanningDisplayMode.CONSTITUENT.toString(), Boolean.FALSE);
        this.isPlanningLoadedMap.put(EPlanningDisplayMode.SHOT.toString(), Boolean.FALSE);
    }

    /** Reload milestones for current planning. MILESTONES_LOADED event is forwarded when loading finishes. */
    public void reloadMilestones()
    {
        this.milestoneFilter.setLeftMember(PlanningMainModel.getCurrentPlanning().getId());
        this.milestoneStore.setPath(PlanningMainModel.getCurrentPlanning().getDefaultPath() + "/"
                + ServicesPath.MILESTONES);
        this.milestoneStore.reload();
    }
}
