package fr.hd3d.planning.ui.client.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PagingTaskReader;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FastLongMap;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.planning.ui.client.config.EUnassignedFilter;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;


/**
 * Model handling main data.
 * 
 * @author HD3D
 */
public class PlanningMainModel extends MainModel
{
    /**
     * Change field name is use to set a change field on planning. If planning, is changed, it should be saved, when
     * save button is clicked.
     */
    public static final String CHANGED_FIELD = "change";

    /** Planning on which user is working. */
    public static PlanningModelData currentPlanning = null;

    /** Displaying mode used for task backgrounds. */
    public static ETaskDisplayMode displayMode = ETaskDisplayMode.TASK_TYPE;

    /** True if auto save mode is enabled. */
    public static Boolean isAutoSave = false;

    // public static DateWrapper today;
    /** Tasks that were modified and that should be saved. */
    private static final FastMap<TaskModelData> modifiedTasks = new FastMap<TaskModelData>();

    public static final FastMap<TaskModelData> getPlanningModifiedTasks()
    {
        return modifiedTasks;
    }

    public static Hd3dModelData getPlanningModifiedTask(TaskModelData task)
    {
        return modifiedTasks.get(task.getId().toString());
    }

    public static PlanningModelData getCurrentPlanning()
    {
        return currentPlanning;
    }

    public static void setCurrentPlanning(PlanningModelData planning)
    {
        currentPlanning = planning;
    }

    private static FastLongMap<TaskModelData> selectedTasks = new FastLongMap<TaskModelData>();

    public static void addSelectedTask(TaskModelData task)
    {
        selectedTasks.put(task.getId(), task);
    }

    public static void removeSelectedTask(TaskModelData task)
    {
        selectedTasks.remove(task.getId());
    }

    /** Store containing task of unassigned task grid. */
    private final ServicesPagingStore<TaskModelData> unassignedTaskStore = new ServicesPagingStore<TaskModelData>(
            new PagingTaskReader());

    private final ServiceStore<TaskTypeModelData> unassignedTaskTypeStore = new ServiceStore<TaskTypeModelData>(
            ReaderFactory.getTaskTypeReader());

    /** Lucene constraint used to filter data on work object name. */
    private final LuceneConstraint workObjectConstraint = new LuceneConstraint(EConstraintOperator.prefix, "wo_name",
            "", null);
    /** Task groups that were modified and that should be saved. */
    private final List<Hd3dModelData> modifiedTaskGroups = new ArrayList<Hd3dModelData>();
    /** Task groups that were created and that should be saved. */
    private final List<Hd3dModelData> createdTaskGroups = new ArrayList<Hd3dModelData>();
    /** Extra lines that were created and that should be saved. */
    private final List<Hd3dModelData> createdExtraLines = new ArrayList<Hd3dModelData>();

    /** Constraints applied to unassigned task grid. */
    private final AndConstraint unassignedConstraints = new AndConstraint();
    /** Constraint needed to retrieve unassigned tasks (with no worker set). */
    private final Constraint unassignedFilter = new Constraint(EConstraintOperator.isnull, "worker.id");
    /** Logic constraint needed to retrieved unplanned tasks (with no start date or no end date). */
    private final OrConstraint unplannedFilter = new OrConstraint(new IsNullConstraint(TaskModelData.START_DATE_FIELD),
            new IsNullConstraint(TaskModelData.END_DATE_FIELD));

    private final InConstraint taskTypeFilter = new InConstraint("taskType.id");

    private ListStore<TaskModelData> tasksStore;

    public void setTasksStore(ListStore<TaskModelData> taskListStore)
    {
        this.tasksStore = taskListStore;
        this.tasksStore.setMonitorChanges(true);
    }

    public ListStore<TaskModelData> getTasksStore()
    {
        return tasksStore;
    }

    /** Constructor : do nothing. */
    public PlanningMainModel()
    {
        this.unassignedTaskStore.addParameter(this.unassignedConstraints);
        this.unassignedTaskStore.addParameter(this.workObjectConstraint);
        this.unassignedTaskStore.addParameter(new OrderBy(Arrays.asList("taskType.name", "boundEntityTaskLinks.woPath",
                "boundEntityTaskLinks.woName")));
        this.unassignedTaskStore.groupBy(TaskModelData.WORK_OBJECT_PARENTS_NAME_FIELD);

        this.unassignedTaskTypeStore.addParameter(new OrderBy(RecordModelData.NAME_FIELD));
        this.setListeners();
    }

    /**
     * @return Task store used by the unassigned task grid.
     */
    public ServicesPagingStore<TaskModelData> getUnassignedTaskStore()
    {
        return this.unassignedTaskStore;
    }

    /**
     * @return Task type store used by the task type combo box of unassigned task grid.
     */
    public ServiceStore<TaskTypeModelData> getUnassignedTaskTypeStore()
    {
        return this.unassignedTaskTypeStore;
    }

    /**
     * @return The list of modified tasks.
     */
    public ArrayList<Hd3dModelData> getModifiedTasks()
    {
        ArrayList<Hd3dModelData> taskList = new ArrayList<Hd3dModelData>();

        for (TaskModelData task : modifiedTasks.values())
        {
            taskList.add(task);
        }

        return taskList;
    }

    /**
     * @return The list of modified task groups.
     */
    public List<Hd3dModelData> getModifiedTaskGroups()
    {
        return this.modifiedTaskGroups;
    }

    /**
     * Add <i>task</i> to the list of modified tasks.
     * 
     * @param task
     *            The task to save.
     */
    public void addModifiedTask(TaskModelData task)
    {
        modifiedTasks.put(task.getId().toString(), task);
    }

    /**
     * Add <i>taskGroup</i> to the list of modified task groups.
     * 
     * @param taskGroup
     *            The task group to save.
     */
    public void addModifiedTaskGroup(TaskGroupModelData taskGroup)
    {
        Hd3dModelData groupToRemove = null;
        for (Hd3dModelData group : this.modifiedTaskGroups)
        {
            if (group.getId().longValue() == taskGroup.getId().longValue())
                groupToRemove = group;
        }
        if (groupToRemove != null)
            this.modifiedTaskGroups.remove(groupToRemove);
        this.modifiedTaskGroups.add(taskGroup);
    }

    /**
     * Add <i>taskGroup</i> to the list of created task groups.
     * 
     * @param taskGroup
     *            The task group to save.
     */
    public void addCreatedTaskGroup(TaskGroupModelData taskGroup)
    {
        this.createdTaskGroups.add(taskGroup);
    }

    /**
     * Add <i>extraLine</i> to the list of created task extra lines.
     * 
     * @param taskGroup
     *            The task group to save.
     */
    public void addCreatedExtraLine(ExtraLineModelData extraLine)
    {
        this.createdExtraLines.add(extraLine);
    }

    /**
     * Save planning, if it is marked as changed via its CHANGE_FIELD. If saving succeeds or planning is not changed,
     * PLANNING_SAVED event is forwarded.
     */
    public void savePlanning()
    {
        // if (currentPlanning != null || (Boolean) currentPlanning.get(CHANGED_FIELD))
        // {
        // currentPlanning.save(PlanningAppEvents.PLANNING_SAVED);
        // }
        // else
        // {
        EventDispatcher.forwardEvent(PlanningAppEvents.PLANNING_SAVED);
        // }
        // currentPlanning.set(CHANGED_FIELD, false);
    }

    /**
     * Save all modified task groups. TASK_GROUPS_SAVED event is forwarded when it succeeds. If there is nothing to save
     * the event is forwarded.
     */
    public void saveTaskGroups()
    {
        if (CollectionUtils.isEmpty(this.modifiedTaskGroups))
        {
            EventDispatcher.forwardEvent(PlanningAppEvents.TASK_GROUPS_SAVED);
        }
        else
        {
            BulkRequests.bulkPut(this.modifiedTaskGroups, PlanningAppEvents.TASK_GROUPS_SAVED);
        }
    }

    /**
     * Save all modified tasks. TASKS_SAVED event is forwarded when it succeeds. If there is nothing to save the event
     * is forwarded.
     */
    public void saveTasks()
    {
        if (CollectionUtils.isEmpty(modifiedTasks.values()))
        {
            EventDispatcher.forwardEvent(PlanningAppEvents.TASKS_SAVED);
        }
        else
        {
            ArrayList<Hd3dModelData> taskList = new ArrayList<Hd3dModelData>();
            // Temporary work around to remove non positive extra line IDs
            for (TaskModelData task : modifiedTasks.values())
            {
                task.set(TaskModelData.EXTRA_LINE_ID_FIELD, null);
                task.setDefaultPath(ServicesPath.getTasksPath(task.getId()));
                taskList.add(task);
            }
            BulkRequests.bulkPut(taskList, PlanningAppEvents.TASKS_SAVED);
        }
    }

    /**
     * Save all created extra lines. EXTRA_LINES_CREATED event is forwarded when it succeeds. If there is nothing to
     * save the event is forwarded.
     */
    public void saveCreatedExtraLines()
    {
        EventDispatcher.forwardEvent(PlanningAppEvents.EXTRA_LINES_CREATED);
    }

    /**
     * Clear the modified tasks list.
     */
    public void clearTasksToSave()
    {
        modifiedTasks.clear();
    }

    /**
     * Clear the modified task groups list.
     */
    public void clearTaskGroupsToSave()
    {
        this.modifiedTaskGroups.clear();
        this.createdTaskGroups.clear();
    }

    /**
     * Clear the created task groups list.
     */
    public void clearTaskGroupsToCreate()
    {
        this.createdTaskGroups.clear();
    }

    /**
     * Clear the created extra lines list.
     */
    public void clearExtraLineToCreate()
    {
        this.createdExtraLines.clear();
    }

    /**
     * Remove unassigned task store and apply parameters corresponding to filter.
     * 
     * @param filter
     *            The filter used to determine which parameter apply.
     */
    public void reloadUnassignedTasks(EUnassignedFilter filter)
    {
        this.unassignedConstraints.setRightMember(null);
        switch (filter)
        {
            case ALL:
                if (this.unassignedConstraints.getLeftMember() != taskTypeFilter)
                {
                    this.unassignedConstraints.setLeftMember(null);
                }
                else
                {
                    this.unassignedConstraints.setRightMember(null);
                }
                break;
            case UNASSIGNED:
                if (this.unassignedConstraints.getLeftMember() != taskTypeFilter)
                {
                    this.unassignedConstraints.setLeftMember(unassignedFilter);
                }
                else
                {
                    this.unassignedConstraints.setRightMember(unassignedFilter);
                }
                break;
            case UNPLANNED:
                if (this.unassignedConstraints.getLeftMember() != taskTypeFilter)
                {
                    this.unassignedConstraints.setLeftMember(unplannedFilter);
                }
                else
                {
                    this.unassignedConstraints.setRightMember(unplannedFilter);
                }
                break;
            default:
                break;
        }
        this.unassignedTaskStore.reload();
    }

    public void reloadUnassignedTasks(List<TaskTypeModelData> taskTypes)
    {
        taskTypeFilter.setLeftMember(CollectionUtils.getIds(taskTypes));

        if (this.unassignedConstraints.getLeftMember() == null)
        {
            this.unassignedConstraints.setLeftMember(taskTypeFilter);
        }
        else if (this.unassignedConstraints.getLeftMember() != taskTypeFilter)
        {
            this.unassignedConstraints.setRightMember(this.unassignedConstraints.getLeftMember());
            this.unassignedConstraints.setLeftMember(taskTypeFilter);
        }
        this.unassignedTaskStore.reload();
    }

    public void reloadWithoutTaskType()
    {
        if (this.unassignedConstraints.getLeftMember() == taskTypeFilter)
        {
            this.unassignedConstraints.setLeftMember(this.unassignedConstraints.getRightMember());
        }
        else if (this.unassignedConstraints.getRightMember() == taskTypeFilter)
        {
            this.unassignedConstraints.setRightMember(null);
        }
        this.unassignedTaskStore.reload();
    }

    public void updateGridTask(TaskModelData gridTask, TaskModelData task)
    {
        Record gridTaskRecord = this.getUnassignedTaskStore().getRecord(gridTask);
        Long workerId = task.getWorkerID();
        String workerName = task.getWorkerName();
        Date startDate = task.getStartDate();
        Date endDate = task.getEndDate();

        gridTaskRecord.set(TaskModelData.WORKER_ID_FIELD, null);
        gridTaskRecord.set(TaskModelData.WORKER_ID_FIELD, workerId);
        gridTaskRecord.set(TaskModelData.WORKER_NAME_FIELD, null);
        gridTaskRecord.set(TaskModelData.WORKER_NAME_FIELD, workerName);
        gridTaskRecord.set(TaskModelData.START_DATE_FIELD, null);
        gridTaskRecord.set(TaskModelData.START_DATE_FIELD, startDate);
        gridTaskRecord.set(TaskModelData.END_DATE_FIELD, null);
        gridTaskRecord.set(TaskModelData.END_DATE_FIELD, endDate);
    }

    private void setListeners()
    {
        this.unassignedTaskStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                for (TaskModelData task : unassignedTaskStore.getModels())
                {
                    TaskModelData taskModified = modifiedTasks.get(task.getId().toString());
                    if (taskModified != null)
                        updateGridTask(task, taskModified);
                }
            }
        });
    }

    public void updateWorkObjectConstraint(String value)
    {
        if (value == null)
            value = "";
        this.workObjectConstraint.setLeftMember(value);
    }

}
