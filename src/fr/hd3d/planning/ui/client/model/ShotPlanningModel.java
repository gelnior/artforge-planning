package fr.hd3d.planning.ui.client.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.ext.json.JsonRepresentation;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.modeldata.reader.ShotReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.listener.SequenceShotsLoadListener;
import fr.hd3d.planning.ui.client.modeldata.TimedSequenceModelData;


public class ShotPlanningModel extends WorkObjectPlanningModel
{
    /** Store used to temporary store sequences retrieved for shot planning. */
    private final ServiceStore<SequenceModelData> sequenceStore = new ServiceStore<SequenceModelData>(
            new SequenceReader());
    /** Store used to temporary store shots retrieved for constituent planning. */
    private final ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(new ShotReader());

    private final SequenceShotsLoadListener shotStoreListener = new SequenceShotsLoadListener();

    public ShotPlanningModel()
    {
        super();

        this.sequenceStore.addEventLoadListener(WorkObjectPlanningEvents.ROOT_SEQUENCES_LOADED);
        this.shotStore.addLoadListener(shotStoreListener);
    }

    /** @return Store used to temporary store sequences retrieved for shot planning. */
    public ServiceStore<SequenceModelData> getSequenceStore()
    {
        return this.sequenceStore;
    }

    public ServiceStore<ShotModelData> getShotStore()
    {
        return this.shotStore;
    }

    public void loadSequencesData(final ServiceStore<SequenceModelData> sequences)
    {
        BaseCallback callback = new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                onRootSequencesDataLoaded(sequences.getModels(), response);
            }
        };
        RestRequestHandlerSingleton.getInstance().getRequest(
                PlanningMainModel.currentPlanning.getPath() + "/" + ServicesPath.WORKOBJECT_SEQUENCE_RANGE
                        + sequences.getAt(0).getParent(), callback);
    }

    private void onRootSequencesDataLoaded(List<SequenceModelData> categories, Response response)
    {
        try
        {
            FastMap<TimedSequenceModelData> timedSequences = new FastMap<TimedSequenceModelData>();

            JsonRepresentation jsonRepresentation = new JsonRepresentation(response.getEntity());
            JSONValue value = JSONParser.parse(jsonRepresentation.getText());
            JSONObject object = value.isObject();
            JSONObject workObjectPlanning = object.get("workObjectPlanning").isObject();
            JSONArray children = workObjectPlanning.get("childs").isArray();

            if (children != null)
            {
                for (int i = 0; i < children.size(); i++)
                {
                    JSONObject child = children.get(i).isObject();
                    if (child != null)
                    {
                        TimedSequenceModelData sequence = new TimedSequenceModelData();

                        JSONNumber idNumber = child.get("sequenceID").isNumber();
                        Double idDouble = new Double(idNumber.doubleValue());
                        Long id = idDouble.longValue();

                        String name = child.get("sequenceName").isString().stringValue();

                        String startDateString = child.get("startDate").isString().stringValue();
                        Date startDate = null;
                        if (!Util.isEmptyString(startDateString))
                        {
                            startDate = DateFormat.TIMESTAMP_FORMAT.parse(startDateString);
                        }

                        String endDateString = child.get("endDate").isString().stringValue();
                        Date endDate = null;
                        if (!Util.isEmptyString(endDateString))
                        {
                            endDate = DateFormat.TIMESTAMP_FORMAT.parse(endDateString);
                        }

                        sequence.setId(id);
                        sequence.setName(name);
                        sequence.setStartDate(startDate);
                        sequence.setEndDate(endDate);

                        timedSequences.put(id.toString(), sequence);
                    }
                }
            }

            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.ROOT_SEQUENCES_DATA_LOADED, timedSequences);

        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing root sequences data", e);
        }
    }

    // Sequences
    public void loadSequencesForShotMode()
    {
        sequenceStore.setPath(ServicesPath.PROJECTS + MainModel.currentProject.getId() + "/" + ServicesPath.SEQUENCES);

        sequenceStore.reload();
    }

    public void loadSequencesSequence(final TaskGroupModelData taskGroup)
    {
        BaseCallback callback = new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                onSequenceSequencesLoaded(taskGroup, response);
            }
        };
        RestRequestHandlerSingleton.getInstance().getRequest(
                PlanningMainModel.currentPlanning.getPath() + "/" + ServicesPath.WORKOBJECT_SEQUENCE_RANGE
                        + taskGroup.getObjectId(), callback);
    }

    private void onSequenceSequencesLoaded(final TaskGroupModelData taskGroup, Response response)
    {
        try
        {
            JsonRepresentation jsonRepresentation = new JsonRepresentation(response.getEntity());

            JSONValue value = JSONParser.parse(jsonRepresentation.getText());
            JSONObject object = value.isObject();
            JSONObject workObjectPlanning = object.get("workObjectPlanning").isObject();

            JSONArray children = workObjectPlanning.get("childs").isArray();
            FastMap<Date> startDates = new FastMap<Date>();
            FastMap<Date> endDates = new FastMap<Date>();

            List<TimedSequenceModelData> sequences = new ArrayList<TimedSequenceModelData>();

            if (children != null)
            {
                for (int i = 0; i < children.size(); i++)
                {
                    JSONObject child = children.get(i).isObject();
                    if (child != null)
                    {
                        JSONNumber idNumber = child.get("sequenceID").isNumber();
                        Double idDouble = new Double(idNumber.doubleValue());
                        Long id = idDouble.longValue();
                        Date startDate = null;
                        Date endDate = null;
                        String startDateString = child.get("startDate").isString().stringValue();
                        if (!Util.isEmptyString(startDateString))
                        {
                            startDate = DateFormat.TIMESTAMP_FORMAT.parse(startDateString);
                            startDates.put(id.toString(), startDate);
                        }

                        String endDateString = child.get("endDate").isString().stringValue();
                        if (!Util.isEmptyString(endDateString))
                        {
                            endDate = DateFormat.TIMESTAMP_FORMAT.parse(endDateString);
                            endDates.put(id.toString(), endDate);
                        }

                        String name = child.get("sequenceName").isString().stringValue();

                        if (startDate != null && endDate != null)
                        {
                            TimedSequenceModelData sequence = new TimedSequenceModelData();
                            sequence.setId(id);
                            sequence.setName(name);
                            sequence.setStartDate(startDate);
                            sequence.setEndDate(endDate);
                            sequences.add(sequence);
                        }
                    }
                }

            }

            AppEvent event = new AppEvent(WorkObjectPlanningEvents.SEQUENCE_SEQUENCES_LOADED);
            event.setData(WorkObjectPlanningEvents.TASK_GROUP_VAR, taskGroup);
            event.setData(WorkObjectPlanningEvents.SEQUENCES_VAR, sequences);

            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing sequences data", e);
        }
    }

    public void loadShotsSequence(TaskGroupModelData taskGroup)
    {
        shotStore.setPath(ServicesPath.SHOTS);
        shotStore.clearParameters();
        shotStore.addParameter(new EqConstraint("sequence.id", taskGroup.getObjectId()));

        shotStoreListener.setTaskGroup(taskGroup);
        shotStore.reload();
    }
}
