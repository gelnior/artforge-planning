package fr.hd3d.planning.ui.client.model;

import java.util.Arrays;
import java.util.Date;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.TaskGroupReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.ExtraFields;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;


/**
 * Model handles task type planning data and their loading.
 * 
 * @author HD3D
 */
public class TaskTypePlanningModel extends WorkObjectPlanningModel
{
    /** Task group used for initial loading. */
    private final ServiceStore<TaskGroupModelData> rootTaskGroups = new ServiceStore<TaskGroupModelData>(
            new TaskGroupReader());
    private String entityName;

    public TaskTypePlanningModel()
    {
        super();

        this.rootTaskGroups.addParameter(new OrderBy(Arrays.asList("startDate", "boundEntityTaskLinks.woName")));
        this.rootTaskGroups.addParameter(new Constraint(EConstraintOperator.isnull, "taskGroup.id"));
        this.rootTaskGroups.addEventLoadListener(WorkObjectPlanningEvents.ROOT_TASK_GROUPS_LOADED);
    }

    /**
     * @return This entity name.
     */
    public String getEntityName()
    {
        return this.entityName;
    }

    /**
     * Set entity to retrieve inside task type planning.
     * 
     * @param entityName
     *            The entity name to set.
     */
    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    /**
     * @return Root task groups of current task type planning.
     */
    public ServiceStore<TaskGroupModelData> getRootTaskGroups()
    {
        return this.rootTaskGroups;
    }

    /** Reload root task group for task type planning. ROOT_TASK_GROUPS_LOADED event is forwarded when loading finishes. */
    public void reloadTaskGroupStore()
    {
        final String taskGroupPath = PlanningMainModel.currentPlanning.getDefaultPath() + "/" + ServicesPath.TASKGROUPS;
        this.rootTaskGroups.setPath(taskGroupPath);
        this.rootTaskGroups.clearParameters();

        Date startDate = PlanningMainModel.currentPlanning.getStartDate();
        Date endDate = PlanningMainModel.currentPlanning.getEndDate();
        // startDate is between planning startDate and endDate OR endDate is between planning startDate and endDate.
        OrConstraint dateConstraint = new OrConstraint(new Constraint(EConstraintOperator.btw,
                TaskGroupModelData.START_DATE_FIELD, startDate, endDate), new Constraint(EConstraintOperator.btw,
                TaskGroupModelData.END_DATE_FIELD, startDate, endDate));
        // previous constraint OR (startDate < planning start date AND endDate > planning end date) <= Task group bar is
        // bigger than displayed planning.
        OrConstraint fullDateConstraint = new OrConstraint(new AndConstraint(new Constraint(EConstraintOperator.leq,
                TaskGroupModelData.START_DATE_FIELD, startDate), new Constraint(EConstraintOperator.geq,
                TaskGroupModelData.END_DATE_FIELD, endDate)), dateConstraint);

        AndConstraint fullConstraint = new AndConstraint(fullDateConstraint, new EqConstraint("taskType.entityName",
                this.entityName));

        rootTaskGroups.addParameter(fullConstraint);
        rootTaskGroups.addParameter(new OrderBy(TaskGroupModelData.START_DATE_FIELD));
        rootTaskGroups.reload();
    }

    /**
     * Refresh task group date by setting start date as the first start date of the tasks of which task type corresponds
     * to group task type and setting end date as the last end date of the tasks of which task type corresponds to group
     * task type. When loading is finished,TASK_GROUP_BOUNDS_SYNCHRONIZED event is forwarded with task group attached.
     * 
     * @param taskGroup
     *            The group of which dates will refreshed.
     */
    public void refreshTaskGroupDates(final TaskGroupModelData taskGroup)
    {
        final ServiceStore<TaskModelData> taskStart = new ServiceStore<TaskModelData>(new TaskReader());
        taskStart.setPath(MainModel.currentProject.getDefaultPath() + "/" + ServicesPath.TASKS);

        taskStart.addParameter(new OrderBy(TaskModelData.START_DATE_FIELD));
        taskStart.addParameter(new Pagination(0L, 1L));

        Constraints constraints = new Constraints();
        constraints.add(new EqConstraint("taskType.id", taskGroup.getTaskTypeID()));
        constraints.add(new Constraint(EConstraintOperator.isnotnull, TaskModelData.START_DATE_FIELD));
        taskStart.addParameter(constraints);

        taskStart.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onTaskStartLoaded(taskGroup, taskStart);
            }
        });
        taskStart.reload();
    }

    /**
     * When oldest task is retrieved, group start date is set with task start date. Then, most recent task is retrieved.
     * 
     * @param taskGroup
     *            The task group of which dates will be refreshed.
     * @param taskStart
     *            The oldest task.
     */
    private void onTaskStartLoaded(final TaskGroupModelData taskGroup, ServiceStore<TaskModelData> taskStart)
    {
        if (taskStart.getCount() > 0)
        {
            final TaskGroupModelData tmpTaskGroup = new TaskGroupModelData();

            tmpTaskGroup.setId(taskGroup.getId());
            tmpTaskGroup.setTaskTypeID(taskGroup.getTaskTypeID());
            tmpTaskGroup.setStartDate(taskStart.getAt(0).getStartDate());

            final ServiceStore<TaskModelData> taskEnd = new ServiceStore<TaskModelData>(new TaskReader());
            taskEnd.setPath(MainModel.currentProject.getDefaultPath() + "/" + ServicesPath.TASKS);

            taskEnd.addParameter(new OrderBy("-" + TaskModelData.END_DATE_FIELD));
            taskEnd.addParameter(new ExtraFields(Const.TOTAL_ACTIVITIES_DURATION));
            taskEnd.addParameter(new Pagination(0L, 1L));

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint("taskType.id", tmpTaskGroup.getTaskTypeID()));
            constraints.add(new Constraint(EConstraintOperator.isnotnull, TaskModelData.END_DATE_FIELD));
            taskEnd.addParameter(constraints);

            taskEnd.addLoadListener(new LoadListener() {
                @Override
                public void loaderLoad(LoadEvent le)
                {
                    onTaskEndLoaded(tmpTaskGroup, taskEnd);
                }
            });
            taskEnd.reload();
        }
    }

    /**
     * When most recent task is loaded, task group end date is updated with task date, then
     * TASK_GROUP_BOUNDS_SYNCHRONIZED event is forwarded with task group as attachment.
     * 
     * @param taskGroup
     *            The task group of which task should be retrieved.
     * @param taskEnd
     *            The most recent task.
     */
    private void onTaskEndLoaded(TaskGroupModelData taskGroup, ServiceStore<TaskModelData> taskEnd)
    {
        if (taskEnd.getCount() > 0)
        {
            taskGroup.setEndDate(taskEnd.getAt(0).getEndDate());
            EventDispatcher.forwardEvent(WorkObjectPlanningEvents.TASK_GROUP_BOUNDS_SYNCHRONIZED, taskGroup);
        }
    }

}
