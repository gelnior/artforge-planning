package fr.hd3d.planning.ui.client.model.listener;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;


public class SequenceShotsLoadListener extends LoadListener
{

    private TaskGroupModelData taskGroup;

    @Override
    public void loaderLoad(LoadEvent le)
    {
        EventDispatcher.forwardEvent(WorkObjectPlanningEvents.SEQUENCE_SHOTS_LOADED, taskGroup);
    }

    public void setTaskGroup(TaskGroupModelData taskGroup)
    {
        this.taskGroup = taskGroup;
    }
}
