package fr.hd3d.planning.ui.client.view;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.DomQuery;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Region;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.util.FastLongMap;
import fr.hd3d.common.ui.client.util.IdGenerator;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.PlanningAbsolutePanel;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.ColumnBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskGroupBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;
import fr.hd3d.common.ui.client.widget.planning.widget.line.TaskGroupLine;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.BasePlanningTree;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.ExtraLineItem;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;
import fr.hd3d.planning.ui.client.controller.WorkObjectPlanningController;
import fr.hd3d.planning.ui.client.dnd.move.TaskDraggable;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;


/**
 * Base class for building a planning. It gives all methods to add easily task bars, task group bars, extra lines and
 * makes operations on them.
 * 
 * @author HD3D
 */
public abstract class WorkObjectPlanningView extends PlanningAbsolutePanel
{

    /** Controller handling work object events. */
    protected WorkObjectPlanningController controller;
    /** Panel containing this planning. */
    protected final PlanningPanelView parentPanel;

    /** Task group items displayed inside planning. */
    protected final FastMap<TaskGroupItem> taskGroupItems = new FastMap<TaskGroupItem>();
    /** Task group lines displayed inside planning. */
    protected final FastMap<TaskGroupLine> taskGroupLines = new FastMap<TaskGroupLine>();

    /** Displayed worker lines. */
    protected final FastMap<ExtraLine> extraLines = new FastMap<ExtraLine>();
    /** Task bars displayed inside planning. */
    protected final FastLongMap<TaskBar> taskBars = new FastLongMap<TaskBar>();

    /** Columns emphasizing the fact that there are milestones on planning. */
    protected final FastMap<ColumnBar> milestoneColumns = new FastMap<ColumnBar>();

    /** Column representing current day inside planning. */
    protected ColumnBar currentDayColumn = new ColumnBar(ColumnBar.DAY_COLUMN_COLOR);

    /** Tree that allows navigation inside planning. */
    protected BasePlanningTree tree;

    Boolean isMoving = Boolean.FALSE;

    int previousX = 0;
    int previousY = 0;

    /**
     * Default constructor.
     * 
     * @param parentPanel
     *            The panel containing this widget.
     */
    public WorkObjectPlanningView(final PlanningPanelView parentPanel)
    {
        this.parentPanel = parentPanel;

        this.setStyleAttribute("border-top", "1px solid #99BBE8");
        this.setStyleAttribute("border-right", "1px solid #99BBE8");

        final Listener<ComponentEvent> listener = new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                int x = ce.getClientX();
                int y = ce.getClientY();
                int distanceX = x - previousX;
                int distanceY = y - previousY;
                parentPanel.scrollPlanning(-distanceX, -distanceY);
                previousX = x;
                previousY = y;
            }
        };

        this.addListener(Events.OnMouseDown, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                addListener(Events.OnMouseMove, listener);
                previousX = ce.getClientX();
                previousY = ce.getClientY();
                ce.cancelBubble();
                ce.setCancelled(true);
                ce.preventDefault();
            }
        });

        this.addListener(Events.OnMouseOut, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                // removeListener(Events.OnMouseMove, listener);
            }
        });
        this.addListener(Events.OnMouseUp, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be)
            {
                removeListener(Events.OnMouseMove, listener);
            }
        });
    }

    /** Makes this widget unreactive to MVC events. */
    public void idle()
    {
        this.controller.mask();
    }

    /** Made this widget reactive to MVC events. */
    public void unIdle()
    {
        this.controller.unMask();
    }

    /**
     * Add a column bar of which color and date is the same as the milestone given in parameter. Column is registered
     * with the milestone ID.
     * 
     * @param milestone
     *            The milestone to add.
     */
    public void addMilestone(MilestoneModelData milestone)
    {
        ColumnBar milestoneColumn = new ColumnBar(milestone.getColor());
        milestoneColumn.setDate(milestone.getDate());
        milestoneColumn.setToolTip(milestone.getTitle());
        this.insert(milestoneColumn, 0);

        this.milestoneColumns.put(milestone.getId().toString(), milestoneColumn);
    }

    /**
     * Remove column bar corresponding to the milestone given in parameter.
     * 
     * @param milestone
     *            The milestone to remove.
     */
    public void removeMilestone(MilestoneModelData milestone)
    {
        ColumnBar bar = milestoneColumns.get(milestone.getId().toString());
        if (bar != null)
            this.remove(bar);
    }

    /**
     * Display current day column and set it to the right place.
     * 
     * @return displayed day column.
     */
    public ColumnBar updateDayColumn()
    {
        this.add(this.currentDayColumn);
        this.currentDayColumn.setDate(new Date());
        this.currentDayColumn.setToolTip("Today");

        return this.currentDayColumn;
    }

    /**
     * Register navigation tree linked to this planning.
     * 
     * @param customTree
     */
    public void setCustomTree(BasePlanningTree customTree)
    {
        this.tree = customTree;
    }

    /**
     * @return Task group selected in planning tree.
     */
    public TaskGroupItem getTreeSelectedItem()
    {
        return (TaskGroupItem) this.tree.getSelectedItem();
    }

    /** Remove every tree items. */
    public void clearTree()
    {
        for (TaskGroupItem taskGroupItem : this.taskGroupItems.values())
        {
            taskGroupItem.clearChildren();
            if (taskGroupItem.getTaskGroup().getTaskGroupID() == null)
            {
                if (tree.getWidgetIndex(taskGroupItem) >= 0)
                    tree.remove(taskGroupItem);
            }
        }
        this.taskGroupItems.clear();
    }

    /** Remove all children from this widget. Clears data from registration. */
    public void clear()
    {
        for (TaskGroupLine taskGroupLine : this.taskGroupLines.values())
        {
            taskGroupLine.clearChildren();
            Long taskGroupid = taskGroupLine.getTaskGroupItem().getTaskGroup().getTaskGroupID();
            if (taskGroupid == null)
            {
                if (this == taskGroupLine.getParent())
                    this.remove(taskGroupLine);
            }
        }
        this.extraLines.clear();
        this.taskBars.clear();
        this.taskGroupLines.clear();

        for (ColumnBar milestoneColumn : this.milestoneColumns.values())
        {
            if (milestoneColumn.getParent() == this)
                this.remove(milestoneColumn);
        }
        this.milestoneColumns.clear();

        if (this.currentDayColumn.getParent() == this)
            this.remove(this.currentDayColumn);

        this.resetBorders();
    }

    public void changeHeight(TaskGroupItem taskGroupItem)
    {
        TaskGroupLine taskGroupLine = this.taskGroupLines.get(taskGroupItem.getTaskGroup().getId().toString());
        taskGroupLine.getChildren().setVisible(taskGroupItem.isOpen());
    }

    /**
     * Expand given task group.
     * 
     * @param taskGroup
     *            The task group to expand.
     */
    public void setTaskGroupOpen(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = this.taskGroupItems.get(taskGroup.getId().toString());
        taskGroupItem.setOpen(true);
        taskGroupItem.changeState();
        this.changeHeight(taskGroupItem);
    }

    /**
     * For all displayed tasks, it recalculates the task color depending on current display mode.
     */
    public void updateTaskColors()
    {
        for (ExtraLine lines : this.extraLines.values())
        {
            lines.updateTaskColors(PlanningMainModel.displayMode);
        }
    }

    /**
     * Add task group (line and item) to planning corresponding to display mode. If task group has no parent it is
     * directly attached to the root of the tree. Else it is attached beneath its parent. By default task group is
     * collapsed. <br>
     * The result is that a task group item is attached inside tree and task group line is created inside planning
     * panel. Moreover a task group bar is added to the task group line to display on which dates the task group is
     * planned. Every task group line and items are registered by its ID in a map.
     * 
     * @param taskGroup
     *            Task group to add.
     */
    public void addTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = new TaskGroupItem(taskGroup);
        if (taskGroup.getTaskGroupID() == null)
        {
            this.tree.addTaskGroupItem(taskGroupItem);
            this.taskGroupItems.put(taskGroup.getId().toString(), taskGroupItem);
        }
        else
        {
            TaskGroupItem parentItem = this.taskGroupItems.get(taskGroup.getTaskGroupID().toString());
            parentItem.addItem(taskGroupItem);
            this.taskGroupItems.put(taskGroup.getId().toString(), taskGroupItem);
        }
        this.addTaskGroupLine(taskGroup.getType(), taskGroup, taskGroupItem);
    }

    /**
     * Add task group line to planning panel corresponding to display mode. If task group has no parent, it is added
     * directly to panel, else it is added as a children to one of the already added task group.
     * 
     * @param displayMode
     *            Display mode is needed to know which planning is concerned by this addition.
     * @param taskGroup
     *            The task group to add.
     * @param groupItem
     *            The item linked to the line to add.
     */
    public void addTaskGroupLine(EPlanningDisplayMode displayMode, TaskGroupModelData taskGroup, TaskGroupItem groupItem)
    {
        TaskGroupBar taskGroupBar = new TaskGroupBar(PlanningPanelView.timeLine, taskGroup, "000000",
                taskGroup.getColor());
        if (displayMode == EPlanningDisplayMode.TASK_TYPE)
        {
            TaskDraggable.createTaskGroupDraggableResizable(taskGroupBar, this);
        }

        TaskGroupLine taskGroupLine = new TaskGroupLine(PlanningPanelView.timeLine, groupItem);
        taskGroupLine.setGroupBar(taskGroupBar);

        if (taskGroup.getTaskGroupID() == null)
        {
            this.add(taskGroupLine);
            this.changePlanningScrollBar(taskGroupBar);
            this.layout();
        }
        else
        {
            TaskGroupLine parentContainer = this.taskGroupLines.get(taskGroup.getTaskGroupID().toString());
            parentContainer.addChild(taskGroupLine);
            parentContainer.getChildren().layout();

            this.layout();
            parentContainer.layout();
            parentContainer.getChildren().setVisible(true);
        }

        this.taskGroupLines.put(taskGroup.getId().toString(), taskGroupLine);
    }

    private void changePlanningScrollBar(TaskGroupBar groupPanel)
    {
        if (PlanningPanelView.timeLine.getWrappers() != null && PlanningPanelView.timeLine.getWrappers().length > 0)
        {
            DateWrapper startDate = PlanningPanelView.timeLine.getWrappers()[0];
            Date endDate = groupPanel.getStartDate();
            int nbDays = Hd3dDateUtil.daysBetween(startDate.asDate(), endDate);
            this.parentPanel.getPlanningScrollPanel().setHorizontalScrollPosition(
                    nbDays * BasePlanningWidget.getCellWidth());
        }
    }

    /**
     * Launch GWT layout function on task group line corresponding to task group.
     * 
     * @param taskGroup
     *            The task group to layout.
     */
    public void layoutTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        taskGroupLine.layoutLines();
    }

    /**
     * Expand corresponding task group while loading its data.
     * 
     * @param taskGroup
     *            Task group to expand.
     */
    public void expandFirst(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = this.taskGroupItems.get(taskGroup.getId().toString());
        taskGroupItem.refreshChildren();
    }

    /**
     * Make corresponding task group item children visible, same for corresponding task group line children.
     * 
     * @param taskGroup
     *            Task group to expand.
     */
    public void expandTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = this.taskGroupItems.get(taskGroup.getId().toString());
        TaskGroupLine taskGroupLine = this.taskGroupLines.get(taskGroup.getId().toString());

        if (taskGroupItem != null)
            taskGroupItem.expand();
        else
            Logger.warn("No task group item has been found for " + taskGroup);
        if (taskGroupLine != null)
            taskGroupLine.expand();
        else
            Logger.warn("No task group line has been found for " + taskGroup);
    }

    /**
     * Refresh selected task group line corresponding to task group by collapsing task group item, clearing it then
     * expand it as it was its first expand (it means expand it and load its data).
     * 
     * @param taskGroup
     *            Task group to refresh.
     */
    public void refreshTaskGroup(TaskGroupModelData taskGroup)
    {
        this.collapseTaskGroup(taskGroup);
        this.clearTaskGroup(taskGroup);
        this.expandFirst(taskGroup);
    }

    /**
     * Remove children from corresponding tree item and planning line.
     * 
     * @param taskGroup
     *            Task group to clear.
     */
    private void clearTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = taskGroupItems.get(taskGroup.getId().toString());
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        taskGroupItem.clearChildren();
        taskGroupLine.clearChildren();
    }

    /**
     * Hide children from corresponding tree item and planning line.
     * 
     * @param taskGroup
     *            Task group to collapse.
     */
    public void collapseTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = taskGroupItems.get(taskGroup.getId().toString());
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());

        taskGroupItem.collapse();
        taskGroupLine.collapse();
    }

    /**
     * @param region
     *            Region used to retrieve extra line.
     * @return Extra line which is in the place described by region (Y top coordinate is between region Y coordinates.
     */
    public ExtraLine getExtraLine(Region region, EPlanningDisplayMode mode)
    {
        for (ExtraLine line : extraLines.values())
        {
            if (DomQuery.is(line.getElement(), "div." + ExtraLine.EXTRA_LINE_MOUSE_OVER_EVENT))
            {
                Region lineRegion = line.el().getRegion();

                if (region.top > lineRegion.top && region.top < lineRegion.top + line.el().getHeight())
                {
                    return line;
                }
            }
        }
        return null;
    }

    /**
     * @param taskGroup
     *            Task group to check.
     * @return True if tasks of <i>taskGroup</i> are already loaded.
     */
    public boolean isTaskGroupLoaded(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = this.taskGroupItems.get(taskGroup.getId().toString());
        return taskGroupItem.isLoaded();
    }

    /**
     * Mark task group as loaded.
     * 
     * @param taskGroup
     *            The task group to mark as loaded.
     */
    public void setTaskGroupLoaded(TaskGroupModelData taskGroup)
    {
        TaskGroupItem taskGroupItem = this.taskGroupItems.get(taskGroup.getId().toString());
        taskGroupItem.setFirstExpand(false);
    }

    /**
     * Add a dirty marker (modification marker) to task group bar corresponding to <i>taskGroup</i>.
     * 
     * @param taskGroup
     *            The modified task group.
     */
    public void setTaskGroupDirty(TaskGroupModelData taskGroup)
    {
        TaskGroupLine taskGroupLine = this.taskGroupLines.get(taskGroup.getId().toString());
        taskGroupLine.setModificationIndicator();
    }

    /**
     * Add an extra line to <i>taskGroup</i> children.
     * 
     * @param taskGroup
     *            Task group inside which a line will be added.
     * @param extraLineModelData
     *            Line informations.
     * @return Added line.
     */
    ExtraLine addExtraLine(TaskGroupModelData taskGroup, ExtraLineModelData extraLineModelData)
    {
        TaskGroupItem taskGroupItem = taskGroupItems.get(taskGroup.getId().toString());
        return addExtraLine(taskGroupItem, extraLineModelData);
    }

    /**
     * Add an extra line to task group (tree item and task group line).
     * 
     * @param taskGroup
     *            Task group item inside which a line will be added.
     * @param extraLineModelData
     *            Line informations.
     * @return Added line.
     */
    public ExtraLine addExtraLine(TaskGroupItem taskGroupItem, ExtraLineModelData extraLineModelData)
    {
        ExtraLineItem extraLineItem = this.tree.addExtraLine(taskGroupItem, extraLineModelData);

        extraLineModelData.setTaskGroupID(taskGroupItem.getTaskGroup().getId());

        ExtraLine extraLine = new ExtraLine(PlanningPanelView.timeLine);

        extraLine.setExtraLineItem(extraLineItem);
        extraLine.setTaskGroup(taskGroupItem.getTaskGroup());
        extraLine.setWidth(PlanningPanelView.timeLine.getWrappers().length * BasePlanningWidget.getCellWidth());
        TaskGroupLine taskGroupLine = this.taskGroupLines.get(extraLineModelData.getTaskGroupID().toString());
        taskGroupLine.addChild(extraLine);
        taskGroupLine.layout();
        taskGroupLine.getChildren().layout();
        taskGroupItem.setOpen(true);
        taskGroupItem.changeState();

        this.extraLines.put(extraLine.getExtraLineItem().getExtraLine().getId().toString(), extraLine);
        // this.extraLines.put(extraLine.getExtraLineItem().getExtraLine().getPersonID().toString(), extraLine);

        return extraLine;
    }

    /**
     * Remove modification indicator (red triangle) from task bars.
     * 
     * @param tasks
     *            The tasks modified then saved of which task bar should not display anymore modification indicator.
     */
    public void removeTaskModificationIndicator(Collection<TaskModelData> tasks)
    {
        for (TaskModelData task : tasks)
        {
            TaskBar taskBar = this.taskBars.get(task.getId().toString());
            if (taskBar != null)
                taskBar.removeStyleName(CSSUtils.MODIFIED_STYLE);
        }
    }

    /**
     * Remove modification indicator (red triangle) from task bars.
     * 
     * @param tasks
     *            The tasks modified then saved of which task bar should not display anymore modification indicator.
     */
    public void removeTaskGroupModificationIndicator(List<TaskGroupModelData> taskGroups)
    {
        for (TaskGroupModelData taskGroup : taskGroups)
        {
            TaskGroupLine taskGroupLine = this.taskGroupLines.get(taskGroup.getId().toString());
            if (taskGroupLine != null)
                taskGroupLine.removeModificationIndicator();
        }
    }

    /**
     * Reload data displayed inside tool tip of the task bar corresponding to task.
     * 
     * @param task
     *            The task of which task must be reloaded.
     */
    public void refreshTask(TaskModelData task)
    {
        TaskBar taskBar = this.taskBars.get(task.getId().toString());
        if (taskBar != null)
            taskBar.refreshForTask(task);
    }

    /**
     * Fora given task, refresh task bar activity positions.
     * 
     * @param task
     *            The task to refresh.
     */
    public void refreshActivitiesForTask(TaskModelData task)
    {
        TaskBar taskBar = this.taskBars.get(task.getId().toString());
        if (taskBar != null)
        {
            taskBar.refreshActivities();
        }
    }

    /**
     * Refresh task bar tool tip corresponding to task.
     * 
     * @param task
     *            The task of which tool tip must be refreshed.
     */
    public void refreshToolTip(TaskModelData task)
    {
        TaskBar taskBar = this.taskBars.get(task.getId().toString());
        if (taskBar != null)
        {
            taskBar.refreshToolTip();
        }
    }

    /**
     * Add a task bar to the task bar map. It is needed to identify task bar to remove modification indicator.
     * 
     * @param taskBar
     *            The task bar to add.
     */
    public void registerTaskBar(TaskBar taskBar)
    {
        this.taskBars.put(taskBar.getTask().getId().toString(), taskBar);
    }

    /**
     * @param task
     *            The task of which the task is reqested.
     * @return The task bar corresponding to task.
     */
    public TaskBar getTaskBar(TaskModelData task)
    {
        return this.taskBars.get(task.getId().toString());
    }

    /**
     * @param task
     *            Remove task bar corresponding to task from the planning.
     * @return The removed bar.
     */
    public TaskBar removeTaskBar(TaskModelData task)
    {
        TaskBar bar = this.getTaskBar(task);
        if (bar != null)
        {
            if (bar.getParent() != null)
            {
                bar.getExtraLine().removeOccupation(bar);
                bar.getExtraLine().removeTask(bar);
                this.taskBars.remove(task.getId().toString());
            }
            return bar;
        }
        return null;
    }

    /**
     * Add task panel, corresponding to task, to <i>taskGroup</i>. Task group line depend of taskGroup type, if it is a
     * task type task group, a category task group...
     * 
     * @param taskGroup
     *            The task group in which task will be added.
     * @param task
     *            The task to add.
     */
    public abstract TaskBar addTaskPanel(TaskGroupModelData taskGroup, TaskModelData task);

    /**
     * Create a task panel for <i>taskGroup</i>. If no line corresponding to <i>object</i> (object of which ID is
     * <i>iineObjectId</i> and name is <i>lineObjectName</i>, a new one is created. Then task panel is added to this
     * line.
     * 
     * @param taskGroup
     *            The task group in which task will be inserted.
     * @param task
     *            The task to add to task group.
     * @param lineObjectId
     *            The object ID linked to the line in which the task will be.
     * @param lineObjectName
     *            The object name linked to the line in which the task will be.
     * @param taskBarLabel
     *            Label that will be set on created task bar.
     */
    protected TaskBar addTask(TaskGroupModelData taskGroup, TaskModelData task, Long lineObjectId,
            String lineObjectName, String taskBarLabel)
    {
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        ExtraLine line = taskGroupLine.getExtraLine(lineObjectId);

        if (line == null)
        {
            ExtraLineModelData lineModelData = new ExtraLineModelData();
            lineModelData.setId(Long.valueOf(IdGenerator.getNewId()));
            lineModelData.setName(lineObjectName);
            lineModelData.setPersonID(lineObjectId);
            lineModelData.setTaskGroup(taskGroup);

            line = this.addExtraLine(taskGroup, lineModelData);
        }

        TaskBar taskBar = null;
        if ((!BasePlanningWidget.isPreviewMode() && task.getActualStartDate() != null) || task.getStartDate() != null
                && task.getEndDate() != null && !task.getEndDate().before(task.getStartDate()))
        {
            // Movable/Resizable stuff
            taskBar = TaskBar.createInstance(PlanningPanelView.timeLine, task, taskBarLabel, this,
                    PlanningMainModel.displayMode, true);

            TaskDraggable.createDraggableResizable(taskBar, this, taskGroup.getType());
            taskBar.setLabel(taskBarLabel);

            // register task bar in planning.
            this.taskBars.put(task.getId(), taskBar);

            // Get first date of planning.
            DateWrapper firstDate = PlanningPanelView.timeLine.getWrappers()[0];
            Date firstDateDate = firstDate.asDate();
            int startNbDays = 0;
            int endNbDays = 0;

            if (!BasePlanningWidget.isPreviewMode() && task.getActualStartDate() != null)
            {
                startNbDays = CalendarUtil.getDaysBetween(firstDateDate, task.getActualStartDate());

                if (task.getActualEndDate() != null
                        && (task.getEndDate() == null || (task.getActualEndDate().after(task.getEndDate()) || task
                                .isFinished())))
                    endNbDays = CalendarUtil.getDaysBetween(firstDateDate, task.getActualEndDate());
                else if (task.getEndDate() != null && task.getEndDate().after(task.getActualStartDate()))
                    endNbDays = CalendarUtil.getDaysBetween(firstDateDate, task.getEndDate());
                else
                    endNbDays = startNbDays + task.getDayDuration().intValue();
            }
            else
            {
                if (task.getStartDate() != null)
                    startNbDays = CalendarUtil.getDaysBetween(firstDateDate, task.getStartDate());

                if (task.getEndDate() != null)
                    endNbDays = CalendarUtil.getDaysBetween(firstDateDate, task.getEndDate());
                else
                    endNbDays = startNbDays;
            }

            line.addTask(taskBar);
            line.moveToRightLine(startNbDays * PlanningPanelView.getCellWidth(),
                    (endNbDays + 1) * PlanningPanelView.getCellWidth(), taskBar, Boolean.FALSE);

            line.displayDuration();
        }
        return taskBar;
    }

    /**
     * @param taskGroup
     *            The task group to set.
     * @return All tasks set inside a given task group.
     */
    public List<TaskModelData> getTasksFromTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        return taskGroupLine.getTasks();
    }

    /**
     * Displays given activity on task bar corresponding to task on which activity is set.
     * 
     * @param activity
     *            The activity to display.
     */
    public void displayActivity(TaskActivityModelData activity)
    {
        TaskBar bar = this.taskBars.get(activity.getTaskId().toString());
        if (bar != null)
            bar.addWorkedDay(activity);
    }

    /**
     * Clear activities set inside task bar.
     * 
     * @param taskGroup
     */
    public void clearActivities(TaskGroupModelData taskGroup)
    {
        for (TaskBar bar : this.taskBars.values())
            if (bar.getExtraLine() != null && bar.getExtraLine().getTaskGroup() != null
                    && bar.getExtraLine().getTaskGroup().getId().longValue() == taskGroup.getId().longValue())
                bar.clearWorkedDay();
    }

    public void markTaskAsSelected(TaskModelData task)
    {
        TaskBar bar = this.getTaskBar(task);
        if (bar != null)
            bar.setBorderColor("red");
    }

    public void markTaskAsDeselected(TaskModelData task)
    {
        TaskBar bar = this.getTaskBar(task);
        if (bar != null)
            bar.setBorderColor("black");
    }
}
