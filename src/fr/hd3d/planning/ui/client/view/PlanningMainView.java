package fr.hd3d.planning.ui.client.view;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.planning.ui.client.controller.PlanningMainController;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.view.widget.PlanningCreationDialog;
import fr.hd3d.planning.ui.client.view.widget.PlanningToolBar;
import fr.hd3d.planning.ui.client.widget.taskgrid.UnassignedTasksGrid;


/**
 * Planning application main view. It sets the general layout and main tool bar.
 * 
 * @author HD3D
 */
public class PlanningMainView extends MainView
{
    /** Planning main model handles global data. */
    private final PlanningMainModel model = new PlanningMainModel();
    /** Planning main model handles planning main events. */
    private final PlanningMainController controller = new PlanningMainController(this.model, this);

    /** Main panel used for layout. */
    private BorderedPanel mainPanel;
    /** Tool bar containing applications main buttons and fields (like project combo box). */
    private PlanningToolBar toolBar;

    /** Grid containing project tasks. */
    private UnassignedTasksGrid taskGrid;
    /** Panel containing work object planning and user planning. */
    private PlanningPanelView workObjectUserPlanningView;

    /** Planning creation dialog allows user to enter planning settings for new planning creation. */
    private PlanningCreationDialog planningCreationDialog;

    public static FormPanel exportCsvFormPanel;

    /**
     * Constructor.
     */
    public PlanningMainView()
    {
        super("Planning");
    }

    /**
     * Register main controller to event dispatcher.
     */
    @Override
    public void init()
    {
        EventDispatcher.get().addController(controller);
    }

    /**
     * Initialize all widgets (planning, task grid...) and register sub controllers.
     */
    public void initWidgets()
    {
        this.mainPanel = new BorderedPanel();

        this.createToolbar();
        this.createTaskGridPanel();
        this.createPlanningWidgetPanel();

        this.addToViewport(mainPanel);
        this.addControllers();

        RootPanel.get().add(loadingPanel);
    }

    public void createExportCsvFormPanel()
    {
        exportCsvFormPanel = new FormPanel();
        exportCsvFormPanel.setMethod(FormPanel.METHOD_POST);
        RootPanel.get().add(exportCsvFormPanel);
    }

    /**
     * Display planning creation form dialog. This form displays two dates : start date and end date. Other planning
     * informations are automatically calculated.
     */
    public void displayPlanningCreationDialog()
    {
        if (this.planningCreationDialog == null)
        {
            this.planningCreationDialog = new PlanningCreationDialog();
        }
        this.planningCreationDialog.show();

    }

    /**
     * Reload projects available in project combo box.
     */
    public void reloadProjects()
    {
        // TODO: Register project combo box store to model then call reload
        // method from model.
        this.toolBar.reloadProjectCombo();
    }

    /**
     * Bind date fields with planning.
     * 
     * @param planning
     *            The planning to bind.
     */
    public void bindDateFields(PlanningModelData planning)
    {
        this.toolBar.updateDateFields(planning);
    }

    /** Return start date field value. */
    public Date getStartDate()
    {
        // TODO: Make binding work properly to not retrieved end date from view. Should be retrieved from model instead.
        return this.toolBar.getStartDate();
    }

    /** Return end date field value. */
    public Date getEndDate()
    {
        // TODO: Make binding work properly to not retrieved end date from view. Should be retrieved from model instead.
        return this.toolBar.getEndDate();
    }

    /** Hide saving indicator. */
    public void hideSaving()
    {
        if (this.toolBar != null)
            this.toolBar.hideSaving();
    }

    /** Show saving indicator. */
    public void showSaving()
    {
        this.toolBar.showSaving();
    }

    /** Disable tool bar fields and buttons (project combo, planning combo, date fields...). */
    public void disableToolBarFields()
    {
        this.toolBar.disable();
    }

    /** Enable tool bar fields and buttons (project combo, planning combo, date fields...). */
    public void enableToolBarFields()
    {
        if (this.toolBar != null)
        {
            this.toolBar.enable();
        }
    }

    /** Set planning tool bar on top of the widgets. */
    private void createToolbar()
    {
        this.toolBar = new PlanningToolBar();
        this.mainPanel.setTopComponent(toolBar);
    }

    /**
     * Create task grid panel.
     */
    private void createTaskGridPanel()
    {
        this.taskGrid = new UnassignedTasksGrid(this.model.getUnassignedTaskStore(),
                this.model.getUnassignedTaskTypeStore());

        BorderLayoutData eastData = mainPanel.addEast(taskGrid, 700);
        eastData.setCollapsible(true);
        eastData.setMaxSize(800);
        eastData.setMinSize(100);
    }

    /**
     * Create planning panel.
     */
    private void createPlanningWidgetPanel()
    {
        this.workObjectUserPlanningView = new PlanningPanelView();
        this.workObjectUserPlanningView.init();
        this.mainPanel.addCenter(workObjectUserPlanningView.getContentPanel(), BorderedPanel.NO_MARGIN,
                BorderedPanel.MARGIN, BorderedPanel.NO_MARGIN, BorderedPanel.NO_MARGIN);
    }

    /**
     * Register controllers of widgets.
     */
    private void addControllers()
    {
        this.controller.addChild(this.workObjectUserPlanningView.getController());
        // this.workObjectUserPlanningView.updateControllers();
    }

    /**
     * Display loading panel with text corresponding to planning loading.
     */
    public void showPlanningLoading()
    {
        this.loadingPanel.setText("Loading planning", "Loading task group data...");
        this.loadingPanel.show();
    }

    /**
     * Display loading panel with text corresponding to task group synchronization.
     */
    public void showtaskGroupSynchronizationLoading()
    {
        this.loadingPanel.setText("Creating task groups", "Synchronizing task groups with task types...");
        this.loadingPanel.show();
    }

    /**
     * Display loading panel with text corresponding to constituent categories loading.
     */
    public void showRootCategoriesLoading()
    {
        this.loadingPanel.setText("Loading categories", "Loading categories data...");
        this.loadingPanel.show();
    }

    /**
     * Display loading panel with text corresponding to shot sequences loading.
     */
    public void showRootSequencesLoading()
    {
        this.loadingPanel.setText("Loading sequences", "Loading sequences data...");
        this.loadingPanel.show();
    }

    /**
     * Hide loading panel.
     */
    public void hideLoading()
    {
        this.loadingPanel.hide();
    }

    public void openOdsExport(String path)
    {
        Window.open(path, CONSTANTS.CSVExport(), "");
    }

    /**
     * Clear displayed value inside task type combo box used to filter unassigned task grid.
     */
    public void clearTaskTypeFilter()
    {
        this.taskGrid.clearTaskTypeCombo();
    }

    /**
     * @return List of task types selected in task type multi combo box.
     */
    public List<TaskTypeModelData> getSelectedTaskTypes()
    {
        return this.taskGrid.getSelectTaskTypes();
    }

    /**
     * Push tool bar auto save toggle button.
     */
    public void toggleAutoSaveButton()
    {
        this.toolBar.toggleAutoSaveButton();
    }

    public void enableUnassignedTasksGrid()
    {
        this.taskGrid.enableGrid();
    }

    public void toggleZoomButton(String zoomMode)
    {
        this.toolBar.toggleZoomButton(zoomMode);
    }

    public void toggleColorButton(String colorMode)
    {
        this.toolBar.toggleColorButton(colorMode);
    }

    public void setDateRangePickerStartDate(Date date)
    {
        this.toolBar.setDateRangePickerStartDate(date);
    }

    public void setDateRangePickerEndDate(Date date)
    {
        this.toolBar.setDateRangePickerEndDate(date);
    }
}
