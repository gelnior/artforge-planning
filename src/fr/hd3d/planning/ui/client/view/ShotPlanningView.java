package fr.hd3d.planning.ui.client.view;

import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;
import fr.hd3d.planning.ui.client.controller.ShotPlanningController;
import fr.hd3d.planning.ui.client.model.ShotPlanningModel;


public class ShotPlanningView extends WorkObjectPlanningView
{
    protected ShotPlanningModel model = new ShotPlanningModel();
    protected ShotPlanningController controller = new ShotPlanningController(this, model);

    public ShotPlanningView(PlanningPanelView parentPanel)
    {
        super(parentPanel);
        this.setId("shot-planning-lines");

        super.controller = this.controller;
    }

    public ShotPlanningController getController()
    {
        return this.controller;
    }

    /**
     * Add task panel, corresponding to task, to <i>taskGroup</i>. Task bar is set inside shot extra line corresponding
     * to the shot with which task is linked. An extra line will be added if does not exist for this shot.
     * 
     * @param taskGroup
     *            The task group in which task will be added.
     * @param task
     *            The task to add.
     */
    @Override
    public TaskBar addTaskPanel(TaskGroupModelData taskGroup, TaskModelData task)
    {
        String label = task.getTaskTypeName();

        if (!Util.isEmptyString(task.getWorkerName()))
        {
            String[] workerNames = task.getWorkerName().split(" - ");
            if (workerNames.length == 2)
                label += " - " + workerNames[1];
        }
        return this.addTask(taskGroup, task, task.getWorkObjectId(), task.getWorkObjectName(), label);
    }

    public boolean isRegisteredTask(TaskModelData task)
    {
        return this.taskBars.get(task.getId().toString()) != null;
    }

    public void addTaskPanel(final TaskModelData task)
    {
        ExtraLine line = null;
        for (ExtraLine planningLine : this.extraLines.values())
        {
            Long objectLineId = planningLine.getPersonId();
            Long workObjectId = task.getWorkObjectId();
            if (objectLineId.longValue() == workObjectId.longValue())
            {
                line = planningLine;
                continue;
            }
        }
        this.extraLines.get(task.getWorkObjectId());

        if (line != null)
        {
            TaskBar bar = this.addTaskPanel(line.getTaskGroup(), task);
            bar.setDirty();
        }
        else
        {
            // final ExtraLineModelData lineModelData = new ExtraLineModelData();
            // lineModelData.setId(Long.valueOf(IdGenerator.getNewId()));
            // lineModelData.setName(task.getWorkObjectName());
            // lineModelData.setPersonID(task.getWorkObjectId());
            //
            // final ShotModelData shot = new ShotModelData();
            // shot.setId(task.getWorkObjectId());
            // shot.refresh(new GetModelDataCallback(task) {
            // @Override
            // protected void onRecordReturned(List<Hd3dModelData> records)
            // {
            // Hd3dModelData newRecord = records.get(0);
            // this.record.updateFromModelData(newRecord);
            //
            // TaskGroupItem item = taskGroupItems.get(shot.getSequence().toString());
            // TaskGroupModelData taskGroup = item.getTaskGroup();
            // lineModelData.setTaskGroup(taskGroup);
            //
            // addExtraLine(taskGroup, lineModelData);
            // addTaskPanel(taskGroup, task);
            // }
            // });
        }

    }
}
