package fr.hd3d.planning.ui.client.view;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.util.PlanningSettings;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.ColumnBar;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;
import fr.hd3d.planning.ui.client.Planning;
import fr.hd3d.planning.ui.client.controller.PlanningPanelController;
import fr.hd3d.planning.ui.client.model.PlanningMainModel;
import fr.hd3d.planning.ui.client.model.PlanningPanelModel;
import fr.hd3d.planning.ui.client.widget.WorkObjectUserTree;


/**
 * Planning panel displays planning widget.
 * 
 * @author HD3D
 */
public class PlanningPanelView extends BasePlanningWidget
{

    /** Model handling widget data. */
    private final PlanningPanelModel model = new PlanningPanelModel();
    /** Controller handling planning widget events. */
    private final PlanningPanelController controller = new PlanningPanelController(this, this.model);

    /** Constituent planing panel displays planning where categories are task group and constituents are extra lines. */
    private final ConstituentPlanningView constituentPlanningPanel = new ConstituentPlanningView(this);
    /** Shot planing panel displays planning where sequences are task group and sequences are extra lines. */
    private final ShotPlanningView shotPlanningPanel = new ShotPlanningView(this);
    /** Task type planning panel displays planning where task types are task group and workers are extra lines. */
    private final TaskTypePlanningView taskTypePlanningPanel = new TaskTypePlanningView(this,
            ConstituentModelData.SIMPLE_CLASS_NAME);
    /** Task type planning panel displays planning where task types are task group and workers are extra lines. */
    private final TaskTypePlanningView taskTypeShotPlanningPanel = new TaskTypePlanningView(this,
            ShotModelData.SIMPLE_CLASS_NAME);

    /** Fast map needed to easily select right planning panel to add elements on it. */
    private final FastMap<WorkObjectPlanningView> panelMap = new FastMap<WorkObjectPlanningView>();
    /** Give last selected panel, useful to hide current panel or remove it to add another after. */
    private WorkObjectPlanningView lastSelectedPlanningPanel;
    /** Column displaying current day. */
    private ColumnBar dayColumn;

    /**
     * Constructor.
     */
    public PlanningPanelView()
    {
        super(new WorkObjectUserTree(), null, true);

        this.boundaryPanel = this.taskTypePlanningPanel;
        this.setWithOtherElement(true);

        this.panelMap.put(EPlanningDisplayMode.TASK_TYPE.toString(), this.taskTypePlanningPanel);
        this.taskTypePlanningPanel.setCustomTree(this.getPlanningTree().getCustomTree(EPlanningDisplayMode.TASK_TYPE));
        this.lastSelectedPlanningPanel = this.taskTypePlanningPanel;

        this.panelMap.put(EPlanningDisplayMode.TASK_TYPE_SHOT.toString(), this.taskTypeShotPlanningPanel);
        this.taskTypeShotPlanningPanel.setCustomTree(this.getPlanningTree().getCustomTree(
                EPlanningDisplayMode.TASK_TYPE_SHOT));
        this.taskTypeShotPlanningPanel.idle();

        this.panelMap.put(EPlanningDisplayMode.CONSTITUENT.toString(), this.constituentPlanningPanel);
        this.constituentPlanningPanel.setCustomTree(this.getPlanningTree().getCustomTree(
                EPlanningDisplayMode.CONSTITUENT));
        this.constituentPlanningPanel.idle();

        this.panelMap.put(EPlanningDisplayMode.SHOT.toString(), this.shotPlanningPanel);
        this.shotPlanningPanel.setCustomTree(this.getPlanningTree().getCustomTree(EPlanningDisplayMode.SHOT));
        this.shotPlanningPanel.idle();

        this.controller.addChild(this.taskTypePlanningPanel.getController());
        this.controller.addChild(this.taskTypeShotPlanningPanel.getController());
        this.controller.addChild(this.constituentPlanningPanel.getController());
        this.controller.addChild(this.shotPlanningPanel.getController());
    }

    /**
     * Initialize planning widget components.
     */
    @Override
    public void init()
    {
        super.init();
        this.getPlanningTree().init(model);
    }

    /**
     * @return The planning tree.
     */
    @Override
    public WorkObjectUserTree getPlanningTree()
    {
        return (WorkObjectUserTree) super.getPlanningTree();
    }

    /** View controller (event handler). */
    public PlanningPanelController getController()
    {
        return this.controller;
    }

    /** @return Panel containing all other widgets. */
    public BorderedPanel getContentPanel()
    {
        return this.mainPanel;
    }

    /**
     * Show planning panel (display it if it was hidden, useful for loading data then show them). Task type planning is
     * displayed by default.
     */
    public void showPlanningPanel()
    {
        this.boundaryPanel.show();
    }

    /**
     * Display planning corresponding to planning display mode. It means it displays constituent planning its mode is
     * set to constituent, shot planning if mode is set to shot...
     * 
     * @param mode
     *            The display mode needed to chose which planning will be displayed.
     */
    public void displayPlanning(EPlanningDisplayMode mode)
    {
        ((WorkObjectUserTree) this.planningTree).setDisplayMode(mode);

        this.lastSelectedPlanningPanel.idle();
        this.planningPanel.remove(lastSelectedPlanningPanel);
        this.planningPanel.setWidget(panelMap.get(mode.toString()));
        this.lastSelectedPlanningPanel = panelMap.get(mode.toString());
        this.panelMap.get(mode.toString()).unIdle();
    }

    /**
     * Build a new time line from settings given in parameter (start date and end date). Set column bar for current day.
     * 
     * @param settings
     *            Settings needed to build planning panel.
     */
    public void buildTimeLine(PlanningSettings settings)
    {
        this.getTimeLinePanel().clear();

        TimeLine timeline = new TimeLine(settings);
        this.setTimeLine(timeline);
        this.getTimeLinePanel().add(timeline);
        String width = ((getTimeline().getWrappers().length * BasePlanningWidget.getCellWidth()) - 1) + "px";
        for (String key : panelMap.keySet())
        {
            panelMap.get(key).setWidth(width);
            panelMap.get(key).setHeight("");
            if (getTimeline().getWrappers().length > 0 && getTimeline().isInPeriod(new Date()))
            {
                this.dayColumn = panelMap.get(key).updateDayColumn();
            }
        }
    }

    /** Remove every planning items. */
    public void clearPlanning()
    {
        for (WorkObjectPlanningView planning : panelMap.values())
        {
            planning.clear();
        }

        this.boundaryPanel.hide();
        timeLine.clear();
    }

    /** Update task color depending on task color display mode set by user. */
    public void updateTaskColors()
    {
        for (WorkObjectPlanningView planningView : panelMap.values())
        {
            planningView.updateTaskColors();
        }
    }

    /**
     * Toggle task type display button and un-toggle other display buttons.
     */
    public void resetDisplayButtons()
    {
        this.getPlanningTree().resetDisplayButtons();
    }

    /**
     * Add a milestone to time line and add a corresponding column bar in planning. Milestone position is determined by
     * its date.
     * 
     * @param milestone
     *            The milestone to add in time line.
     */
    public void addMilestone(MilestoneModelData milestone)
    {
        timeLine.addMilestone(milestone);

        if (getTimeline().getWrappers().length > 0 && getTimeline().isInPeriod(milestone.getDate()))
            for (WorkObjectPlanningView planning : panelMap.values())
            {
                planning.addMilestone(milestone);
                planning.layout();
            }
    }

    /**
     * Display a message box that ask confirmation for milestone deletion. If user confirms, DELETE_MILESTONE_CONFIRMED
     * event is dispatched to controllers.
     * 
     * @param milestone
     *            The milestone concerned by deletion.
     */
    public void displayDeleteMilestoneConfirmation(MilestoneModelData milestone)
    {
        AppEvent event = new AppEvent(PlanningCommonEvents.DELETE_MILESTONE_CONFIRMED, milestone);
        ConfirmationDisplayer.display(Planning.COMMON_CONSTANTS.DeleteMileStone(), Planning.MESSAGES.confirmDelete(),
                event);
    }

    /**
     * Remove <i>milestone</i> from time line and corresponding column bar from planning.
     * 
     * @param milestone
     *            The milestone to remove from time line.
     */
    public void removeMilestone(MilestoneModelData milestone)
    {
        timeLine.removeMilestone(milestone);

        for (WorkObjectPlanningView planning : panelMap.values())
        {
            planning.removeMilestone(milestone);
            planning.layout();
        }
    }

    /**
     * Remove task modification indicator on all task bars representing tasks listed in <i>tasks</i>.
     * 
     * @param tasks
     *            The saved task list.
     */
    public void removeTaskModificationIndicator(List<TaskModelData> tasks)
    {
        for (WorkObjectPlanningView planning : panelMap.values())
        {
            planning.removeTaskModificationIndicator(tasks);
        }
    }

    /**
     * Remove task modification indicator on all task group bars representing task groups listed in <i>tasks</i>.
     * 
     * @param taskGroups
     *            The saved task groups.
     */
    public void removeTaskGroupModificationIndicator(List<TaskGroupModelData> taskGroups)
    {
        for (WorkObjectPlanningView planning : panelMap.values())
        {
            planning.removeTaskGroupModificationIndicator(taskGroups);
        }
    }

    /**
     * Clear tree for each planning displayed.
     */
    public void clearTrees()
    {
        for (WorkObjectPlanningView planning : panelMap.values())
        {
            planning.clearTree();
        }
    }

    /** Move planning scroll cursor to current day. */
    public void scrollToCurrentDay()
    {
        if (dayColumn != null)
        {
            int scrollLeft = dayColumn.getLeft();

            if (timeLinePanel != null)
            {
                timeLinePanel.setHorizontalScrollPosition(scrollLeft);
            }
            planningPanel.setHorizontalScrollPosition(scrollLeft);
        }
    }

    public TaskGroupItem getSelectedTaskGroupItem()
    {
        return this.panelMap.get(PlanningMainModel.displayMode.toString()).getTreeSelectedItem();
    }
}
