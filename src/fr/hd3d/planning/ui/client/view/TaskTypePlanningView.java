package fr.hd3d.planning.ui.client.view;

import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.user.dialog.AddUserDialog;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskGroupBar;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;
import fr.hd3d.common.ui.client.widget.planning.widget.line.TaskGroupLine;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;
import fr.hd3d.planning.ui.client.controller.TaskTypePlanningController;
import fr.hd3d.planning.ui.client.dnd.insert.ExtraLineDropTarget;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;
import fr.hd3d.planning.ui.client.events.WorkObjectPlanningEvents;
import fr.hd3d.planning.ui.client.model.TaskTypePlanningModel;


public class TaskTypePlanningView extends WorkObjectPlanningView
{
    private final TaskTypePlanningModel model = new TaskTypePlanningModel();
    private final TaskTypePlanningController controller = new TaskTypePlanningController(this, model);

    public TaskTypePlanningView(PlanningPanelView parentPanel, String entityName)
    {
        super(parentPanel);

        this.model.setEntityName(entityName);
        super.controller = this.controller;
    }

    /**
     * Add <i>nbDays</i> to the X coordinate of all group tasks.
     * 
     * @param taskGroup
     *            The task group of which task are moved.
     * @param nbDays
     *            The number of days to add to each task start date and end date (can be negative).
     */
    public void moveAllGroupTasks(TaskGroupModelData taskGroup, int nbDays)
    {
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        taskGroupLine.moveAllTasks(nbDays);
    }

    /**
     * Warning : Work only for task group from task type planning. <br>
     * Resize task group panel corresponding to task group. New size depends on task group start and end dates.
     * 
     * @param taskGroup
     *            The task group to resize.
     */
    public void resizeTaskGroup(TaskGroupModelData taskGroup)
    {
        TaskGroupLine taskGroupLine = taskGroupLines.get(taskGroup.getId().toString());
        TaskGroupBar taskGroupBar = taskGroupLine.getGroupBar();
        taskGroupBar.getTimeObject().setStartDate(taskGroup.getStartDate());
        taskGroupBar.getTimeObject().setEndDate(taskGroup.getEndDate());
        taskGroupBar.resetSize();
        taskGroupBar.resetPosition();
        taskGroupBar.addStyleName(CSSUtils.MODIFIED_STYLE);
        EventDispatcher.forwardEvent(PlanningAppEvents.TASK_GROUP_MOVED, taskGroupBar.getTimeObject());
    }

    @Override
    public ExtraLine addExtraLine(TaskGroupItem taskGroupItem, ExtraLineModelData extraLineModelData)
    {
        ExtraLine extraLine = super.addExtraLine(taskGroupItem, extraLineModelData);
        if (taskGroupItem.getTaskGroup().getType() == EPlanningDisplayMode.TASK_TYPE)
        {
            new ExtraLineDropTarget(extraLine, this);
        }
        return extraLine;
    }

    public Controller getController()
    {
        return controller;
    }

    /**
     * Display add worker dialog for task group.
     * 
     * @param taskGroup
     *            Task group inside which Worker will be added.
     */
    public void showAddWorkerDialog(TaskGroupModelData taskGroup)
    {
        AddUserDialog.getInstance(WorkObjectPlanningEvents.TASK_GROUP_ADDING_USERS_REQUESTED).show();
    }

    @Override
    public TaskBar addTaskPanel(TaskGroupModelData taskGroup, TaskModelData task)
    {
        return this.addTask(taskGroup, task, task.getWorkerID(), task.getWorkerName(), task.getWorkObjectName());
    }

}
