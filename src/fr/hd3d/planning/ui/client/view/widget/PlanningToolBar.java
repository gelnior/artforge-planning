package fr.hd3d.planning.ui.client.view.widget;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.widget.BaseToolBar;
import fr.hd3d.common.ui.client.widget.EasyDateRangePicker;
import fr.hd3d.common.ui.client.widget.ProjectCombobox;
import fr.hd3d.common.ui.client.widget.ToggleButtonList;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;


/**
 * Planning main tool bar, contains main fields and main buttons like project combo box, , start date field, end date
 * field....
 * 
 * @author HD3D
 */
public class PlanningToolBar extends BaseToolBar
{
    /** The combo box needed to select current project. */
    private final ProjectCombobox projectComboBox = new ProjectCombobox();

    /** Button that forward the planning save clicked event when pressed. */
    private final ToolBarButton saveButton = new ToolBarButton(Hd3dImages.getSaveIcon(), "Save planning",
            PlanningAppEvents.PLANNING_SAVE_CLICKED);

    /** Button that forward the planning export ODS clicked event when pressed. */
    private final ToolBarButton exportButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(), "Export ODS",
            PlanningAppEvents.EXPORT_ODS_PLANNING_CLICKED);

    /** Button that forwards the "set small zoom" clicked event when pressed. */
    private final ToolBarToggleButton smallZoomButton = new ToolBarToggleButton(Hd3dImages.getPlanningSmallZoomIcon(),
            "Set zoom mode to small.", PlanningAppEvents.YEAR_DISPLAY_CLICKED);
    /** Button that forwards the "set middle zoom" clicked event when pressed. */
    private final ToolBarToggleButton middleZoomButton = new ToolBarToggleButton(
            Hd3dImages.getPlanningNormalZoomIcon(), "Set zoom mode to middle.", PlanningAppEvents.MONTH_DISPLAY_CLICKED);
    /** Button that forwards the "set big zoom" clicked event when pressed. */
    private final ToolBarToggleButton bigZoomButton = new ToolBarToggleButton(Hd3dImages.getPlanningBigZoomIcon(),
            "Set zoom mode to big.", PlanningAppEvents.WEEK_DISPLAY_CLICKED);

    /** Button that forwards the "colors task with task type color" clicked event when pressed. */
    private final ToolBarToggleButton taskTypeColorButton = new ToolBarToggleButton(Hd3dImages.getTaskTypeColorIcon(),
            "Set the task type color as background color on all task bars.",
            PlanningAppEvents.COLOR_TASK_TASK_TYPE_CLICKED);
    /** Button that forwards the "colors task with task status color clicked" event when pressed. */
    private final ToolBarToggleButton statusColorButton = new ToolBarToggleButton(Hd3dImages.getTaskStatusColorIcon(),
            "Set the task status color as background color on all task bars.",
            PlanningAppEvents.COLOR_TASK_STATUS_CLICKED);
    /** Button that forwards the "colors task with task late state color clicked" event when pressed. */
    private final ToolBarToggleButton startColorButton = new ToolBarToggleButton(
            Hd3dImages.getTaskLateColorIcon(),
            "Set the <i>is late</i> color as background color on all task bars. <i>is late</i> color is red if task status is still stand by and the start date is passed. <i>is late</i> color is green in other case.",
            PlanningAppEvents.COLOR_TASK_START_CLICKED);

    /** Button that forwards the refresh planning clicked event. */
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh planning",
            PlanningAppEvents.REFRESH_PLANNING_CLICKED);
    /** Button that forwards the refresh planning clicked event. */
    private final ToolBarToggleButton autoSaveButton = new ToolBarToggleButton(Hd3dImages.getAutoSaveIcon(),
            "Enable autosaving mode.", PlanningAppEvents.PLANNING_AUTO_SAVE_CLICKED);

    /** Date picker used to set planning dates. */
    private final EasyDateRangePicker dateRangePicker = new EasyDateRangePicker();

    private final ToggleButtonList zoomButtonList = new ToggleButtonList();
    private final ToggleButtonList colorButtonList = new ToggleButtonList();

    /**
     * Default constructor : set fields and listeners.
     */
    public PlanningToolBar()
    {
        super();

        this.setFields();
        this.setListeners();
    }

    /** Reload project combo box list. */
    public void reloadProjectCombo()
    {
        this.projectComboBox.setData();
    }

    /**
     * Update fields with current planning data by binding them to it.
     * 
     * @param planning
     *            The planning to bind on date fields.
     */
    public void updateDateFields(PlanningModelData planning)
    {
        this.dateRangePicker.setStartDate(new DateWrapper(planning.getStartDate()));
        this.dateRangePicker.setEndDate(new DateWrapper(planning.getEndDate()));
    }

    /** @return Start date field value. */
    public Date getStartDate()
    {
        return this.dateRangePicker.getStartDate().asDate();
    }

    /** @return End date field value. */
    public Date getEndDate()
    {
        return this.dateRangePicker.getEndDate().asDate();
    }

    /** Instead of enabling tool bar it enables its components (fields and buttons). */
    @Override
    public void enable()
    {
        this.projectComboBox.enable();
        this.dateRangePicker.enable();
        this.autoSaveButton.enable();
        this.saveButton.enable();
        this.refreshButton.enable();
    }

    /** Instead of disabling tool bar it disables its components (fields and buttons). */
    @Override
    public void disable()
    {
        this.projectComboBox.disable();
        this.dateRangePicker.disable();
        this.autoSaveButton.disable();
        this.saveButton.disable();
        this.refreshButton.disable();
    }

    /**
     * Push auto save toggle button.
     */
    public void toggleAutoSaveButton()
    {
        this.autoSaveButton.toggle();
    }

    /** Add fields to tool bar. */
    private void setFields()
    {
        this.add(projectComboBox);

        this.add(new SeparatorToolItem());
        this.add(dateRangePicker);

        this.add(new SeparatorToolItem());
        this.zoomButtonList.addButton(this.smallZoomButton);
        this.zoomButtonList.addButton(this.middleZoomButton);
        this.zoomButtonList.addButton(this.bigZoomButton);
        this.zoomButtonList.addListToToolBar(this);

        this.add(new SeparatorToolItem());
        this.colorButtonList.addButton(this.taskTypeColorButton);
        this.colorButtonList.addButton(this.statusColorButton);
        this.colorButtonList.addButton(this.startColorButton);
        this.colorButtonList.addListToToolBar(this);

        // this.add(new SeparatorToolItem());
        // this.add(exportButton);
        this.add(new SeparatorToolItem());
        this.add(refreshButton);
        this.add(autoSaveButton);
        this.add(saveButton);
        this.add(status);
    }

    /** Set changed listener on fields. */
    private void setListeners()
    {
        this.projectComboBox.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                CommonEvents.PROJECT_CHANGED));
        this.dateRangePicker.setEvent(PlanningAppEvents.PLANNING_DATE_CHANGED);
    }

    public void toggleZoomButton(String zoomMode)
    {
        if (EZoomMode.BIG.toString().equals(zoomMode))
            this.zoomButtonList.toggle(bigZoomButton);
        else if (EZoomMode.NORMAL.toString().equals(zoomMode))
            this.zoomButtonList.toggle(middleZoomButton);
        else if (EZoomMode.SMALL.toString().equals(zoomMode))
            this.zoomButtonList.toggle(smallZoomButton);
        else
            this.zoomButtonList.toggle(middleZoomButton);
    }

    public void toggleColorButton(String colorMode)
    {
        if (ETaskDisplayMode.TASK_TYPE.toString().equals(colorMode))
            this.colorButtonList.toggle(taskTypeColorButton);
        else if (ETaskDisplayMode.STATUS.toString().equals(colorMode))
            this.colorButtonList.toggle(statusColorButton);
        else if (ETaskDisplayMode.ON_TIME.toString().equals(colorMode))
            this.colorButtonList.toggle(startColorButton);
        else
            this.colorButtonList.toggle(statusColorButton);
    }

    public void setDateRangePickerStartDate(Date date)
    {
        this.dateRangePicker.setStartDate(new DateWrapper(date));
    }

    public void setDateRangePickerEndDate(Date date)
    {
        this.dateRangePicker.setEndDate(new DateWrapper(date));
    }
}
