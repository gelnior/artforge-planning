package fr.hd3d.planning.ui.client.view.widget;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.common.ui.client.widget.field.BaseDateField;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.planning.ui.client.config.PlanningConfig;
import fr.hd3d.planning.ui.client.events.PlanningAppEvents;


/**
 * Planning creation dialog is a simple form with a start date and end date fields. It allows user to create a master
 * planning.
 * 
 * @author HD3D
 */
public class PlanningCreationDialog extends FormDialog
{
    /** Planning binded to form for easy creation on OK click. */
    private final PlanningModelData planning = new PlanningModelData();

    /** Planning start date field */
    private final BaseDateField startDateField = new BaseDateField();
    /** Planning end date field */
    private final BaseDateField endDateField = new BaseDateField();

    /**
     * Constructor, set field, header and styles.
     */
    public PlanningCreationDialog()
    {
        super(PlanningAppEvents.PLANNING_CHANGED, "Set date for new planning");

        this.setHideOnButtonClick(false);
        this.setClosable(false);
        this.setWidth(330);
        this.setModal(true);
        this.cancelButton.hide();
        this.planning.setMaster(Boolean.TRUE);

        this.setFields();
        this.cancelButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                hide();
            }
        });
    }

    /**
     * When show, binded planning is automatically set up depending on current project.
     */
    @Override
    public void show()
    {
        this.planning.setId(null);
        this.planning.setStartDate(null);
        this.planning.setEndDate(null);
        this.planning.setProject(MainModel.currentProject);
        this.planning.setName(MainModel.currentProject.getName());

        this.okButton.disable();
        this.hideSaving();

        super.show();
    }

    /**
     * When OK is clicked, the binded planning is saved and the PLANNING_CHANGE event is forwarded to controllers.
     */
    @Override
    protected void onOkClicked()
    {
        if (!isValid())
        {
            MessageBox.alert("Error", "Dates are not valid. Please correct it.", null);
        }
        else
        {
            this.showSaving();
            // TODO : do this action inside controller.
            this.planning.save(new PostModelDataCallback(planning) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);

                    AppEvent mvcEvent = new AppEvent(okEvent, planning);
                    mvcEvent.setData(PlanningConfig.PLANNING_CREATION_EVENT_VAR_NAME, Boolean.TRUE);
                    onSaveFinished();

                    EventDispatcher.forwardEvent(mvcEvent);
                }
            });
        }
    }

    /**
     * When save is finished, the dialog and saving indicator are hidden.
     */
    private void onSaveFinished()
    {
        this.hide();
        this.hideSaving();
    }

    /**
     * When a field changed, the OK button is enabled only if form is valid (start date < end date and no null dates).
     */
    @Override
    protected void onFieldChanged()
    {
        if (isValid())
        {
            this.okButton.enable();
        }
        else
        {
            this.okButton.disable();
        }
    }

    /**
     * @return True if start date < end date and if start date and end date are not null.
     */
    private boolean isValid()
    {
        if (this.startDateField.getValue() == null || this.endDateField.getValue() == null)
            return false;

        if (this.startDateField.getValue().after(this.endDateField.getValue()))
            return false;

        return true;
    }

    /**
     * Add fields to form and bind it to planning model data.
     */
    private void setFields()
    {
        FieldBinding startDateBinder = new FieldBinding(startDateField, PlanningModelData.START_DATE_FIELD);
        startDateBinder.bind(planning);
        FieldBinding endDateBinder = new FieldBinding(endDateField, PlanningModelData.END_DATE_FIELD);
        endDateBinder.bind(planning);

        this.startDateField.setFieldLabel("Start date:");
        this.endDateField.setFieldLabel("End date:");

        this.addField(startDateField);
        this.addField(endDateField);
    }

}
