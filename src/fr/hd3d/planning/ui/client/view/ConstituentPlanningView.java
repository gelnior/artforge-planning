package fr.hd3d.planning.ui.client.view;

import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.planning.ui.client.controller.ConstituentPlanningController;
import fr.hd3d.planning.ui.client.model.ConstituentPlanningModel;


public class ConstituentPlanningView extends WorkObjectPlanningView
{
    protected ConstituentPlanningModel model = new ConstituentPlanningModel();
    protected ConstituentPlanningController controller = new ConstituentPlanningController(this, model);

    public ConstituentPlanningView(PlanningPanelView parentPanel)
    {
        super(parentPanel);

        super.controller = this.controller;

        this.setId("constituent-planning-lines");
    }

    public ConstituentPlanningController getController()
    {
        return this.controller;
    }

    /**
     * Add task panel, corresponding to task, to <i>taskGroup</i>. Task bar is set inside constituent extra line
     * corresponding to the constituent with which task is linked. An extra line will be added if does not exist for
     * this constituent.
     * 
     * @param taskGroup
     *            The task group in which task will be added.
     * @param task
     *            The task to add.
     */
    @Override
    public TaskBar addTaskPanel(TaskGroupModelData taskGroup, TaskModelData task)
    {
        String label = task.getTaskTypeName();
        task.getWorkerName();
        if (task.getWorkerName() != null)
        {
            String[] workerNames = task.getWorkerName().split(" - ");
            if (workerNames.length == 2)
                label += " - " + workerNames[1];
        }
        return this.addTask(taskGroup, task, task.getWorkObjectId(), task.getWorkObjectName(), label);
    }
}
