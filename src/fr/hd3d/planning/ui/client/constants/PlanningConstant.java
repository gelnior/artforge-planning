package fr.hd3d.planning.ui.client.constants;



public class PlanningConstant
{
    public static final int MONTH_SCALE_VALUE = 15;
    public static final int WEEK_SCALE_VALUE = 45;
    public static final int DAY_SCALE_VALUE = 75;
    public static final String TIMESTAMP_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss'.0'";
    public static final String PLANNING_APPLICATION_NAME = "Planning";
    public static final String PLANNING_VIEW_CONFIG = "planning_view_config";
    public static final String USER_REPERE_SELECTED_CSS = "userRepere-selected";
    public static final String USER_REPERE_CSS = "userRepere";
    public static final String USER_MODE = "user_mode";
    public static final String WORKOBJECT_MODE = "workobject_mode";
    // public static final String UNASSIGNED_TASK_TEXT = "Unassigned";
    // public static final String PLANNED_UNASSIGNED_TASK_TEXT = "Planned Unassigned";
    // service uri
    public static final String CREATE_TASK_GROUP_SCRIPT_NAME = "createTaskGroupByProjectTaskType";

}
