package fr.hd3d.planning.ui.client.constants;

import com.google.gwt.i18n.client.Messages;


public interface PlanningLocaleMessages extends Messages
{

    @DefaultMessage("This task cannot be assign here because some absence have been found.")
    String absenceFound();

    @DefaultMessage("Absence found")
    String absenceFoundTitle();

    @DefaultMessage("Build planning time line")
    String buildPlanningTimeLine();

    @DefaultMessage("Build categories")
    String buildWorkObjectCategory();

    @DefaultMessage("Build sequences")
    String buildWorkObjectSequence();

    @DefaultMessage("Selected elements are the same. Please choose another one")
    String changeWorkerInfos();

    @DefaultMessage("Change worker infos")
    String changeWorkerInfosTitle();

    @DefaultMessage("Do you really want to delete ?")
    String confirmDelete();

    @DefaultMessage("Create auto task group")
    String createAutoTaskGroup();

    @DefaultMessage("Please enter name")
    String enterUnassignedName();

    @DefaultMessage("Export finished with success")
    String exportInfos();

    @DefaultMessage("Export infos")
    String exportInfosTitle();

    @DefaultMessage("End date must be after start date")
    String invalideEndDate();

    @DefaultMessage("The date must be between {0} and {1} (the current planning bounds)")
    String invalidMileStoneDate(String startDate, String endDate);

    @DefaultMessage("Planned duration must be less or equal to (end - start) duration")
    String invalidePlannedDuration();

    @DefaultMessage("Start date must be before end date")
    String invalideStartDate();

    @DefaultMessage("Load categories")
    String loadCategories();

    @DefaultMessage("Load category infos")
    String loadCategoryInfos();

    @DefaultMessage("Load constituent tasks")
    String loadConstituentTasks();

    @DefaultMessage("Load extra lines")
    String loadExtraLines();

    @DefaultMessage("Load extra lines tasks")
    String loadExtraLinesTask();

    @DefaultMessage("Load milestones")
    String loadMileStone();

    @DefaultMessage("Load planning")
    String loadPlanning();

    @DefaultMessage("Load planning planned tasks")
    String loadPlanningPlannedTasks();

    @DefaultMessage("Load project task group")
    String loadProjectTaskGroup();

    @DefaultMessage("Load sequences")
    String loadSequence();

    @DefaultMessage("Load sequence infos")
    String loadSequenceInfos();

    @DefaultMessage("Load shot tasks")
    String loadShotTasks();

    @DefaultMessage("Load task changes")
    String loadTaskChanges();

    @DefaultMessage("Load task groups")
    String loadTaskGroups();

    @DefaultMessage("Load task in category")
    String loadTaskInCategory();

    @DefaultMessage("Load task in group")
    String loadTaskInGroup();

    @DefaultMessage("Load task in sequence")
    String loadTaskInSequence();

    @DefaultMessage("Load time line bounds")
    String loadTimeLineBounds();

    @DefaultMessage("New value")
    String newValue();

    @DefaultMessage("Please select one task group")
    String noGroupSelected();

    @DefaultMessage("No Group selected")
    String noGroupSelectedTitle();

    @DefaultMessage("No task group found<br/>Click on&nbsp;")
    String noTaskGroupFound();

    @DefaultMessage("&nbsp;to create one and display tasks.")
    String noTaskGroupFoundEnd();

    @DefaultMessage("This user has another task(s) in other project(s).<br/>Do you really want to assign task ?")
    String otherTaskFound();

    @DefaultMessage("Other tasks found")
    String otherTaskFoundTitle();

    @DefaultMessage("Paint task group")
    String paintTaskGroupUI();

    @DefaultMessage("Would you like to save your changes?")
    String saveChangesConfirm();

    @DefaultMessage("Error step controller is running !")
    String saveError();

    @DefaultMessage("Save error")
    String saveErrorTitle();

    @DefaultMessage("Save extra lines")
    String saveExtraLines();

    @DefaultMessage("The planning have been correctly saved")
    String savePlanningResult();

    @DefaultMessage("Tasks have been correctly saved")
    String savePlanningTasksResult();

    @DefaultMessage("Save Result")
    String saveResultTitle();

    @DefaultMessage("Save task changes")
    String saveTaskChanges();

    @DefaultMessage("Saving task groups")
    String saveTaskGroups();

    @DefaultMessage("Saving tasks")
    String saveTasks();

    @DefaultMessage("Saving...")
    String saving();

    @DefaultMessage("Saving tasks, please wait...")
    String savingTask();

    @DefaultMessage("Select planning")
    String selectPlanning();

    @DefaultMessage("These users already exist : <br/> {0}")
    String userAlreadyExist(String message);

    @DefaultMessage("User exist")
    String userAlreadyExistTitle();

    @DefaultMessage("Please wait")
    String waiting();

}
