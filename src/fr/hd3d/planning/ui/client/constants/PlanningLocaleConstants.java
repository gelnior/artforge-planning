package fr.hd3d.planning.ui.client.constants;

import com.google.gwt.i18n.client.Constants;


public interface PlanningLocaleConstants extends Constants
{
    @DefaultStringValue("Actual end date")
    String actualEndDateField();

    @DefaultStringValue("Actual start date")
    String actualStartDateField();

    @DefaultStringValue("Add task group")
    String addTaskGroupMenuItem();

    @DefaultStringValue("Add unassigned")
    String addUnassignedMenuItem();

    @DefaultStringValue("Auto Group")
    String autoGroupField();

    @DefaultStringValue("Auto resize task group")
    String autoResizeTaskGroupMenuItem();

    @DefaultStringValue("Cancel")
    String cancelButtonText();

    @DefaultStringValue("Change worker")
    String changeWorkerToolTip();

    @DefaultStringValue("Color")
    String colorField();

    @DefaultStringValue("Create")
    String createButtonText();

    @DefaultStringValue("Planning creation box")
    String createPlanningDialogTitle();

    @DefaultStringValue("Task group creation box")
    String createTaskGroupDialogTitle();

    @DefaultStringValue("Day view")
    String dayViewToolTip();

    @DefaultStringValue("Delete task group")
    String deleteTaskGroupMenuItem();

    @DefaultStringValue("Delete unassigned")
    String deleteUnassignedeMenuItem();

    @DefaultStringValue("Display end Date")
    String displayEndDateField();

    @DefaultStringValue("Display start Date")
    String displayStartDateField();

    @DefaultStringValue("Duplicate planning master")
    String duplicatePlanningMasterToolTip();

    @DefaultStringValue("Duration")
    String durationField();

    @DefaultStringValue("Edit Planning")
    String editPlanningToolTip();

    @DefaultStringValue("Edit Task box")
    String editTaskDialogTitle();

    @DefaultStringValue("Edit task")
    String editTaskMenuItem();

    @DefaultStringValue("End date")
    String endDateField();

    @DefaultStringValue("Master")
    String masterField();

    @DefaultStringValue("Mode People")
    String modePeopleToolTip();

    @DefaultStringValue("Mode Work Object")
    String modeWorkObjectToolTip();

    @DefaultStringValue("Month view")
    String monthViewToolTip();

    @DefaultStringValue("Name")
    String nameField();

    @DefaultStringValue("New Planning")
    String newPlanningToolTip();

    @DefaultStringValue("No Duration")
    String noDuration();

    @DefaultStringValue("No planning selected")
    String noPlanningSelectedMessageBoxTitle();

    @DefaultStringValue("You have to select one planning before adding milestone")
    String noPlanningSelectedMessageBoxMessage();

    @DefaultStringValue("Planned duration")
    String plannedDuration();

    @DefaultStringValue("Planned Unassigned")
    String plannedUnassigned();

    @DefaultStringValue("Planning settings")
    String planningSettingToolTip();

    @DefaultStringValue("Print")
    String printToolTip();

    @DefaultStringValue("Redo")
    String redoToolTip();

    @DefaultStringValue("Rename")
    String renameDialogTitle();

    @DefaultStringValue("Rename task group")
    String renameTaskGroupMenuItem();

    @DefaultStringValue("Rename unassigned")
    String renameUnassigned();

    @DefaultStringValue("Resize task")
    String resizeTaskToolTip();

    @DefaultStringValue("Save Changes?")
    String saveChanges();

    @DefaultStringValue("Save")
    String saveToolTip();

    @DefaultStringValue("Search")
    String searchField();

    @DefaultStringValue("Select task")
    String selectTaskToolTip();

    @DefaultStringValue("Show absence")
    String showAbsenceToolTip();

    @DefaultStringValue("Show comments")
    String showCommentToolTip();

    @DefaultStringValue("Start Date")
    String startDateField();

    @DefaultStringValue("Task group")
    String taskGroupToolTip();

    @DefaultStringValue("Unassigned")
    String unAssigned();

    @DefaultStringValue("UnAssigned line label")
    String unAssignedLineLabelDialogTitle();

    @DefaultStringValue("Unassigned Line")
    String unAssignedLineToolTip();

    @DefaultStringValue("All tasks")
    String unAssignedTaskFilterAllTask();

    @DefaultStringValue("assigned")
    String unAssignedTaskFilterAssigned();

    @DefaultStringValue("planned")
    String unAssignedTaskFilterPlanned();

    @DefaultStringValue("un")
    String unAssignedTaskFilterPrefix();

    @DefaultStringValue("Unassigned task")
    String unAssignedTaskMenuItem();

    @DefaultStringValue("Undo")
    String undoToolTip();

    @DefaultStringValue("Update")
    String updateButtonText();

    @DefaultStringValue("Week view")
    String weekViewToolTip();

    @DefaultStringValue("Worker")
    String workerField();

    @DefaultStringValue("show Constituent Planning")
    @Key("showConstituentPlanning")
    String showConstituentPlanning();

    @DefaultStringValue("show Shot Planning")
    @Key("showShotPlanning")
    String showShotPlanning();

}
